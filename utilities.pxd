import numpy as np
cimport numpy as np
import scipy.stats as sts

from stdarg cimport *
from libc.math cimport fabs,log,M_PI
from libc.float cimport DBL_MIN,DBL_MAX

cdef inline double d_max(double a, double b): return a if a >= b else b
cdef inline double d_min(double a, double b): return a if a <= b else b

#cdef inline double isinf(double a): return True if 


ctypedef np.double_t DTYPE_t
ctypedef np.int_t ITYPE_t



#cpdef double newton_raphson_gaussian( double xmin, double xmax, double x0, int seg, np.ndarray[DTYPE_t, ndim=2] priorConsts, np.ndarray[DTYPE_t, ndim=3] llConsts, double alpha, double beta, np.ndarray[DTYPE_t] v, int p , double step=*, double tol=* , int sgn=* )

#cdef double newton_raphson(double (*f)(double,int,np.ndarray[DTYPE_t,ndim=2],np.ndarray[DTYPE_t,ndim=3],double,double,np.ndarray[DTYPE_t,ndim=1], double, int ), void (*fprime)(double,int,np.ndarray[DTYPE_t,ndim=2],np.ndarray[DTYPE_t,ndim=3],double,double,np.ndarray[DTYPE_t,ndim=1], int, np.ndarray[DTYPE_t,ndim=1] out ) , double xmin, double xmax, double x0, int seg, np.ndarray[DTYPE_t, ndim=2] priorConsts, np.ndarray[DTYPE_t, ndim=3] llConsts, double alpha, double beta, np.ndarray[DTYPE_t] v, int p , double step, double tol , int sgn, np.ndarray[DTYPE_t,ndim=1] fpout )



cdef double eval_pdf( double vnew, int seg, double[:,:] priorConsts, double[:,:] llConsts, double alpha, double beta, double[:] v, double logdetLam, int p)

cdef void eval_grad( double t, int seg, double[:,:] priorConsts, double[:,:] llConsts, double alpha, double beta, double[:] v, int p, double[:] out)

cdef double beta_logpdf(double x, double alpha, double beta)

cdef double npsum(double[:] a, int p)

cpdef compute_extrema_p(p, np.ndarray[DTYPE_t] v, tmin, tmax, alpha, beta, seg, np.ndarray[DTYPE_t,ndim=2] pconstants, np.ndarray[DTYPE_t,ndim=2] constants, sgn, ttol , tinit , direction)

cpdef benchmark_numpy()
cpdef benchmark_c()
