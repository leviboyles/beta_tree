import numpy as np
cimport numpy as np
from stdarg cimport *
ctypedef np.double_t DTYPE_t
ctypedef np.int_t ITYPE_t

cdef void sample_multiple_particle( double (*eval_pdf)(double,int,double[:,:], double[:,:],double,double,double[:], double, int )   , np.ndarray[DTYPE_t,ndim=2]  T, int cur_i, double cur_t, int n_iter, int n_init_iter, double[:,:] pC, double[:,:] lC, double al,double be, double[:] v, double dL, int p ,int seed, int* iiout, double* ttout)
cdef void sample( double (*eval_pdf)(double,int,double[:,:], double[:,:],double,double,double[:], double, int ) ,np.ndarray[ITYPE_t,ndim=1] Ind, np.ndarray[DTYPE_t,ndim=2]  T, np.ndarray[DTYPE_t, ndim=1] Mx, int cur_i, double cur_t, int n_iter, double[:,:] pC, double[:,:] lC, double al,double be, double[:] v, double dL, int p ,int seed, int* iiout, double* ttout)


cdef void va_sample( double (*eval_pdf)(double, int, vl_wrapper*), np.ndarray[ITYPE_t,ndim=1] Ind, np.ndarray[DTYPE_t,ndim=2]  T, np.ndarray[DTYPE_t, ndim=1] Mx, int cur_i, double cur_t, int n_iter, int seed, int* iiout, double* ttout, ...)
cdef void va_step_out( double (*eval_pdf)(double,int, vl_wrapper*) ,double y, int seg, double x, double xmin, double xmax, double w, double[:] out, vl_wrapper* vlw)
