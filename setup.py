from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

import numpy as np

ext_modules = [ Extension("utilities",
                ["utilities.pyx"],
                extra_compile_args=["-O3"],
                include_dirs=[ np.get_include() ]) ,
                Extension("slice_sampler", 
                ["slice_sampler.pyx"], 
                extra_compile_args=["-O3"],
                include_dirs=[ np.get_include() ])] #,
                #extra_compile_args=["-g"],
                #extra_link_args=["-g"] ), ] 

setup(
  name = 'Beta Tree Inference',
  cmdclass = {'build_ext': build_ext},
  ext_modules = ext_modules
)
