cdef extern from "stdarg.h":
    ctypedef struct va_list:
        pass
    ctypedef struct fake_type:
        pass
    void va_start(va_list, void* arg)
    void* va_arg(va_list, fake_type)
    void va_end(va_list)
    void va_copy(va_list, va_list )

    #va_ functions are actually macros, so to specify a type you need to 
    #pass a string through cython...
    fake_type int_type "int"
    fake_type double_type "double"
    fake_type pointer_type "void*"


cdef struct vl_wrapper:
    va_list v


