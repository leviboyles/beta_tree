import numpy as np
import scipy.stats as sts
import scipy.linalg as lin
import polya
import utilities as util

import slice_sampler as ssamp
import scipy.optimize as opt

import scipy.cluster.hierarchy as hier
import scipy.spatial.distance as dist
from scipy.maxentropy import logsumexp
import cProfile
import pstats
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib

from collections import deque
from time import time

#from libc.math cimport log,M_PI
#test
def beta_tree_mcmc(X_in, l_alpha, l_beta, numiter, k=0, testX = None, plot=True, plotLabels=None, noisyLeaves=False):

    (N,p) = np.shape(X_in)

    if plotLabels != None:
        pLab = np.zeros(len(plotLabels),dtype=np.int)
        pLab[:] = plotLabels
    else:
        pLab = None
    #end

    eta = .1

    #child pointers   
    C = np.zeros( (2*N-1,2), dtype=np.int) 
    #"remaining" stick lengths
    v = np.zeros( 2*N-1) #, dtype=np.float128)
    #Beta stick proportions 
    B = np.zeros( 2*N-1)


    #initialize to draw from prior
    (Z,P) = polya.beta_htree(2,2, N, treetype='ordered')

    if k == 0:
        k = np.mean(np.var(X_in,0))

    hypupdate_min = 2*N-1
    #initialize with linkage
#    dd = dist.pdist(X_in)
#    Z = hier.linkage(dd)
  
    C[N:,:] = Z[:,0:2]
    P = np.zeros(N*2-1,dtype=np.int) 
    for i in range(N):
        P[C[N-1+i,0]] = N-1+i
        P[C[N-1+i,1]] = N-1+i
    #end 
    

    P[len(P)-1] = -1


    #stick lengths
#    v[N:] = Z[:,2]/max(Z[:,2])    

    #initialize to uniform times 
    vv = sts.uniform.rvs(size=N-1)
    vv = np.sort(vv)/np.max(vv)
    v[N:] = vv

    for i in range(N,2*N-1):
        B[i] = 1 - v[i]/v[P[i]]
    #end
    
    Y = np.zeros( (N,p))
    nu0 = np.zeros(N)

    if noisyLeaves:
        nu0[:] = .1  

    Y[:] = X_in[:]    
    
    #noise model parameters    
    df = 100
    sig0_2 = k

    #noise model hypers
    a0 = 1
    b0 = 1
    lam0 = 10

    (logZ,Mu,nu) = util.compute_messages(X_in,nu0,C,v,k)
    (logZd,Mu_d,nu_d) = util.compute_downward_messages(X_in,C,P,v,k,Mu,nu)

    print np.sum(logZ)



    M = compute_M(C)
    prune_ind = 0
    Trees = []
    Times = []


    xi_alpha = 200
    lam_alpha = 2

    xi_beta = 2
    lam_beta = 200

    if plot:
        plt.figure(1)

    time0 = time()


    
    sample_locs = False
    
    if sample_locs:
        sX = util.sample_locations(Y,Mu_d,nu_d,C,P,v,k )
    else:
        sX = None

    for itr in range(numiter):


        if np.mod(itr,N) == 0 and plot:


            Zout = construct_dendrogram(C,v,M)
            plt.clf()

            plot_color_dendrogram(Zout,pLab=pLab,cmap=cm.jet,leg_str=['BCR','E2A','Hyperdip','MLL','T-ALL','TEL','OTHERS'])

#            if pLab != None:
#                colors = np.zeros(2*N-1)
#
#                def defineColors(C,ind,colors):
#
#                    if ind < N:
#                        colors[ind] = float(1+pLab[ind])/(1+np.max(pLab))
#                        return 
#                    #end
#
#                    ll = C[ind,0]
#                    rr = C[ind,1]
#                    defineColors(C,ll,colors)
#                    defineColors(C,rr,colors)
#
#                    if colors[ll] == colors[rr]:
#                        colors[ind] = colors[ll]
#
#
#                defineColors(C,2*N-2,colors)
#            
#                colors2 = np.zeros(2*N-1)
#                colors2[ind_map[N:]] = colors[N:]
#
#
#                def cmap(x):
#                    if colors2[x] == 0:
#                        return matplotlib.colors.rgb2hex((0,0,0,0))
#                    else:
#                        return matplotlib.colors.rgb2hex(cm.jet(colors2[x]))
#
#            else:
#                cmap = None
#
#            #end                
#
#
#            hier.dendrogram(Zout,p=N,color_threshold=0,labels=pLab, link_color_func=cmap)
            plt.draw()



        while P[P[prune_ind]] == -1: #don't remove the left or right subtree of the root (0 prob under prior)
            prune_ind = np.mod(prune_ind+1,2*N-1)
        #end

        if np.mod(prune_ind,2*N-1) == 0 and itr >= hypupdate_min:
            #k = update_hyp_variance(X_in,Mu,nu,C,P,v,1,1,k )

            #k = update_hyp_variance_mgzed(Mu,nu,C,v,1,1 )
            print "Variance hyperparameter: %f" % k
            (l_alpha,l_beta) = util.update_hyp_alpha_beta(P,v,l_alpha,l_beta,xi_alpha,lam_alpha, xi_beta, lam_beta,20,20) 
            print "Stick breaking parameters: (%f, %f)" % (l_alpha,l_beta)
            print itr    

            (logZ,Mu,nu) = util.compute_messages(Y,nu0,C,v,k)

            (logZd,Mu_d,nu_d) = util.compute_downward_messages(Y,C,P,v,k,Mu,nu)

            if noisyLeaves:
                (blah,df,sig0_2, sigt) = update_noise_model(X_in,Y, df, sig0_2, a0,b0,lam0, Mu, nu, C, P, v, k )
                #(blah,blah2,blah3, sigt) = update_noise_model(X_in,Y, df, sig0_2, a0,b0,lam, Mu, nu, C, P, v, k )
                nu0[:] = sigt/k

                (logZ,Mu,nu) = util.compute_messages(Y,nu0,C,v,k)
                (logZd,Mu_d,nu_d) = util.compute_downward_messages(Y,C,P,v,k,Mu,nu)
                print "Noise model df,var,svar: (%f, %f, %f, %f)" % (df,sig0_2,np.mean(sigt),np.max(sigt))

        if 0:
            printflag = True
        else:
            printflag = False

#        if itr > hypupdate_min:
#            u = sts.uniform.rvs()
#            sample_locs = u < eta 
#        else:
#            sample_locs = True

        if sts.uniform.rvs() < eta:
            msamp = 200
        else:
            msamp = 100


        (C,P) = util.jump_structure_opt(Y,nu0,prune_ind,C,P,v,k,l_alpha,l_beta,Mu,nu,logZ,printflag = printflag, sX=sX, msamp=msamp )
#        (C,P) = jump_structure(X_in,prune_ind,C,P,v,k,l_alpha,l_beta,Mu,nu,logZ,printflag = printflag  )
    
        M = compute_M(C)
        prior_prob = compute_prior(v,C,P,l_alpha,l_beta,M) #-np.sum(np.log(M[N:]))
        (logZ,Mu,nu) = util.compute_messages(Y,nu0,C,v,k)

        (logZd,Mu_d,nu_d) = util.compute_downward_messages(Y,C,P,v,k,Mu,nu)

        post_prob = prior_prob + np.sum(logZ)

        assert not np.isinf( post_prob)


        if np.mod(itr,N) == 0:
#            (logZ2,Mu2,nu2) = compute_messages_at(N+30,X_in,C,P,v,np.eye(p) )

#            (logZd,Mud,nud) = util.compute_downward_messages(X_in,C,P,v,k,Mu,nu)
#            (logZd2,Mud2,nud2) = compute_downward_messages(X_in,C,P,v,k,Mu,nu)


#            dm_ind = N+30
#            path = [dm_ind]
#
#            tmp_ind = dm_ind
#            while P[tmp_ind] != -1:
#                path.append(P[tmp_ind])
#                tmp_ind = P[tmp_ind]
#
#            #print path
#            dsum2 = np.sum(logZd2[path])
#            dsum1 = np.sum(logZd[path])
#
#            rem = set(range(2*N-1)).difference(path)
#            #rem.add(dm_ind)
#            pp = P[dm_ind]
#            ll = C[dm_ind,0]
#            rr = C[dm_ind,1]
#
#            Mu_m = np.c_[Mud2[dm_ind],Mu[ll],Mu[rr]].T
#            nu_m = np.r_[nud2[dm_ind],nu[ll],nu[rr]]
#            ss = np.array( [v[pp]-v[dm_ind], v[dm_ind]-v[ll], v[dm_ind]-v[rr]] )
#
#            (lZ3,blah,blah2) = util.compute_message_p( Mu_m, nu_m, k, ss) 
#
#            usum = np.sum(logZ[list(rem)     ])


#            assert( np.sum(logZ) >- 14000 or itr < 200)


            print "Posterior Prob: %f" % post_prob
#            print "Prior: %f LL: %f" % (prior_prob,  np.sum(logZ))
#            print "LL2: %f LL3: %f LL4: %f" % (np.sum(logZ2), dsum2+usum+lZ3,dsum1+usum+lZ3 )
            print "Total Time Elapsed: %f" % (time()-time0 )
        #prune_ind += 1
        if itr >= 2*N and np.mod(itr,(2*N-1) ) == 0:
            Trees.append( (C,P,v.copy(), k, l_alpha, l_beta, Y.copy(), nu0.copy(),df,sig0_2) )
            Times.append( time()-time0 )

        prune_ind = np.mod(prune_ind+1,2*N-1)
       
        
 
    #end

    Zout = construct_dendrogram(C,v,M)
    if plot:
        plt.clf()

        plot_color_dendrogram(Zout,pLab=pLab,cmap=cm.jet, leg_str=['BCR','E2A','Hyperdip','MLL','T-ALL','TEL','OTHERS'])

    #end

    zplot=0
    Xplot = 0
    Yplot = 0
    testLL = 0
    predvals = 0

    if testX != None:

        msamp = 100
        predvals = compute_predictives(Trees,X_in,testX,msamp,noisyLeaves=noisyLeaves)

        predvals2 = np.sum(predvals,1)

        testLL = np.zeros(len(predvals2))
        for j in range(len(predvals2)):
            testLL[j] =  logsumexp(predvals2[0:j+1]) - np.log(j+1)


        if plot:
            plt.plot(Times,testLL,'r-')

    #end


    if p == 2 and plot:
        plt.figure()
        plt.clf()
        plt.scatter(X_in[:,0], X_in[:,1])

        (xmin,xmax) = plt.xlim()
        (ymin,ymax) = plt.ylim()


        rangestep_x = (xmax-xmin)/100
        rangestep_y = (ymax-ymin)/100

        xplot = np.arange(xmin-1,xmax+1,rangestep_x)
        yplot = np.arange(ymin-1,ymax+1,rangestep_y)

        (Xplot,Yplot) = np.meshgrid(xplot,yplot)


        XY = np.r_['2',np.atleast_3d(Xplot), np.atleast_3d(Yplot)]
        
        msamp = 100

        predvals = compute_predictives(Trees,X_in,XY,msamp,noisyLeaves=noisyLeaves) 

        predvals = np.array(predvals)

        zplot = np.zeros( (len(Xplot), len(Yplot)))

        for xx in range(len(Xplot)):
            for yy in range(len(Yplot)):
                zplot[xx,yy] = logsumexp(predvals[:,xx,yy])


#        zplot = 0 
#        itr = 0
#        for tree in Trees:
#
#            itr+= 1
#            print "On tree %d of %d" % (itr,len(Trees))
#            
#            (C,P,vt,k,alpha,beta) = tree
#            (pMu,pnu,pv,pseg,nu_u,nu_d) = util.predictive_density(X_in,C,P,vt,k,alpha,beta,msamp,10)            
#          
#            assert pnu[0]+pv[0] > 0
#
#            print "Computing grid values"
# 
#            for j in range(len(pMu)):
#                ztmp = sts.norm.pdf(XY,loc=pMu[j], scale=np.sqrt(pnu[j]+pv[j]) )
#                zplot += np.prod(ztmp,2)
#            #end
#
        

        lzplot = zplot 
        zplot = np.exp(zplot) 

        print "plotting contour"

        lzmax = np.max(lzplot)
        V = np.arange(lzmax,lzmax-15,-1)


        plt.contour(Xplot,Yplot,lzplot,V)        
        plt.xlim( (xmin,xmax))
        plt.ylim( (ymin,ymax))

        plt.figure()
        plt.clf()
        plt.scatter(X_in[:,0], X_in[:,1])
        plt.contour(Xplot,Yplot,zplot,10)        
        plt.xlim( (xmin,xmax))
        plt.ylim( (ymin,ymax))

    #end


    return (Zout,v,Trees,Times,testLL,predvals,Xplot,Yplot,zplot)
    #return (logZ,logZ2, Mu, Mu2, nu, nu2, X,Y,Pr,LL)
#    return (X,Y,Pr,LL) #(logZ,Mu,nu)

#    for i in range(numiter):
        

    #end 


def plot_color_dendrogram(Z,pLab=None,cmap=cm.jet,leg_str=None):
    Nm1 = len(Z)
    N = Nm1+1

    if pLab != None:


        H = hier.dendrogram(Z,p=N,color_threshold=0,labels=pLab,no_plot=True) 

        lbls = H['ivl']
        lblcoords = np.arange(5,10*len(lbls)+5,10)

        plt.xticks(lblcoords,lbls)

        ax = plt.gca()
        ax.set_ylim([0,1.05])
        ax.set_xlim([0,10*len(lbls)])
        plt.setp( ax.get_xticklines(),visible=False)
        plt.setp( ax.get_xticklabels(),fontsize=6)

        if leg_str != None:

            for k in range(len(np.unique(pLab))):
                clr = float(1+k)/(1+np.max(pLab))
                clr_str = matplotlib.colors.rgb2hex(cmap(clr))

                plt.plot(-1,-1,clr_str,label=leg_str[k])

            #end
            plt.legend(prop={'size':9})
        #end

        colors = np.zeros(2*N-1)

        def defineColors(Z,ind,colors):

            if ind < N:
                colors[ind] = float(1+pLab[ind])/(1+np.max(pLab))
                return 
            #end

            ll = Z[ind-N,0]
            rr = Z[ind-N,1]
            defineColors(Z,ll,colors)
            defineColors(Z,rr,colors)

            if colors[ll] == colors[rr]:
                colors[ind] = colors[ll]


        defineColors(Z,2*N-2,colors)
    



        ict = H['icoord']
        dct = H['dcoord']

        plotorder = np.argsort(np.array(dct)[:,1])

        ic = np.array(ict)[plotorder]
        dc = np.array(dct)[plotorder]


        for i in range(len(Z)):

            ind = N+i

            if colors[ind] != 0:


                cstr = matplotlib.colors.rgb2hex(cmap(colors[ind])) 
                plt.plot(ic[i],dc[i], cstr)

                assert Z[i,2] == dc[i][1]

            else:
                ll = Z[i,0]
                rr = Z[i,1]

                lcol = matplotlib.colors.rgb2hex(cmap(colors[ll]))
                rcol = matplotlib.colors.rgb2hex(cmap(colors[rr]))

                plt.plot(ic[i][0:2], dc[i][0:2], lcol)
                plt.plot(ic[i][1:3], dc[i][1:3], 'k')
                plt.plot(ic[i][2:4], dc[i][2:4], rcol)
            #end


    else:
        H = hier.dendrogram(Z,p=N,color_threshold=0) #,link_color_func=cmap)

#msamp, number of predictive samples
#ksmap, number slice sampling steps to take between each predictive sample
def compute_predictives(Trees,X_in,testX,msamp,ksamp=10,noisyLeaves=False):
   
    dim = np.ndim(testX) 
    predvals = []
    itr = 0

    for tree in Trees:

        itr+= 1
        print "On tree %d of %d" % (itr,len(Trees))
        
        (C,P,vt,k,alpha,beta,Y, nu0, df, sig0_2) = tree
        (pMu,pnu,pv,pseg,nu_u,nu_d) = util.predictive_density(Y,nu0,C,P,vt,k,alpha,beta,msamp,ksamp)            
      
        assert pnu[0]+pv[0] > 0

        print "Computing test values"

        predtmp = []
        for j in range(len(pMu)):


            if noisyLeaves:

                if df*sig0_2/2 == 0:
                    print "Noise model at 0 variance"
                    sig2 = 0
                else:
                    sig2 = sts.invgamma.rvs(df/2, scale=(df*sig0_2/2),size=50)

                for i in range(len(sig2)):

                    ztmp = sts.norm.logpdf(testX,loc=pMu[j], scale=np.sqrt(k*(pnu[j]+pv[j]) +sig2[i]  ) )

                    predtmp.append( np.sum(ztmp,dim-1) )

                #end
            else:
                ztmp = sts.norm.logpdf(testX,loc=pMu[j], scale=np.sqrt(k*(pnu[j]+pv[j])   ) )

                predtmp.append( np.sum(ztmp,dim-1) )


            #predtmp.append( np.sum(ztmp,dim-1) )


        #end
        predtmp = np.array(predtmp)

        #logsumexp
        predtmp2 = np.log(np.sum(np.exp(predtmp-np.max(predtmp,0)),0))-np.log(len(predtmp))+np.max(predtmp,0)


        predvals.append(predtmp2)

    #end
        
    return predvals 

def compute_prior(v,C,P,alpha,beta,M):
    tnm1 = len(v)
    N = (tnm1+1)/2
    #structure term
    prior_prob = -np.sum(np.log(M[N:]))

    Q = deque([2*N-2])

    while len(Q) > 0:

        pp = Q.popleft()
        ll = C[pp,0]
        rr = C[pp,1]

        vpp = v[pp]
        vll = v[ll]
        vrr = v[rr]

        if ll >= N:
    
            B1 = 1 - vll/vpp
            prior_prob += beta_logpdf(B1,alpha,beta) 
            Q.append(ll)
        if rr >= N:
            B1 = 1 - vrr/vpp
            prior_prob += beta_logpdf(B1,alpha,beta) 
            Q.append(rr)

    #end 
    
    return prior_prob 


def construct_dendrogram(C,v,M):
    tnm1 = len(v)
    N = (tnm1+1)/2

    Ctmp = C[N:,:]

    Q = deque([tnm1-1])

    cur_ind = tnm1-1

    Z = np.zeros( (N-1,4) )

    vind = np.argsort( v[N:] )

    vvind = np.argsort( vind)

    vmap = np.r_[ np.arange(0,N), vvind+N]

    Z[:,2] = v[vind+N]
    Z[:,0] = vmap[Ctmp[vind,0]]
    Z[:,1] = vmap[Ctmp[vind,1]]

#    gN = np.nonzero(Z[:,0] >= N)[0]
#    Z[gN,0] = vind[Ctmp[vind,0][gN]-N]+N
#
#    gN = np.nonzero(Z[:,1] >= N)[0]
#    Z[gN,1] = vind[Ctmp[vind,1][gN]-N]+N


#    while len(Q) > 0:
#
#        par = Q.popleft()
#        ll = C[par,0]
#        rr = C[par,1]
#
#
#        if ll >= N:
#            Q.append(ll)
#            ll_ind = cur_ind-len(Q)
#            ind_map[ll] = ll_ind
#        else:
#            ll_ind = ll
#        if rr >= N:
#            Q.append(rr)
#            rr_ind = cur_ind-len(Q)
#            ind_map[rr] = rr_ind
#        else:
#            rr_ind = rr
#
#        Z[cur_ind-N,0] = ll_ind
#        Z[cur_ind-N,1] = rr_ind
#        Z[cur_ind-N,2] = v[par]
#        Z[cur_ind-N,3] = M[par]+1
#        cur_ind -= 1


    return Z

#compute M counts (the number of internal nodes below a node, including itself)
def compute_M(C):
    (tnm1,_) = np.shape(C)

    M = np.zeros( tnm1, dtype=np.int)

    compute_M_h(tnm1-1,C,M)

    return M

def compute_M_h(ind,C,M):

    (tnm1,_) = np.shape(C)
    N = (1+tnm1)/2

    if ind < N:
        return 0

    M[ind] = compute_M_h(C[ind,0],C,M) + compute_M_h(C[ind,1],C,M)+1

    return M[ind]

#compute upward messages for Brownian motion
def compute_messages(X, C, v, k):
    (N,p) = np.shape(X)

    Mu = np.zeros( (2*N-1,p) )
    nu = np.zeros( 2*N-1 )

    logZ = np.zeros( 2*N-1 )

    compute_messages_h(X,C,v,k, 2*N-2, Mu,nu,logZ)

    return (logZ,Mu,nu)

def compute_messages_h(X,C,v, k, ind, Mu_a, nu_a, logZ):
    (N,p) = np.shape(X)
    
    if ind < N:
        Mu_a[ind,:] = X[ind,:]
        nu_a[ind] = 0
        return (X[ind,:], 0)
    #end

    ll = C[ind,0]
    rr = C[ind,1]

    (Mu_l, nu_l) = compute_messages_h(X,C,v,k, ll, Mu_a, nu_a, logZ)
    (Mu_r, nu_r) = compute_messages_h(X,C,v,k, rr, Mu_a, nu_a, logZ)

    t_l = v[ll]
    t_r = v[rr]
    t = v[ind]

    nu = ( (nu_l + t-t_l)**-1 + (nu_r + t-t_r)**-1)**-1
    Mu = ( Mu_l/(nu_l+t-t_l) + Mu_r/(nu_r+t-t_r) )* nu

    Mu_a[ind,:] = Mu
    nu_a[ind] = nu

    nu_s = nu_l+nu_r+2*t-t_l-t_r

    df = Mu_r - Mu_l

    mdist = np.dot(df,df)/k
    logdetLam = p*np.log(k)


    logZ[ind] = -.5*( logdetLam + p*np.log( 2*np.pi*nu_s ) + mdist/nu_s) 

    assert not np.isnan(logZ[ind])
    assert not np.isinf(logZ[ind])

#    assert not ind == 164

    return (Mu,nu)


#downwards messages from a parent to a child is stored at the index of the child
def compute_downward_messages(X, C,P,v,k,Mu_u, nu_u):
    (N,p) = np.shape(X)

    Mu_d = np.zeros((2*N-1,p))
    nu_d = np.zeros(2*N-1)

    logZ = np.zeros(2*N-1)

    Q = deque([2*N-2])

    while len(Q) > 0:

        ind = Q.popleft()
        ll = C[ind,0]
        rr = C[ind,1]
        
        if ind == 2*N-2:
            (lZll, Mu_ll, nu_ll) = util.compute_message_p(Mu_u[[rr],:], nu_u[[rr]], k, v[[ind]]-v[rr])
            (lZrr, Mu_rr, nu_rr) = util.compute_message_p(Mu_u[[ll],:], nu_u[[ll]], k, v[[ind]]-v[ll])
        else:
            Mull_in = np.c_[Mu_u[rr,:] , Mu_d[ind,:]].T
            Murr_in = np.c_[Mu_u[ll,:] , Mu_d[ind,:]].T

            pp = P[ind]

            sll = np.array( [ v[ind]-v[rr], v[pp]-v[ind] ] )
            srr = np.array( [ v[ind]-v[ll], v[pp]-v[ind] ] )

            (lZll, Mu_ll, nu_ll) = util.compute_message_p(Mull_in, np.array([nu_u[rr], nu_d[ind]]), k, sll )
            (lZrr, Mu_rr, nu_rr) = util.compute_message_p(Murr_in, np.array([nu_u[ll], nu_d[ind]]), k, srr )

        #end

        #assert not np.any( np.isnan(Mu_ll))
        #assert not np.any( np.isnan(Mu_rr))

        Mu_d[ll] = Mu_ll
        Mu_d[rr] = Mu_rr

        nu_d[ll] = nu_ll
        nu_d[rr] = nu_rr

        logZ[ll] = lZll
        logZ[rr] = lZrr

        if ll >= N:
            Q.append(ll)
        if rr >= N:
            Q.append(rr)

#        compute_downward_messages_h(X,C,P,v,k,ll,Mu_u,nu_u,Mu_d,nu_d,logZ)
#        compute_downward_messages_h(X,C,P,v,k,rr,Mu_u,nu_u,Mu_d,nu_d,logZ)



    #end

    #compute_downward_messages_h(X,C,P,v,k,2*N-2,Mu_u,nu_u,Mu_d,nu_d,logZ)

    return (logZ,Mu_d,nu_d)

def compute_downward_messages_h(X,C,P,v,k,ind,Mu_u,nu_u,Mu_d,nu_d,logZ):

    (N,p) = np.shape(X)

    if ind < N:
        return
    #end

    ll = C[ind,0]
    rr = C[ind,1]
    
    if ind == 2*N-2:
        (lZll, Mu_ll, nu_ll) = util.compute_message(Mu_u[[rr],:], nu_u[[rr]], k, v[[ind]]-v[rr])
        (lZrr, Mu_rr, nu_rr) = util.compute_message(Mu_u[[ll],:], nu_u[[ll]], k, v[[ind]]-v[ll])
    else:
        Mull_in = np.c_[Mu_u[rr,:] , Mu_d[ind,:]].T
        Murr_in = np.c_[Mu_u[ll,:] , Mu_d[ind,:]].T

        pp = P[ind]

        sll = np.array( [ v[ind]-v[rr], v[pp]-v[ind] ] )
        srr = np.array( [ v[ind]-v[ll], v[pp]-v[ind] ] )

        (lZll, Mu_ll, nu_ll) = util.compute_message(Mull_in, np.array([nu_u[rr], nu_d[ind]]), k, sll )
        (lZrr, Mu_rr, nu_rr) = util.compute_message(Murr_in, np.array([nu_u[ll], nu_d[ind]]), k, srr )

    #end

    #assert not np.any( np.isnan(Mu_ll))
    #assert not np.any( np.isnan(Mu_rr))

    Mu_d[ll] = Mu_ll
    Mu_d[rr] = Mu_rr

    nu_d[ll] = nu_ll
    nu_d[rr] = nu_rr

    logZ[ll] = lZll
    logZ[rr] = lZrr

    compute_downward_messages_h(X,C,P,v,k,ll,Mu_u,nu_u,Mu_d,nu_d,logZ)
    compute_downward_messages_h(X,C,P,v,k,rr,Mu_u,nu_u,Mu_d,nu_d,logZ)


def compute_messages_at(root, X,C,P,v,k):
    (N,p) = np.shape(X)

    Mu = np.zeros( (2*N-1,p) )
    nu = np.zeros( 2*N-1 )

    logZ = np.zeros( 2*N-1 )

    unmgized = set(np.arange(2*N-1))

    unmgized.remove(root)

    compute_messages_at_h(root, X,C,P,v,k,root, Mu, nu, logZ, unmgized)

    return (logZ,Mu,nu)

def compute_messages_at_h(root, X,C,P,v,k,ind, Mu_a, nu_a, logZ, rem):


    (N,p) = np.shape(X)
    
    if ind < N:
        Mu_a[ind,:] = X[ind,:]
        nu_a[ind] = 0
        return (X[ind,:], 0)
    #end

    if len(rem) == 0:
        return
    #end 

    ll = C[ind,0]
    rr = C[ind,1]
    pp = P[ind]

    t = v[ind]

    nbinds = set([ll,rr,pp])
    #don't use the root's fake parent
    nbinds.difference_update([-1])
    nbinds = rem.intersection(nbinds)

    rem.difference_update(nbinds)


    for i in nbinds:
        compute_messages_at_h(root, X,C,P,v,k, i, Mu_a, nu_a, logZ, rem)
    #end

    s = []

    for i in nbinds:
        s.append( np.abs(t-v[i]) )
    #end


    s = np.array(list(s))

#    nu = np.sum( (nu_a[nbinds]+s)**-1)**-1
#    Mu = np.sum( Mu_a[nbinds]/(nu_a[nbinds]+s))*nu

    nbinds = list(nbinds)

    (lZ,Mu,nu) = compute_message(Mu_a[nbinds,:],nu_a[nbinds],k, s)

#    if len(nbinds) > 1:
#        (lZ,Mu,nu) = util.compute_message_p(Mu_a[nbinds,:],nu_a[nbinds],1, s)

    #assert not ind == 164
    #assert np.any(v[list(nbinds)] >0)
    #assert( not np.isnan(Mu) )

    nu_a[ind] = nu
    Mu_a[ind,:] = Mu
    logZ[ind] = lZ



#compute message given a set of (up to 3) input messages, at a distances s_in away from the relevent node
#if s_in is not specified a function over s_in is given
def compute_message(Mu_in, nu_in, Lambda, s_in=-1, distances=0, debug=False):

    (nn,p) = np.shape(Mu_in)



#    nu = np.sum( (nu_in+s_in)**-1)**-1
#    Mu = np.sum( Mu_in/(nu_in+s_in).reshape(nn,1),0)*nu


#    assert not np.isnan(nu)
#    assert not np.isinf(nu)

    if nn == 1:
        if debug:
            def mes_f(s_in):
                nu = np.sum( (nu_in+s_in)**-1)**-1
                Mu = np.sum( Mu_in/(nu_in+s_in).reshape(nn,1),0)*nu
                return (0,Mu_in,nu)
            return mes_f
        elif np.sum(s_in) >= 0:
            nu = np.sum( (nu_in+s_in)**-1)**-1
            Mu = np.sum( Mu_in/(nu_in+s_in).reshape(nn,1),0)*nu
            return (0,Mu_in,nu)

        

    elif nn == 2:

        df = Mu_in[1]-Mu_in[0]

        if np.all(Lambda == np.eye(p)):
            mdist = np.dot(df,df)
            detLam = 1
        else:
            rhs = lin.solve(Lambda,df)
            mdist = np.dot(df,rhs)
            detLam = lin.det(Lambda)
        #end

        if debug:
            def mes_f(s_in):
                nu = np.sum( (nu_in+s_in)**-1)**-1
                Mu = np.sum( Mu_in/(nu_in+s_in).reshape(nn,1),0)*nu
                nu_s = nu_in[0]+nu_in[1]+s_in[0]+s_in[1]

                lZ= -.5*( np.log(detLam ) + p*np.log( 2*np.pi*nu_s ) + mdist/nu_s) 
                return (lZ,Mu,nu) 
            return mes_f
        elif np.sum(s_in) >= 0:
            nu = np.sum( (nu_in+s_in)**-1)**-1
            Mu = np.sum( Mu_in/(nu_in+s_in).reshape(nn,1),0)*nu
            nu_s = nu_in[0]+nu_in[1]+s_in[0]+s_in[1]

            lZ= -.5*( np.log(detLam ) + p*np.log( 2*np.pi*nu_s ) + mdist/nu_s) 
            return (lZ,Mu,nu) 

    elif nn == 3:

        df12 = Mu_in[1]-Mu_in[0]
        df23 = Mu_in[2]-Mu_in[1]
        df31 = Mu_in[0]-Mu_in[2]

        if np.all(Lambda == np.eye(p)):
            mdist12 = np.dot(df12,df12)
            mdist23 = np.dot(df23,df23)
            mdist31 = np.dot(df31,df31)
            detLam = 1
        else:
            rhs = lin.solve(Lambda, df12)
            mdist12 = np.dot(rhs,df12)#*nu_t[2]/nu_s
            rhs = lin.solve(Lambda, df23)
            mdist23 = np.dot(rhs,df23)#*nu_t[0]/nu_s
            rhs = lin.solve(Lambda, df31)
            mdist31 = np.dot(rhs,df31)#*nu_t[1]/nu_s

            detLam = lin.det(Lambda)
        #end

        if not distances == 0:
            distances.append(mdist12)
            distances.append(mdist23)
            distances.append(mdist31)
        #end


        if debug:
            f = np.sum(df12+df23+df31+mdist31+mdist23+mdist12)

            assert (not np.isnan(f) and not np.isinf(f))

            def mes_f(s_in):      
                nu = np.sum( (nu_in+s_in)**-1)**-1
                Mu = np.sum( Mu_in/(nu_in+s_in).reshape(nn,1),0)*nu
     
                assert( not np.isnan(nu) and not np.isinf(nu))

                nu_t = nu_in+s_in
                nu_s = nu_t[0]*nu_t[1]+nu_t[1]*nu_t[2]+nu_t[2]*nu_t[0]

                if nu_s != 0:
                    lZ = -( p*np.log(2*np.pi)+np.log(detLam) + .5*p*np.log(nu_s) + .5*(mdist12*nu_t[2]/nu_s+mdist23*nu_t[0]/nu_s+mdist31*nu_t[1]/nu_s))
                    assert not np.isnan(lZ)
                else:
                    lZ = -np.inf

                return (lZ,Mu,nu) 
            return mes_f
        elif np.sum(s_in) >= 0:
            nu = np.sum( (nu_in+s_in)**-1)**-1
            Mu = np.sum( Mu_in/(nu_in+s_in).reshape(nn,1),0)*nu
 
            assert( not np.isnan(nu) and not np.isinf(nu))

            nu_t = nu_in+s_in
            nu_s = nu_t[0]*nu_t[1]+nu_t[1]*nu_t[2]+nu_t[2]*nu_t[0]

            if nu_s != 0:
                lZ = -( p*np.log(2*np.pi)+np.log(detLam) + .5*p*np.log(nu_s) + .5*(mdist12*nu_t[2]/nu_s+mdist23*nu_t[0]/nu_s+mdist31*nu_t[1]/nu_s))
                assert not np.isnan(lZ)
            else:
                lZ = -np.inf

            return (lZ,Mu,nu) 


    #end

#obsolete don't use
def jump_structure(X,prune_ind,C,P,v,k,alpha,beta,Mu_u,nu_u, logZ_u, printflag = False):
    (N,p) = np.shape(X)

    Cn = C.copy()
    Pn = P.copy()
    logZ_un = logZ_u.copy()

    prune_sib = C[P[prune_ind], np.nonzero(C[P[prune_ind]] != prune_ind)[0][0]]

    prune_branch(prune_ind,Cn,Pn)
    M = compute_M(Cn)
    (logZ_pu,Mu_pu,nu_pu) = util.compute_messages(X,Cn,v,k)
    (logZ_pd,Mu_pd,nu_pd) = util.compute_downward_messages(X,Cn,Pn,v,k,Mu_pu,nu_pu)

    pconsts = util.jump_priors(prune_ind,Cn,Pn,v,M,alpha,beta)
    consts = util.jump_likelihoods(prune_ind,Cn,Pn,v,k,Mu_pu,nu_pu,Mu_pd,nu_pd, Mu_u,nu_u, logZ_pu, logZ_pd, logZ_u)  

    #consts2 = jump_likelihoods(prune_ind,Cn,Pn,v,k,Mu_pu,nu_pu,Mu_pd,nu_pd, Mu_u,nu_u, logZ_pu, logZ_pd, logZ_u)

    logLike = np.zeros(2*N-2,dtype=np.object)


#    roots = np.zeros(len(logPriors),dtype=np.object)

#    F = np.zeros(len(logPriors),dtype=np.object)

    F = []
    T = []
    I = []
    Mx = []


    logdetLam = p*np.log(k)

    t_ind = 0
    I_ind = 0

    #def make_func(tind):
    #    func = lambda x : logPriors[tind](x)+logLike[tind](x)
    #    return func

    while pconsts[t_ind,4] == 0  or v[Pn[t_ind]] < v[prune_ind] :
        t_ind += 1

    while t_ind < 2*N-2:
            
        
        #func = make_func(t_ind)
 
        vmin = max(v[t_ind],v[prune_ind])
        vmax = v[Pn[t_ind]]


#        eps = (vmax-vmin)*10**-10
#        vminopt = vmin+eps
#        vmaxopt = vmax-eps
        #(maxima, minima) = compute_extrema( p, v, vmin, vmax, alpha, beta, t_ind, pconsts, consts,1,10.0**-6,0,0)
        (maxima, minima) = util.compute_extrema_p( p, v, vmin, vmax, alpha, beta, t_ind, pconsts, consts,1,0,0,0)



#        allrts = compute_roots(p,v,alpha,beta, consts[t_ind] )
#
#        rts = allrts
#
#        rts = rts[ np.nonzero( np.isreal(rts))[0]]
#        rts = np.real(rts)
#        rts = rts[ np.nonzero( np.less(rts,vmax))[0]]
#        rts = rts[ np.nonzero( np.greater(rts,vmin))[0]]
#
#        assert 0


        if len(maxima) > 1:
            print "multiple modes"

            assert(len(maxima) == len(minima)-1)

            for mi in range(len(maxima)):
                mm = maxima[mi]
                tl = minima[mi]
                tr = minima[mi+1]


                T.append( [tl,tr])
                I.append(t_ind)
                Mx.append(mm)

                if t_ind == prune_sib and v[P[prune_ind]] <= tr and v[P[prune_ind]] >= tl:
                    It = len(I)-1
                #end


#            eps = 10**-40
#            pX = np.linspace(vmin+eps,vmax-eps,1000)
#            Y = np.zeros(len(pX))
#            for xind in range(len(pX)):
#                Y[xind] = util.eval_pdf_p(pX[xind],t_ind,pconsts,consts,alpha,beta,v,logdetLam,p)
#
#            plt.figure(1)
#            plt.clf()
#            plt.plot(pX,Y,'r-')
#            plt.draw()
#            assert(0)
        elif len(minima) == 2 and len(set(minima).difference([vmin,vmax])) == 0 : #only two minima, ie the endpoints of the interval #and not (prune_ind >= N and t_ind >= N): # and prune_ind != 150:
            #F.append(func)
            T.append( [vmin,vmax] )
            I.append(t_ind)
            Mx.append(maxima[0])
            if t_ind == prune_sib:
                It = len(I)-1
            #end
        else:
           
             
            eps = 10**-40
            pX = np.linspace(vmin+eps,vmax-eps,1000)
            Y = np.zeros(len(pX))
            S = np.zeros(len(pX))
            Pr = np.zeros(len(pX))
            LL = np.zeros(len(pX))

            func = lambda x : util.eval_pdf_p(x,t_ind,pconsts,consts,alpha,beta,v,logdetLam,p)

            for xind in range(len(pX)):
                S[xind] = util.eval_pdf_p(pX[xind],t_ind,pconsts,consts,alpha,beta,v,logdetLam,p)
            #    Y[xind] = func(pX[xind])
            #    Pr[xind] = logPriors[t_ind](pX[xind])
                LL[xind] = logLike[t_ind](pX[xind])

            plt.figure(1)
            plt.subplot(231)
            plt.plot(pX,Y,'r-')
            plt.plot(pX,S,'g-')
            if len(maxima) > 0:
                plt.plot(maxima[0],util.eval_pdf_p(maxima[0]),'r*')
            plt.subplot(232)
            plt.plot(pX,Pr-max(Pr),'g-')
            plt.subplot(233)
            plt.plot(pX,LL-max(LL),'b-')
            plt.subplot(234)
            plt.plot(pX,np.exp(Y-max(Y)),'r-')
            if len(maxima) > 0:
                plt.plot(maxima[0],np.exp(func(maxima[0])-max(Y)),'r*')
            plt.subplot(235)
            plt.plot(pX,np.exp(Pr-max(Pr)),'g-')
            plt.subplot(236)
            plt.plot(pX,np.exp(LL-max(LL)),'b-')

#            logPriors[t_ind](vmax,debug=True)
            hp_rts = compute_roots(p,v,alpha,beta, consts[t_ind], highprec=True )

            print prune_ind
            assert(0)        
            print "This also shouldn't happen"
        #end

        t_ind += 1
        while pconsts[t_ind,4] == 0 or v[Pn[t_ind]] < v[prune_ind] :
            t_ind += 1

    #end

    seed = np.random.randint(100000)


    (ii,tt) = ssamp.sample(np.array(I),np.array(T),np.array(Mx),It ,v[P[prune_ind]] , 20,pconsts,consts,alpha,beta,v,logdetLam, p, seed   )


    ins_ind = I[ii] 

    v[P[prune_ind]] = tt
    
    insert_branch(prune_ind,ins_ind,Cn,Pn)


    return (Cn,Pn)
#    eps = 10**-40
#    X = np.linspace(vmin+eps,vmax-eps,1000)
#    Y = np.zeros(len(X))
#    Pr = np.zeros(len(X))
#    LL = np.zeros(len(X))
#    PL = np.zeros(len(X))
#    Gd = np.zeros(len(X))
#    Pr2 = np.zeros(len(X))
#    LL2 = np.zeros(len(X))
#
#    for xind in range(len(X)):
#        Y[xind] = func(X[xind])
#        Pr[xind] = logPriors[t_ind](X[xind])
#        LL[xind] = logLike[t_ind](X[xind])
##        PL[xind] = poly(X[xind])
##        Gd[xind] = grad(X[xind])
##        Pr2[xind] = prior(X[xind]) 
##        LL2[xind] = like(X[xind]) 
#
#    plt.figure(1)
#    plt.subplot(231)
#    plt.plot(X,Y,'r-')
#    if len(roots)==1:
#        plt.plot(roots[0],func(roots[0]),'r*')
#    plt.subplot(232)
#    plt.plot(X,Pr-max(Pr),'g-')
#    plt.plot(X,Pr2-max(Pr2),'m-')
#    plt.subplot(233)
#    plt.plot(X,LL-max(LL),'b-')
#    #plt.plot(X,LL2-max(LL2),'m-')
#    plt.subplot(234)
#    plt.plot(X,np.exp(Y-max(Y)),'r-')
#    #plt.plot(X,PL,'b-')
#    #plt.plot(X,Gd,'g-')
#    if len(roots)==1:
#        plt.plot(roots[0],np.exp(func(roots[0])-max(Y)),'r*')
#    plt.subplot(235)
#    #plt.plot(X,PL,'b-')
#    plt.plot(X,np.exp(Pr-max(Pr)),'g-')
#    plt.subplot(236)
#    plt.plot(X,np.exp(LL-max(LL)),'b-')
#    #plt.plot(X,Gd,'g-')
#    assert(not len(roots) > 1) #multiple roots is unlikely, lets see what's going on here

#    return (X,Y,Pr,LL)

#Prune branch from tree to be resampled back in.  Note this does not update M
def prune_branch(ind,C,P):
    pp = P[ind]
    gp = P[pp]
    
    gp_llrr = np.nonzero(C[gp,:] == pp)[0][0]
    pp_llrr = np.nonzero(C[pp,:] == ind)[0][0]

    #ll = list(C[pp,:])
    #ll.remove(ind)
    #pp_sibling = ll[0] #list(C[pp,:]).remove(ind)

    pp_sibling = C[pp,1-pp_llrr]

    C[gp,gp_llrr] = pp_sibling 
    P[pp_sibling] = gp

    C[pp, 1-pp_llrr] = -1
    P[pp] = -1

def insert_branch(pruned_ind,ins_ind,C,P):
    gp = P[ins_ind]
    pp = P[pruned_ind]

    gp_llrr = np.nonzero(C[gp,:] == ins_ind)[0][0]
    pp_llrr = np.nonzero(C[pp,:] != pruned_ind)[0][0]

    C[gp,gp_llrr] = pp
    C[pp,pp_llrr] = ins_ind

    P[ins_ind] = pp
    P[pp] = gp

#prior probability of adding the subtree at pruned_ind above ins_ind
def jump_priors(prune_ind, C,P,v,M, alpha, beta):
    tnm1 = len(P)

    logPriors = np.zeros(tnm1,dtype=np.object)
    priorConsts = np.zeros( (tnm1,5),dtype=np.double)

    jump_priors_h(prune_ind, tnm1-1, C,P,v,M, alpha, beta, logPriors, priorConsts, 0)

    return (logPriors,priorConsts)

def jump_priors_h(  prune_ind, ins_ind, C,P,v,M, l_alpha, l_beta, logPriors, priorConsts, logMdiff):

    tnm1 = len(P)
    N = (tnm1+1)/2

    if ins_ind == tnm1-1:
        def prior_f( vnew): 
            return -np.inf
        ll = ins_ind #C[ins_ind,0]
        rr = prune_ind #C[ins_ind,1]

        priorConsts[ins_ind,:] = [ll,rr,-1, -np.inf,1]
        logPriors[ins_ind] = prior_f #-np.inf #don't add above root
        ll = C[ins_ind,0]
        rr = C[ins_ind,1]
        lMdiff = logMdiff - np.log(M[ins_ind]+M[prune_ind])+np.log(M[ins_ind]) 
        jump_priors_h( prune_ind, ll, C,P,v,M, l_alpha, l_beta, logPriors, priorConsts, lMdiff )
        jump_priors_h( prune_ind, rr, C,P,v,M, l_alpha, l_beta, logPriors, priorConsts, lMdiff )

        return
    #end

    pp = P[ins_ind]

    Bold = 1-v[ins_ind]/v[pp]

    #beta_oprobs = sts.beta.logpdf(Bold,l_alpha,l_beta)
    beta_oprobs = beta_logpdf(Bold,l_alpha,l_beta)

    #a single leaf attached to the root always has branch length = 1
    if Bold == 1:
        beta_oprobs = 0
    #end

#    if np.isnan(beta_oprobs):
#        beta_oprobs = -np.inf
    
    assert( not np.isinf(logMdiff) and not np.isnan(logMdiff) )

    assert not np.isposinf(beta_oprobs)
    assert not np.isnan(beta_oprobs)

    ll = ins_ind #C[ins_ind,0]
    rr = prune_ind #C[ins_ind,1]
    pp = P[ins_ind]

    #vnew = (v[pp] + np.max(v[[ll,rr]]))/2 
    def prior_f( vnew, debug = False):
        ll = ins_ind #C[ins_ind,0]
        rr = prune_ind #C[ins_ind,1]
        pp = P[ins_ind]

        B1 = 1-vnew/v[pp]
        #Blist.append(B1)

        beta_probs = beta_logpdf(B1,l_alpha,l_beta)

        #leaves have no associated beta rv
        if v[ll] > 0:
            B2 = 1-v[ll]/vnew
            beta_probs += beta_logpdf(B2,l_alpha,l_beta)
        #end
        if v[rr] > 0:
            B3 = 1-v[rr]/vnew
            beta_probs += beta_logpdf(B3,l_alpha,l_beta)
        #end

        if debug:
            print B1
            print ll
            print rr
            print pp
            print vnew
            print v[pp]

        #beta_probs = beta_logpdf(np.array(Blist),l_alpha,l_beta)
        #beta_probs = sts.beta.pdf(Blist , l_alpha,l_beta)
        #beta_probs = np.log(beta_probs)
        

        #assert np.any( np.array(Blist) == 1) or np.any(np.array(Blist) == 0)
        #assert not np.any(np.isposinf(beta_probs)) and not np.any(np.isnan(beta_probs))

        assert not np.isposinf(logMdiff)
        
        res = beta_probs -beta_oprobs + logMdiff

        #assert not np.isposinf(res)
        #assert not np.isnan(res)

        return res 

    logPriors[ins_ind] = prior_f #np.sum(beta_probs)-beta_oprobs + logMdiff

    priorConsts[ins_ind,:] = [ll,rr,pp, logMdiff-beta_oprobs,1]


    if ins_ind >=N:
        ll = C[ins_ind,0]
        rr = C[ins_ind,1]

        lMdiff = logMdiff - np.log(M[ins_ind]+M[prune_ind])+np.log(M[ins_ind]) 

        jump_priors_h( prune_ind, ll, C,P,v,M, l_alpha, l_beta, logPriors, priorConsts, lMdiff )
        jump_priors_h( prune_ind, rr, C,P,v,M, l_alpha, l_beta, logPriors, priorConsts, lMdiff )
    #end


def jump_likelihoods(pruned_ind, C,P,v,k,Mu_u, nu_u, Mu_d, nu_d, Mu_u_old, nu_u_old, logZ_up, logZ_dn, logZ_old, debug=True):
    tnm1 = len(P)

    logLike = np.zeros(tnm1-1,dtype=np.object)
    llConsts = np.zeros( (tnm1-1, 4,3) ,dtype=np.double)

    for ii in range(tnm1-1):
        if debug:
            logLike[ii] = jump_likelihood(pruned_ind, ii, C,P,v,k, Mu_u, nu_u, Mu_d, nu_d, Mu_u_old, nu_u_old, logZ_up, logZ_dn, logZ_old, consts = llConsts[ii],debug=False)   
        else:
            util.jump_likelihood_fast(pruned_ind, ii, C,P,v,k, Mu_u, nu_u, Mu_d, nu_d, Mu_u_old, nu_u_old, logZ_up, logZ_dn, llConsts[ii])

            #jump_likelihood(pruned_ind, ii, C,P,v,k, Mu_u, nu_u, Mu_d, nu_d, Mu_u_old, nu_u_old, logZ_up, logZ_dn, logZ_old, consts = llConsts[ii])   

    return (logLike,llConsts)
 
#likelihood of adding the subtree starting at pruned_ind above ins_ind
def jump_likelihood(pruned_ind, ins_ind, C,P,v,k, Mu_u, nu_u, Mu_d, nu_d, Mu_u_old, nu_u_old, logZ_up, logZ_dn, logZ_old, consts = None, debug=False):

    ins_pp = P[ins_ind]

    p = len(Mu_u[0])

    ll = C[ins_pp,0]
    rr = C[ins_pp,1]
    pp = P[ins_pp]

    if pp == -1:
        lZ_o = logZ_up[ins_pp]

        ll = ins_ind
        rr = pruned_ind
        pp = ins_pp
       
        #vnew = (v[pp] + np.max(v[[ll,rr]]))/2 


        Mus = np.c_[Mu_u[ins_ind,:] , Mu_u_old[pruned_ind], Mu_d[ins_ind,:]].T
        nus = np.r_[nu_u[ins_ind], nu_u_old[pruned_ind], nu_d[ins_ind]].T

        dists = util.compute_distances_p(Mus,k)


        if not consts == None:
            consts[0] = [ins_ind,pruned_ind,ins_pp]
            consts[1] = dists
            consts[2] = nus
            consts[3,0] = -lZ_o
#            consts.append( [ins_ind, pruned_ind, ins_pp] )
#            consts.append(constants)
#            #consts.append( Mus )
#            consts.append( nus )
        #end

        if debug:
            mfunc = compute_message(Mus,nus,k*np.eye(p),distances = dists, debug=True)
            def ll_f(vnew):
                tll = vnew-v[ll]
                trr = vnew-v[rr]
                tpp = v[pp]-vnew
                ss = np.array([tll,trr,tpp])
            
                (lZ_n,Mu,nu) = mfunc(ss) #compute_message( Mus, nus, Lam,s_in=ss ) 

                LL = lZ_n - lZ_o


                return LL 

            #lprob = lZ_n + lZpp - lZ_o
            return ll_f
    else:
        tll = v[ins_pp]-v[ll]
        trr = v[ins_pp]-v[rr]
        tpp = v[pp] - v[ins_pp]

        Mus = np.c_[Mu_u[[ll,rr],:].T, Mu_d[pp,:]].T
        nus = np.r_[nu_u[[ll,rr]], nu_d[pp]]
        ss = np.array([tll,trr,tpp])

        (lZ_o,Mu,nu) = util.compute_message( Mus, nus, k, ss ) 
        lZpp = logZ_dn[ins_ind]

        ll = ins_ind
        rr = pruned_ind
        pp = ins_pp

        #vnew = (v[pp] + np.max(v[[ll,rr]]))/2 
        Mus = np.c_[Mu_u[ins_ind,:] , Mu_u_old[pruned_ind], Mu_d[ins_ind,:]].T
        nus = np.r_[nu_u[ins_ind], nu_u_old[pruned_ind], nu_d[ins_ind]].T

        
        dists = util.compute_distances_p(Mus,k)

        if not consts == None:
            consts[0] = [ins_ind,pruned_ind,ins_pp]
            consts[1] = dists
            consts[2] = nus
            consts[3,0] = lZpp-lZ_o
#            consts.append( [ins_ind, pruned_ind, ins_pp] )
#            consts.append(constants)
#            #consts.append( Mus )
#            consts.append( nus )
        #end

        if debug:
            mfunc = compute_message(Mus,nus,k*np.eye(p), distances = dists, debug=True )
            def ll_f(vnew):
                tll = vnew-v[ll]
                trr = vnew-v[rr]
                tpp = v[pp]-vnew

                ss = np.array([tll,trr,tpp])
            
                (lZ_n,Mu,nu) = mfunc(ss)

                LL = lZ_n + lZpp - lZ_o
                #assert not np.isnan(res)
                return LL 

            #(lZ_n,Mu,nu) = compute_message( Mus, nus, Lam, s_in=ss ) 

            #lprob = lZ_n + lZpp - lZ_o
            return ll_f

    #end

    #return lprob 

def compute_extrema(p, v, tmin, tmax, alpha, beta, seg, pconstants, constants, sgn = 1, ttol = 0, tinit = 0, direction=0):

    #print sgn
    
    tmn1 = len(v)
    N = (tmn1+1)/2

    assert tmax > tmin
    if ttol == 0:
        ttol = (tmax-tmin)*10**-4

    [ll,rr,pp] = constants[seg][0]

    tll = v[ll]
    trr = v[rr]
    tpp = v[pp]

#    [d_lr,d_rp,d_pl] = constants[seg][1]
#
#    #Mus = constants[2]
#    nus = constants[seg][2]
#
#    nu_ll = nus[0]
#    nu_rr = nus[1]
#    nu_pp = nus[2]
#
#
#    dt = d_rp+d_pl - d_lr
#   
#    nu_pt = nu_pp+tpp
#    nu_lt = nu_ll-tll
#    nu_rt = nu_rr-trr
#
#    nut = nu_lt*nu_rt + nu_lt*nu_pt + nu_pt*nu_rt
#    nu_d = nu_pt*d_lr + nu_lt*d_rp + nu_rt*d_pl
#
#
#
#    nu_s = lambda t : nut+2*nu_pt*t-t*t

   #   1 -- maximize
   #  -1 -- maximize
   #sgn = -1

#    if ll >= N and rr >= N:
#        grad = lambda t :  ( -k*(nu_pt-t)/nu_s(t)-.5*(dt*nu_s(t)-2*(nu_pt-t)*(nu_d+dt*t))/nu_s(t)**2+ (alpha-1)*( -1/(tpp-t) + tll/(t*(t-tll)) + trr/(t*(t-trr)) ) - (beta-1)/t )
#        h = lambda t :  k*(t*nu_s(t)+(nu_pt-t)**2)/nu_s(t)**2 + 2*dt*(nu_pt-t)/nu_s(t)**2 - (4*(nu_pt-t)**2+nu_s(t))*(nu_d + dt*t)/nu_s(t)**3 + (alpha-1)*(-1/(t-tpp)**2 - tll*(2*t-tll)/(t**2*(t-tll)**2) - trr*(2*t-trr)/(t**2*(t-trr)**2))+(beta-1)/t**2
#
#    elif ll < N and rr < N:
#        grad = lambda t : (-k*(nu_pt-t)/nu_s(t)-.5*(dt*nu_s(t)-2*(nu_pt-t)*(nu_d+dt*t))/nu_s(t)**2+ (alpha-1)*( -1/(tpp-t)  ) + (beta-1)/t )
#        h = lambda t : k*(t*nu_s(t)+(nu_pt-t)**2)/nu_s(t)**2 + 2*dt*(nu_pt-t)/nu_s(t)**2 - (4*(nu_pt-t)**2+nu_s(t))*(nu_d + dt*t)/nu_s(t)**3 + (alpha-1)*(-1/(t-tpp)**2 )-(beta-1)/t**2
#    else:
#        tnz = max(tll,trr)
#        grad = lambda t : (-k*(nu_pt-t)/nu_s(t)-.5*(dt*nu_s(t)-2*(nu_pt-t)*(nu_d+dt*t))/nu_s(t)**2+ (alpha-1)*( -1/(tpp-t) + tnz/(t*(t-tnz))  )  )
#        h = lambda t : k*(t*nu_s(t)+(nu_pt-t)**2)/nu_s(t)**2 + 2*dt*(nu_pt-t)/nu_s(t)**2 - (4*(nu_pt-t)**2+nu_s(t))*(nu_d + dt*t)/nu_s(t)**3 + (alpha-1)*(-1/(t-tpp)**2 - tnz*(2*t-tnz)/(t**2*(t-tnz)**2) )
#    #end
#

#    pdf = lambda t : sgn*util.eval_pdf_p(t, seg, pconstants, constants, alpha, beta, v, 0, k) 

#    topt = opt.fmin_bfgs(pdf, (tmax+tmin)/2, fprime = grad)


#    if sgn == 1:
#        numiter = 100
#    else:
#        numiter = 500

#    topt = opt.fminbound(pdf,tmin,tmax,xtol = ttol, maxfun = numiter )

    if tinit == 0:
        tinit = (tmax+tmin)/2


    topt = util.newton_raphson_gaussian(tmin,tmax,tinit,seg,pconstants,constants,alpha,beta,v,p,tol=10.0**-8,step=1,sgn=sgn) 
#    print (tmin,tmax,tinit,topt,sgn)
#    pdfopt = util.eval_pdf_p(topt,seg,pconstants,constants,alpha,beta,v,0,p)
#    pdfinit = util.eval_pdf_p(tinit,seg,pconstants,constants,alpha,beta,v,0,p)
#    pdfmin = util.eval_pdf_p(tmin,seg,pconstants,constants,alpha,beta,v,0,p)
#    pdfmax = util.eval_pdf_p(tmax,seg,pconstants,constants,alpha,beta,v,0,p)
#
#    print(pdfmin,pdfmax,pdfinit,pdfopt)

    if topt == tmin:
        return ([tmin],[])
    elif topt == tmax:
        return ([tmax],[]) 
    else:

        pdf0 = util.eval_pdf_p(topt,seg,pconstants,constants,alpha,beta,v,0,p)

        if direction <= 0:

            opt11 = [tmin]
            opt12 = []
            if (topt-tmin)/tmax > 10**-4:
                tinit_l = topt-ttol

                if -sgn*(util.eval_pdf_p(tinit_l,seg,pconstants,constants,alpha,beta,v,0,p) -pdf0) < 0 and tinit_l > tmin:
                    tinit_l = util.linesearch_gaussian( topt, -ttol, tmin, -sgn, seg,pconstants,constants,alpha,beta,v,p)
                #end

                #print "tinit_l: %0.16f" % tinit_l
#                while -sgn*(util.eval_pdf_p(tinit_l,seg,pconstants,constants,alpha,beta,v,0,p) -pdf0) > 0 and tinit_l > tmin:
#                    tinit_l -= ttol
#                #end
                if tinit_l > tmin: 
                    (opt11,opt12) = compute_extrema(p, v, tmin, topt, alpha, beta, seg, pconstants, constants, sgn = -sgn, ttol = ttol, tinit=tinit_l,direction = -1)
                #end
            #end
        else:
            opt11 = []
            opt12 = []


        if direction >= 0:
            opt21 = [tmax]
            opt22 = []
            if (tmax-topt)/tmax > 10**-4:
                tinit_r = topt+ttol

                if -sgn*(util.eval_pdf_p(tinit_r,seg,pconstants,constants,alpha,beta,v,0,p) - pdf0) < 0 and tinit_r < tmax:
                    tinit_r = util.linesearch_gaussian( topt, ttol, tmax, -sgn, seg,pconstants,constants,alpha,beta,v,p)
                #end
                #print "tinit_r: %0.16f" % tinit_r
                pdfinit = util.eval_pdf_p(tinit_r,seg,pconstants,constants,alpha,beta,v,0,p)

                if sgn > 0:
                    assert( pdfinit < pdf0)

#                while -sgn*(util.eval_pdf_p(tinit_r,seg,pconstants,constants,alpha,beta,v,0,p) - pdf0) > 0 and tinit_r < tmax:
#                    tinit_r += ttol
#                #end

                if tinit_r < tmax:
                    (opt21,opt22) = compute_extrema(p, v, topt, tmax, alpha, beta, seg, pconstants, constants, sgn = -sgn, ttol = ttol, tinit=tinit_r, direction=1)
                #end
            #end
        else:
            opt21 = []
            opt22 = []

        opt12.extend(opt22)
        opt12.append(topt)
        opt11.extend(opt21)
        return (opt12,opt11)


def update_hyp_variance(X,Mu,nu,C,P,v,kappa, gamma,k ):

    (tnm1,p) = np.shape(Mu)
    N = (tnm1+1)/2


    sX = util.sample_locations(X,Mu,nu,C,P,v,k) #np.zeros((tnm1,p))


    total_dist = 0
    Q = deque( [tnm1-1] )
    while len(Q) > 0:
        par = Q.popleft()
        ll = C[par,0]
        rr = C[par,1]

        Mu_pp = sX[par]

        Mu_ll = sX[ll]
        Mu_rr = sX[rr]
        s_ll = v[par] - v[ll]
        s_rr = v[par] - v[rr]

        mpl = Mu_ll-Mu_pp
        mpr = Mu_rr-Mu_pp

        dpl = np.dot(mpl,mpl)/s_ll
        dpr = np.dot(mpr,mpr)/s_rr

        total_dist += dpl+dpr 
 
        if ll >= N: 
            Q.append(ll)
        if rr >= N:
            Q.append(rr)

    #end

    shape = (2*N-1)*p/2.0 + kappa
    rate = total_dist/2 + gamma

 
    k = sts.invgamma.rvs(shape,scale=rate)

    return k 




def update_hyp_variance_mgzed(Mus,nus,C,v,kappa, gamma ):

    (tnm1,p) = np.shape(Mus)
    N = (tnm1+1)/2

    Q = deque( [tnm1-1] )
    total_dist = 0

    while len(Q) > 0:
        par = Q.popleft()
        
        ll = C[par,0]
        rr = C[par,1]

        Mu_ll = Mus[ll,:]
        Mu_rr = Mus[rr,:]

        nu_ll = nus[ll]
        nu_rr = nus[rr]

        s_ll = v[par] - v[ll]
        s_rr = v[par] - v[rr]

        mlr = Mu_ll-Mu_rr

        dlr = np.dot(mlr,mlr)/(nu_ll+nu_rr+s_ll+s_rr) 

        total_dist += dlr 
  
        if ll >= N: 
            Q.append(ll)
        if rr >= N:
            Q.append(rr)

    #end

    shape = (N-1)*p/2.0 + kappa
    rate = total_dist/2 + gamma

     
    k = sts.invgamma.rvs(shape,scale=rate)
    #kinv = sts.gamma.rvs(shape,scale=1/rate)
    #print (shape,rate,kinv)
    return k 


def update_noise_model(X,Y, df, sig0_2, a0,b0,lam, Mu, nu, C, P, v, k ):
    (N,p) = np.shape(X)

    sX = util.sample_locations(X,Mu,nu,C,P,v,k,sample_leaves=True)

    dists = np.sum( (X-sX[:N])*(X-sX[:N]),1)

    shape = (p+df)/2
    rate = (df*sig0_2 + dists)/2 

#    shape = (df)/2
#    rate = (df*sig0_2 )/2 

    sig2_t = sts.invgamma.rvs(shape,scale=rate)

    (df,sig0_2) = util.update_df_sig0(df,sig0_2,X,sig2_t,lam,a0,b0,20,20,update_df=False)

    (df0,l0,s0) = sts.invgamma.fit(sig2_t,floc=0)

    print (df,df0, sig0_2,s0)

    #print (shape,rate[0],dists[0])

#    prec = sts.gamma.rvs(shape,scale=1/rate)
#    sig2_t = 1/prec

    return (sX[:N], df, sig0_2, sig2_t)    

#end


def newton_raphson(f,fprime, hess, xmin,xmax, x0, step=0, tol = 10**-10, sgn = 1):
    if step == 0:
        step = xmax-xmin
  
    xlast = -np.inf 
    x = x0

    
    fp = fprime(x)
    fp0 = fp

    h = np.abs(hess(x))
    h0 = h


    itr = 0
    #print (xmin,x0,xmax)
    while (np.abs(fp/h) > tol and sgn == 1) or (np.abs(fp) > tol and sgn == -1) or itr < 3:
        itr+= 1
        xlast = x
        x = x + sgn*step*fp/h
        fx = f(x)
        #print (x, fx, fp, hess(x))

        if x < xmin:
            x = xlast
            if sgn == -1:
                return xmin
            else:
                step *= .9
        elif x > xmax:
            x = xlast
            if sgn == -1:
                return xmax
            else:
                step *= .9

        if np.isinf(fx) or np.isnan(fx):
            d1 = x-xmin
            d2 = xmax-x
            if d1 < d2 and sgn == -1:
                return xmin
            elif sgn == -1:
                return xmax
            else:

                step *= .9

#                ff = lambda x : -f(x)
#                ffprime = lambda x : -fprime(x)
#
#                a0 = opt.line_search(f,fprime,xlast,sgn*fp,gfk=-fp)
#                x = x + a0*fp
#
#                return xlast

        fp = fprime(x)
        h = np.abs(hess(x))
        if np.abs(fp) > 10**5 and h > h0*10 and sgn == -1: #if we are diverging, set a constant step size to allow moving out of the boundaries
            h = h0#np.abs()
    #end
   
    return x 

def compute_roots(k, v, alpha,beta, constants, highprec = False):

    tmn1 = len(v)
    N = (tmn1+1)/2

    [ll,rr,pp] = constants[0]

    tll = v[ll]
    trr = v[rr]
    tpp = v[pp]

    [d_lr,d_rp,d_pl] = constants[1]

    #Mus = constants[2]
    nus = constants[2]

    nu_ll = nus[0]
    nu_rr = nus[1]
    nu_pp = nus[2]


    dt = d_rp+d_pl - d_lr
   
    nu_pt = nu_pp+tpp
    nu_lt = nu_ll-tll
    nu_rt = nu_rr-trr

    nut = nu_lt*nu_rt + nu_lt*nu_pt + nu_pt*nu_rt
    nu_d = nu_pt*d_lr + nu_lt*d_rp + nu_rt*d_pl

    a = np.zeros(5)  #,np.float128)
    b = np.zeros(5)  #,np.float128)
    c = np.zeros(5)  #,np.float128)
    d = np.zeros(5)  #,np.float128)

    a[0] = nu_pt*nu_d-k*nut*nu_pt-dt*nut/2
    a[1] = k*nut-2*k*nu_pt*nu_pt-nu_d
    a[2] = 3*k*nu_pt - dt/2
    a[3] = -k

    c[0] = nut*nut
    c[1] = 4*nu_pt*nut
    c[2] = 4*nu_pt*nu_pt-2*nut
    c[3] = -4*nu_pt
    c[4] = 1

    #only internal nodes have Betas associated with them -- leaf branch lengths are fully determined by them
    if ll >= N and rr >= N:
        tt1 = tll*trr*tpp
    #    tt1 = np.exp(np.sum(np.log([tll,trr,tpp])))

        tt2 = tll*trr + tll*tpp + tpp*trr
        tt3 = tll+trr+tpp

        b[1] = -tt1
        b[2] = tt2
        b[3] = -tt3
        b[4] = 1

        d[0] = (2*alpha+beta-3)*tt1
        d[1] = -(alpha+beta-2)*tt2
        d[2] = (beta-1)*tt3
        d[3] = alpha - beta

        degree = 7
    elif ll < N and rr < N:
        b[1] = -tpp
        b[2] = 1

        d[0] = -(beta-1)*tpp
        d[1] = (alpha+beta-2)

        degree = 5
    else:
        tnz = max(tll,trr)

        b[1] = tpp*tnz
        b[2] = -(tpp+tnz)
        b[3] = 1

        d[0] = -(alpha-1)*tnz*tpp
        d[2] = (alpha-1)

        degree = 6
    #end

    M = a.reshape(5,1).dot(b.reshape(1,5)) + c.reshape(5,1).dot(d.reshape(1,5))

    A = np.zeros(degree+1)
    AA = np.zeros(degree+1,np.float128)
    B = np.zeros(degree+1,np.float128)
    for i in range(5):
        for j in range(5):
            if not M[i,j] == 0:
                A[degree-(i+j)] += M[i,j]
                AA[degree-(i+j)] += M[i,j]
                B[i+j] += M[i,j]
            #end
        #end
    #end



    poly = np.poly1d(A)

    rts = np.roots(A)
    rts = np.real(rts[np.nonzero(np.isreal(rts))[0]])
    rts = np.sort(rts)

    #assert not( ll >= N and rr >= N)
    if ll >= N and rr >= N:
        bpass = False
        for rt in rts:
            if max(tll,trr) < rt and rt < tpp:
                bpass = True

        nu_s = lambda t : nut+2*nu_pt*t-t*t
        grad = lambda t : -k*(nu_pt-t)/nu_s(t)-.5*(dt*nu_s(t)-2*(nu_pt-t)*(nu_d+dt*t))/nu_s(t)**2+ (alpha-1)*( -1/(tpp-t) + tll/(t*(t-tll)) + trr/(t*(t-trr)) ) - (beta-1)/t

        ttt = lambda t : (t-tpp)*(t-tll)*(t-trr)

        grad2 = lambda t : -k*(nu_pt-t)*nu_s(t)*ttt(t)*t -.5*(dt*nu_s(t)-2*(nu_pt-t)*(nu_d+dt*t))*ttt(t)*t + nu_s(t)**2*( (alpha-1)*(t**3-t*tt2+2*tt1) - (beta-1)*ttt(t))
        grad3 = lambda t : -k*(nu_pt-t)*nu_s(t)*ttt(t)*t -.5*(dt*nu_s(t)-2*(nu_pt-t)*(nu_d+dt*t))*ttt(t)*t + nu_s(t)**2*( (alpha-1)*( -1/(tpp-t) + tll/(t*(t-tll)) + trr/(t*(t-trr)) ) - (beta-1)/t)*ttt(t)*t

        grad_correct = lambda t : grad(t)*nu_s(t)**2*ttt(t)*t


        
        #mpmroots = mpm.polyroots(AA)

        assert bpass

        print N
        print ll
        print rr
        print pp
        print A
        print rts
        print constants[1]
        print constants[2]

    
#    if highprec:
#        mpm.dps = 60
#        rts = mpm.polyroots(A)
#        np.set_printoptions(precision=60)

#    for i in range(len(rts)):
#        t = rts[i]
#        nu_s1 = nut+2*nu_pt*t-t*t
#        nu_ps = nu_pp + tpp-t
#        nu_ls = nu_ll + t-tll
#        nu_rs = nu_rr + t-trr
#        
#        nu_s2 = nu_ps*nu_ls + nu_ps*nu_rs + nu_ls*nu_rs
#
#        print '\nnu_s:'
#        print nu_s1-nu_s2
#
#        nu_s = nu_s1
#
#
#        d = d1+d2+d3
#        print '\nNumpy:'
#        print('%.60f' % t)
#        print('%.60f' % d)


#        t = rts3[i]
#        nu_s = nut+2*nu_pt*t-t*t
#        d1 = -k*(nu_pt-t)/nu_s
#        d2 = -.5*(dt*nu_s-2*(nu_pt-t)*(nu_d+dt*t))/nu_s**2
#        d3 = alpha*( -1/(tpp-t) + tll/(t*(t-tll)) + trr/(t*(t-trr)) ) - beta/t
#        print '\nmpmath:'
#        print t
#        print d1+d2+d3
#
#    nu_s = lambda t : nut+2*nu_pt*t-t*t
#
#    grad = lambda t : -k*(nu_pt-t)/nu_s(t)-.5*(dt*nu_s(t)-2*(nu_pt-t)*(nu_d+dt*t))/nu_s(t)**2+ (alpha-1)*( -1/(tpp-t) + tll/(t*(t-tll)) + trr/(t*(t-trr)) ) - (beta-1)/t
#
#    prior = lambda t : (alpha-1)*(np.log(1-t/tpp)+np.log(1-tll/t)+np.log(1-trr/t)) + (beta-1)*(np.log(t/tpp) + np.log(tll/t) + np.log(trr/t))
#
#    like = lambda t : -.5*k*np.log(nu_s(t)) -.5*( (nu_pt-t)*d_lr + (nu_rt+t)*d_pl + (nu_lt+t)*d_rp)/nu_s(t)

    return rts  #,poly,grad,prior,like)

#ctypedef np.double_t DTYPE_t
#
#cdef double eval_pdf( double vnew, int seg, np.ndarray[DTYPE_t, ndim=2] priorConsts, np.ndarray[DTYPE_t, ndim=3] llConsts, double alpha, double beta, np.ndarray[DTYPE_t] v, double logdetLam, int p):
#
#    cdef int ll = <int>priorConsts[seg,0]
#    cdef int rr = <int>priorConsts[seg,1]   
#    cdef int pp = <int>priorConsts[seg,2]
#    cdef double pC = priorConsts[seg,3]
# 
#    cdef double B1 = 1-vnew/v[pp]
#    #Blist.append(B1)
#
#    cdef double beta_probs = beta_logpdf(B1,alpha,beta)
#
#    #leaves have no associated beta rv
#    if v[ll] > 0:
#        B1 = 1-v[ll]/vnew
#        beta_probs += beta_logpdf(B1,alpha,beta)
#    #end
#    if v[rr] > 0:
#        B1 = 1-v[rr]/vnew
#        beta_probs += beta_logpdf(B1,alpha,beta)
#    #end
#
#    cdef double prior = beta_probs + pC
#
#    cdef double lC = llConsts[seg,3,0]
#
#    cdef double d12 = llConsts[seg,1,0]     
#    cdef double d23 = llConsts[seg,1,1]     
#    cdef double d31 = llConsts[seg,1,2]     
#   
#    cdef double tll = vnew-v[ll]
#    cdef double trr = vnew-v[rr]
#    cdef double tpp = v[pp]-vnew
# 
#    cdef double nu1 = llConsts[seg,2,0]+tll
#    cdef double nu2 = llConsts[seg,2,1]+trr
#    cdef double nu3 = llConsts[seg,2,2]+tpp
#
#    cdef double nu_s = nu1*nu2+nu2*nu3+nu3*nu1 
#
#    cdef double lZ = 0
#
#    if nu_s != 0:
#        lZ = -( p*log(2*M_PI)+logdetLam + .5*p*log(nu_s) + .5*(d12*nu3/nu_s+d23*nu1/nu_s+d31*nu2/nu_s))
#    else:
#        lZ = -np.inf
#
#    cdef double LL = lZ+lC
#
#    return prior+LL 

def beta_logpdf(x, alpha, beta):

    if x == 0 or x == 1:
        return -np.inf

    return (alpha-1)*np.log(x)+(beta-1)*np.log(1-x)

#    aterm = (alpha-1)*np.log(x)
#    bterm = (beta-1)*np.log(1-x)
#
#    terms = np.c_[aterm,bterm]
#
#    terms[np.nonzero(np.isnan(terms))] = -np.inf
#
#    return np.sum(terms,1)

#multiply numbers in a numerically stable way
def st_mul(x):
    logx = np.log(x)
    sx = np.sum(logx)


if __name__ == "__main__":
    p = 20
    X = np.random.rand(5000,p)
   
    cProfile.runctx('beta_tree_mcmc(X,2,2,1,2,plot=False)',globals(),locals(),"flink.prof")


