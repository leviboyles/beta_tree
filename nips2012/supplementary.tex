\documentclass{article} % For LaTeX2e
\usepackage{amsmath,amssymb,amsthm,fancyhdr,graphicx,subfig}
%\documentstyle[nips12submit_09,times,art10]{article} % For LaTeX 2.09

\newcommand{\Mt}{\tilde{M}}
\newcommand{\Poisson}{\mathrm{Poisson} }
\newcommand{\Beta}{\mathrm{Beta} }
\newcommand{\ancestors}{\mathrm{An} }
\newcommand{\descendants}{\mathrm{De} }

\newcommand{\bx}{{\bf x}}
\newcommand{\by}{{\bf y}}
\newcommand{\hy}{\hat{y}}
\newcommand{\hL}{\hat{\Lambda}}

\newcommand{\tnu}{\tilde{\nu}}
\newcommand{\td}{\tilde{d}}
\newcommand{\tv}{\tilde{t}}

\title{Supplementary Material}


%\author{
%Levi Boyles\\
%Department of Computer Science\\
%University of California, Irvine\\
%Irvine, CA 92617 \\
%\texttt{lboyles@uci.edu} \\
%\And
%Max Welling \\
%Department of Computer Science\\
%University of California, Irvine\\
%Irvine, CA 92617 \\
%\texttt{welling@uci.edu} \\
%}

% The \author macro works with any number of authors. There are two commands
% used to separate the names and addresses of multiple authors: \And and \AND.
%
% Using \And between authors leaves it to \LaTeX{} to determine where to break
% the lines. Using \AND forces a linebreak at that point. So, if \LaTeX{}
% puts 3 of 4 authors names on the first line, and the last on the second
% line, try using \AND instead of \And before the third author name.

\newcommand{\fix}{\marginpar{FIX}}
\newcommand{\new}{\marginpar{NEW}}
\newtheorem{mylem}{Lemma}
\newtheorem{mythm}{Theorem}

%\nipsfinalcopy % Uncomment for camera-ready version

\begin{document}


\maketitle


\section{Proofs}

\begin{mylem}
A tree $\psi_n$ has $T(\psi_n) = \frac{(n-1)!}{\prod_{i=1}^{n-1} m_i }$ possible orderings on its internal nodes, where $m_i$ is the number of internal nodes in the subtree rooted at node $i$.
\end{mylem}

\begin{proof}
This can be done with a recursion relation.  Consider an unordered tree $\psi_n$ with $n_l$ nodes in the left subtree, $n_r$ in the right, and define $m_i = n_i-1$ to be the number of internal nodes of subtree $i$.  Additionally, there are $k_l$ possible orderings of the left subtree's internal nodes, and $k_r$ possible orderings of the right subtree's internal nodes.  Consider the case where $k_l=k_r=1$, where we need to count the number of ways in which the two sequences of length $m_r$ and $m_l$ can be "interleaved".  There are $(m_r+m_l)!$ possible orderings of the $m_r+m_l$ nodes if we have no constraints on the orderings of the right nodes with respect to each other, and no such constraints for the left nodes.  Thus, the total number of ways to interleave the two sequences is $\frac{(m_r+m_l)!}{m_r! m_l!} = {m_r+m_l \choose m_r}$.  See Figure \ref{fg:unbalanced}.  For $k_r,k_l > 1$, we simply multiply these in, giving the total number of orderings as:
\[ {m_r +m_l \choose m_r} k_r k_l \]
Note we can apply the same logic to the right and left subtrees to determine $k_r$ and $k_l$.  Given the children of node $i$ are $l_i$ and $r_i$, the number of orderings $k_i$ consistent with a particular subtree $i$ is:
\[  k_i = {m_{r_i} + m_{l_i} \choose m_{r_i}} k_{r_i} k_{l_i} \]
So we can write out the total number of orderings as a product over all internal nodes of the tree:
\[ T(\psi_n) = \prod_{i=1}^{n-1} {m_{r_i} + m_{l_i} \choose m_{r_i} } \]
Furthermore, note that $m_i = m_{l_i}+ m_{r_i}+1$ (the number of internal nodes in a tree is the sum of those in the subtrees, plus the root), so the denominator of the binomial coefficients of a parent node will cancel the numerators of the coefficients of its children, however leaving a term proportional to $\frac{1}{m_i}$, and the numerator of the root will not be canceled.  Thus we have:
\[ T(\psi_n) = \frac{(n-1)!}{\prod_{i=1}^{n-1} m_i } \]

\end{proof}



\begin{figure}
\centering
\includegraphics[width=.4\textwidth]{../figures/unbalanced_subtrees.eps}
\caption{A tree with two unbalanced subtrees.  The internal nodes for each of the subtrees can only be ordered in one way with respect to each other, however, for the overall tree there are $\frac{6!}{3!3!} = {6 \choose 3} = 20$ orderings of the internal nodes.}
\label{fg:unbalanced}
\end{figure}



\begin{mythm}
$p(\psi_n)$ defines an exchangeable and consistent prior over $\Psi_n$
\end{mythm}

\begin{proof}
Since $p(\psi_n)$ doesn't depend on the order of data seen, it is exchangeable.  Thus for consistency all we need to show is that we have a well defined conditional prior; ie that when we marginalize out a particular point from $p(\psi_n)$ we get back $p(\psi_{n-1})$, if $\psi_{n-1}$ is a tree structure obtained by removing a leaf from $\psi_n$.
We can do this by showing $\sum_{\psi_{n+1} \in C(\psi_n) } p(\psi_{n+1}) = p(\psi_n)$, where $C(\psi_n)$ is the set of $\psi_{n+1}$ structurally consistent with a particular $\psi_n$.  This is equivalent to showing:
\[ \sum_{\psi_{n+1} \in C(\psi_n) } T(\psi_{n+1}) = {n+1 \choose 2} T(\psi_n) \]
which can be proven by induction, and we take this as our inductive hypothesis for all $l < n$.  The base case for $n=1$: 
\[ T(\psi_{2}) = \frac{(2-1)!}{1! 1!} = 1 = {2 \choose 2} = {2 \choose 2} T(\psi_1) \]
Consider the tree $\psi_n$ and its two subtrees $\psi_{n_l}$ and $\psi_{n_r}$.  As listed before, we have $T(\psi_n) = {m_l + m_r \choose m_l} T(\psi_{n_l}) T(\psi_{n_r})$.  Depending on where we add the $n+1$st point, (denoted $x_{n+1}$) we have:
\[ T(\psi_{n+1}) = \begin{cases} 
{m_l+m_r+1 \choose m_l+1} T(\psi_{n_l+1}) T(\psi_{n_r}) & \mbox{if } x_{n+1} \mbox{ is added to the left subtree}\\
{m_l+m_r+1 \choose m_r+1} T(\psi_{n_l}) T(\psi_{n_r+1}) & \mbox{if } x_{n+1} \mbox{ is added to the right subtree}\\
{m_l + m_r+1 \choose m_l+m_r+1} {m_l+ m_r \choose m_r} T(\psi_{n_l}) T(\psi_{n_r}) &\mbox{if } x_{n+1} \mbox{ is added above the root}
\end{cases}
 \]
Summing over all such $\psi_{n+1}$, and invoking the inductive hypothesis, we have:
\begin{align*} \sum_{\psi_{n+1} \in C(\psi_n)} T(\psi_{n+1}) &= {m_l+m_r+1 \choose m_l+1} {m_l+1 \choose 2} T(\psi_{n_l}) T(\psi_{n_r}) \\
&+ {m_l + m_r +1 \choose m_r +1} {m_r +1 \choose 2} T(\psi_{n_l}) T(\psi_{n_r})\\
&+ (m_l+m_r+1) {m_l + m_r \choose n_l} T(\psi_{n_l}) T(\psi_{n_r})
\end{align*}
\begin{align*}
&= {m_l + m_r \choose m_l} T(\psi_{n_l}) T(\psi_{n_r}) \left(\frac{m_l + m_r + 1}{m_l+1} \frac{(n_l+1)n_l}{2} + \frac{m_l+m_r+1}{m_r+1} \frac{(n_r+1)n_r}{2} + 1  \right)\\
&= T(\psi_n) \left( \frac{ (n-1)(n_l+n_r+2)}{2} + 1 \right)\\
&= T(\psi_n) \left( \frac{ (n-1)(n+2)}{2} + \frac{2}{2} \right)\\
&= {n+1 \choose 2} T(\psi_n)
\end{align*}

\end{proof}



\end{document}
