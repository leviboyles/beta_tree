\documentclass{article} % For LaTeX2e

\usepackage[small]{caption}
\usepackage{nips12submit_e,times}
\usepackage{amsmath,amssymb,amsthm,fancyhdr,graphicx,subfig}
%\documentstyle[nips12submit_09,times,art10]{article} % For LaTeX 2.09

\newcommand{\Mt}{\tilde{M}}
\newcommand{\Poisson}{\mathrm{Poisson} }
\newcommand{\Beta}{\mathrm{Beta} }
\newcommand{\ancestors}{\mathrm{An} }
\newcommand{\descendants}{\mathrm{De} }

\newcommand{\bx}{{\bf x}}
\newcommand{\by}{{\bf y}}
\newcommand{\hy}{\hat{y}}
\newcommand{\hL}{\hat{\Lambda}}

\newcommand{\tnu}{\tilde{\nu}}
\newcommand{\td}{\tilde{d}}
\newcommand{\tv}{\tilde{t}}
\newcommand{\ourprior}{the TMC }
\title{The Time-Marginalized Coalescent Prior for Hierarchical Clustering}


\author{
Levi Boyles\\
Department of Computer Science\\
University of California, Irvine\\
Irvine, CA 92617 \\
\texttt{lboyles@uci.edu} \\
\And
Max Welling \\
Department of Computer Science\\
University of California, Irvine\\
Irvine, CA 92617 \\
\texttt{welling@uci.edu} \\
}

% The \author macro works with any number of authors. There are two commands
% used to separate the names and addresses of multiple authors: \And and \AND.
%
% Using \And between authors leaves it to \LaTeX{} to determine where to break
% the lines. Using \AND forces a linebreak at that point. So, if \LaTeX{}
% puts 3 of 4 authors names on the first line, and the last on the second
% line, try using \AND instead of \And before the third author name.

\newcommand{\fix}{\marginpar{FIX}}
\newcommand{\new}{\marginpar{NEW}}
\newtheorem{mylem}{Lemma}
\newtheorem{mythm}{Theorem}

%\nipsfinalcopy % Uncomment for camera-ready version

\begin{document}


\maketitle

\begin{abstract}
We introduce a new prior for use in Nonparametric Bayesian Hierarchical Clustering.  The prior is constructed by marginalizing out the time information of Kingman's coalescent, providing a prior over tree structures which we call the Time-Marginalized Coalescent (TMC).  This allows for models which factorize the tree structure and times, providing two benefits: more flexible priors may be constructed and more efficient Gibbs type inference can be used.  We demonstrate this on an example model and show we get competitive experimental results.  
\end{abstract}

\section{Introduction}

Hierarchical clustering models aim to fit hierarchies to data, and enjoy the property that clusterings of varying size can be obtained by ``pruning'' the tree at particular levels.  In contrast, standard clustering models must specify the number of clusters beforehand, while NPB clustering models such as the Dirichlet Process Mixture (DPM) \cite{ferguson1983bayesian, lo1984class} directly infer the (effective) number of clusters.  Hierarchical clustering is often used in population genetics for inferring ancestral history and bioinformatics for genetic clustering, and has also seen use in computer vision \cite{salakhutdinovlearning,adams2010tssb} and topic modelling \cite{Bleietal04,adams2010tssb}.

%why hierarchical clustering
Nonparametric Bayesian (NPB) models are a class of models of growing popularity.  Being Bayesian, these models can easily quantify the uncertainty the the resulting inferences, and being nonparametric, they can seamlessly adapt to increasingly complicated data, avoiding the model selection problem.  NPB hierarchical clustering models are an important regime of such models, and have been shown to have superior performance to alternative models in many domains \cite{HellerGhah05}.  Thus, further advances in the applicability of these models is important.

%previous hierarchical clustering
There has been substantial work on NPB models for hierarchical clustering.  Dirichlet Diffusion Trees (DDT) \cite{neal2003dft}, Kingman's Coalescent \cite{Kingman82c,Kingman82g,drummond2007beast,TehDauRoy2008}, and Pitman-Yor Diffusion Trees (PYDT) \cite{knowles2011pitman} all provide models in which data is generated from a Continuous-Time Markov Chain (CTMC) that lives on a tree that splits (or coalesces) according to some continuous-time process.  The nested CRP and DP \cite{Bleietal04, rodriguez2008nested} and Tree-Structured Stick Breaking (TSSB) \cite{adams2010tssb} define priors over tree structures from which data is directly generated.  

%why previous needs to be improved
Although there is extensive and impressive literature on the subject demonstrating its useful clustering properties, NPB hierarchical clustering has yet to see widespred use.  The expensive computational cost typically associated with these models is a likely inhibitor to the adoption of these models.  The CTMC based models are typically more computationally intensive than the direct generation models, and there has been substantial work in improving the speed of inference in these models.  \cite{knowles2011message} introduces a variational approximation for the DDT, and \cite{GorurTeh09a,gorurscalable} provide more efficient SMC schemes for the Coalescent.  The direct generation models are typically faster, but usually come at some cost or limitation; for example the TSSB allows (and requires) that data live at some of its internal nodes. %FIXME
%how ours improves

Our contribution is a new prior over tree structures that is simpler than many of the priors described above, yet still retains the exchangeability and consistency properties of a NPB model.  The prior is derived by marginalizing out the time and ordering in which pairs are joined of the coalescent.  The remaining distribution is an exchangeable and consistent prior over tree structures.  This prior may be used directly with a data generation process, or a notion of time may be reintroduced, providing a prior with a factorization between the tree structures and times.  The simplicity of the prior allows for great flexibility and the potential for more efficient inference algorithms.  For the purposes of this paper, we focus on one such possible model, wherein we generate branch lengths according to a process similar to stick-breaking. 

We introduce the proposed prior on tree structures in Section \ref{sec:coalescent}, the distribution over times conditioned on tree structure in \ref{sec:times}, and the data generation process in \ref{sec:datagen}.  We show experimental results in Section \ref{sec:experiments}, and conclude in Section \ref{sec:conclusion}.

\begin{figure}
\centering
\includegraphics[width=.7\textwidth]{../figures/coalescent_build.eps}
\caption{ Coalescent tree construction {\bf (left)} A pair is uniformly drawn from $N=5$ points to coalesce.  {\bf (middle)}  The coalescence time $t_5$ is drawn from $Exp({5\choose 2})$, and another pair on the remaining 4 points is drawn uniformly.  {\bf (right)} After drawing $t_4 \sim Exp({4 \choose 2})$, the coalescence time for the newly coalesced pair is $t_5+t_4$. } 
\label{fg:Coalescent}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=.45\textwidth]{../figures/marginal_coalescent_build.eps}
\caption{ Consider the trees one might construct by uniformly picking pairs of points to join, starting with four leaves $\{a,b,c,d\}$.  One can join $a$ and $b$ first, and then $c$ and $d$ (and then the two parents), or $c$ and $d$ and then $a$ and $b$ to construct the tree on the left.  By defining a uniform prior over $\phi_n$, and then marginalizing out the order of the internal nodes $\rho$ (equivalently, the order in which pairs are joined), we then have a prior over $\psi_n$ that puts more mass on balanced trees than unbalanced ones.  For example on the tree on right can only be constructed in one way by node joining. }
\label{fg:tree_build}
\end{figure}



\section{Coalescent Prior on Tree Structures}
\label{sec:coalescent}


\subsection{Kingman's Coalescent}
Kingman's Coalescent provides a prior over balanced, edge-weighted trees, wherein the weights are often interpreted as representing some notion of time.  See Figure \ref{fg:Coalescent}.  A coalescent tree can be sampled as follows: start with $n$ points and $n$ dangling edges hanging from them, and all weights set to 0.  Sample a time from $Exp( { n\choose 2})$, and add this value to the weight for each of the dangling edges. Then pick a pair uniformly at random to coalesce (giving rise to their mutual parent, whose new dangling edge has weight 0).   Repeat this process on the remaining $n-1$ points until a full weighted tree is constructed. Note however, that the weights do not influence the choice of tree structures sampled, which suggests we can marginalize out the times and still retain an exchangeable and consistent distribution over trees.  What remains is simply a process in which a uniformly chosen pair of points is joined at every iteration.

\begin{figure}
\centering
\subfloat{ \includegraphics[width=0.24\textwidth]{../figures/beta11_sample.eps} }
\subfloat{ \includegraphics[width=0.24\textwidth]{../figures/beta22_sample.eps} }
\subfloat{ \includegraphics[width=0.24\textwidth]{../figures/beta42_sample.eps} }
\caption{ {\bf (left)} A sample from the described prior with stick-breaking parameter {\cal B}(1,1) (uniform). {\bf (middle)} A sample using {\cal B}(2,2). {\bf (right)} A sample using {\cal B}(4,2). }
\label{fg:samples} 
\end{figure}
\subsection{Coalescent Distribution over Trees}
We consider two types of tree-like structures, generic (rooted) unweighted tree graphs which we denote $\psi_n$ living in $\Psi_n$, and trees of the previous type, but with a specified \emph{ordering} $\rho$ on the internal (non-leaf) nodes of the tree, denoted $(\psi_n,\rho) = \phi_n \in \Phi_n$.  Marginalizing out the times of the coalescent gives a uniform prior over ordered tree structures $\phi_n$.  The order information is necessary because for a given $\psi_n$ there are multiple ways of constructing it by uniformly picking pairs to join, see Figure \ref{fg:tree_build}.  If there are $i$ remaining nodes to join, there are ${i \choose 2}$ ways of joining them, so we have for the probability of a particular $\phi_n$:
\[p(\phi_n) = \prod_{i=2}^n {i \choose 2}^{-1}\] 
This defines an exchangeable and consistent prior over $\Phi_n$; exchangeable because $p(\phi_n)$ does not depend on the order in which the data is seen, and consistent because the conditional prior\footnote{The sequential sampling scheme often associated with NPB models; for example the conditional prior for the CRP is the prior probability of adding the $n+1$st point to one of the existing clusters (or a new cluster) given the clustering on the first $n$ points.} is well defined -- we can imagining adding a new leaf to an existing $\phi_n$, which creates a new internal node.  Let $y_i$ denote the $i$th internal node of $\phi_n$, $i\in\{1...n-1\}$, and let $y^*$ denote the new internal node.  There are $n$ ways of attaching the new internal node below $y_1$, $n-1$ ways of attaching below $y_2$, and so on, giving $\frac{n(n+1)}{2} = {n \choose 2}$ ways of attaching $y^*$ into $\phi_n$.  Thus if we make this choice uniformly at random, we get the probability of the new tree is $p(\phi_{n+1}) = p(\phi_n) {n+1\choose 2}^{-1} =  \prod_{i=2}^{n+1} {i \choose 2}^{-1}$.

It is possible to marginalize out the ordering information in the coalescent tree structures $\phi_n$ to derive exchangeable, consistent priors on ``unordered'' tree structures $\psi_n$.  We can perform this marginalization by counting how many ordered tree structures $\phi_n \in \Phi_n$ are consistent with a particular unordered tree structure $\psi_n$. 

\begin{mylem}
A tree $\psi_n$ has $T(\psi_n) = \frac{(n-1)!}{\prod_{i=1}^{n-1} m_i }$ possible orderings on its internal nodes, where $m_i$ is the number of internal nodes in the subtree rooted at node $i$.
\end{mylem}

%\begin{proof}
%This can be done with a recursion relation.  Consider an unordered tree $\psi_n$ with $n_l$ nodes in the left subtree, $n_r$ in the right, and define $m_i = n_i-1$ to be the number of internal nodes of subtree $i$.  Additionally, there are $k_l$ possible orderings of the left subtree's internal nodes, and $k_r$ possible orderings of the right subtree's internal nodes.  Consider the case where $k_l=k_r=1$, where we need to count the number of ways in which the two sequences of length $m_r$ and $m_l$ can be "interleaved".  There are $(m_r+m_l)!$ possible orderings of the $m_r+m_l$ nodes if we have no constraints on the orderings of the right nodes with respect to each other, and no such constraints for the left nodes.  Thus, the total number of ways to interleave the two sequences is $\frac{(m_r+m_l)!}{m_r! m_l!} = {m_r+m_l \choose m_r}$.  See Figure \ref{fg:unbalanced}.  For $k_r,k_l > 1$, we simply multiply these in, giving the total number of orderings as:
%\[ {m_r +m_l \choose m_r} k_r k_l \]
%Note we can apply the same logic to the right and left subtrees to determine $k_r$ and $k_l$.  Given the children of node $i$ are $l_i$ and $r_i$, the number of orderings $k_i$ consistent with a particular subtree $i$ is:
%\[  k_i = {m_{r_i} + m_{l_i} \choose m_{r_i}} k_{r_i} k_{l_i} \]
%So we can write out the total number of orderings as a product over all internal nodes of the tree:
%\[ T(\psi_n) = \prod_{i=1}^{n-1} {m_{r_i} + m_{l_i} \choose m_{r_i} } \]
%Furthermore, note that $m_i = m_{l_i}+ m_{r_i}+1$ (the number of internal nodes in a tree is the sum of those in the subtrees, plus the root), so the denominator of the binomial coefficients of a parent node will cancel the numerators of the coefficients of its children, however leaving a term proportional to $\frac{1}{m_i}$, and the numerator of the root will not be canceled.  Thus we have:
%\[ T(\psi_n) = \frac{(n-1)!}{\prod_{i=1}^{n-1} m_i } \]
%
%\end{proof}



%\begin{figure}
%\centering
%\includegraphics[width=.4\textwidth]{../figures/unbalanced_subtrees.eps}
%\caption{A tree with two unbalanced subtrees.  The internal nodes for each of the subtrees can only be ordered in one way with respect to each other, however, for the overall tree there are $\frac{6!}{3!3!} = {6 \choose 3} = 20$ orderings of the internal nodes.}
%\label{fg:unbalanced}
%\end{figure}

(For proof see the supplementary material.) This is in agreement with what we would expect: for an unbalanced tree, $m_i = \{n-1,n-2,n-3...\}$ for $i=\{1,2,3...\}$, so this gives $T=1$.  Since an unbalanced tree imposes a full ordering on the internal nodes, there can only be one unbalanced ordered tree that maps to the corresponding unbalanced unordered tree.  As the tree becomes more balanced, the $m_i$s decrease, increasing $T$.

Thus the probability of a particular $\psi_n$ is $T(\psi_n)$ times the probability of an ordered tree $\phi_n$ under the coalescent:
\begin{equation}
\label{eq:prior} 
p(\psi_n) = T(\psi_n) \prod_{i=2}^{n} {i \choose 2}^{-1} = \frac{(n-1)!}{\prod_{i=1}^{n-1} m_i} \prod_{i=2}^{n} {i \choose 2}^{-1} 
\end{equation}
\begin{mythm} $p(\psi_n)$ defines an exchangeable and consistent prior over $\Psi_n$
\end{mythm}
$p(\psi_n)$ is clearly still exchangeable as it does not depend on any order of the data, and was defined by marginalizing out a consistent process, so its conditional priors are still well defined and thus $p(\psi_n)$ is consistent.  For a more explicit proof see the supplementary material.

\begin{figure}
\centering
\includegraphics[width=.7\textwidth]{../figures/MCMC_proposal.eps}
\caption{The subtree $S_l$ rooted at the red node $l$ is pruned in preparation for an MCMC move.  We perform slice sampling to determine where the pruned subtree's parent should be placed next in the remaining tree $\pi_l$.}
\label{fg:mcmc_jump}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=.7\textwidth]{../figures/slice_sampling.eps}
\caption{We compute the posterior pdf for each branch that we might attach to.  If $\alpha$ or $\beta$ are greater than one, the Beta prior on branch lengths can cause these pdfs to go to zero at the limits of their domain.  Thus to enable moves across branches we compute the extrema of each pdf so that all valid intervals are found.}
\label{fg:slice_sampler}
\end{figure}



\section{Data Generation Model}
Given a prior over tree structures such as \eqref{eq:prior}, we can define a data generating process in many ways (indeed, any $L_1$ bounded martingale will do \cite{steinhardt2012martingale}); here we restrict our attention to generative models in which we first sample times give a tree structure, and then sample the data according to some process described on those times (in our case Brownian Motion).
\subsection{Branch Lengths}
\label{sec:times}
Given a tree structure $\psi$, we can sample branch lengths, $s_i = t_{p_i}-t_i$, with $t_i$ the time of coalescent event $i$, with $t=0$ at the leaves and $p_i$ is the parent of $i$. Consider the following construction similar to a stick-breaking process:  Start with a stick of unit length.  Starting at the root, travel down the given $\psi$, and at each split point duplicate the current stick into two sticks, assigning one to each child.  Then, sample a Beta random variable $B$ for each of the two sticks where the corresponding children are not leaves.  $B$ will be the proportion of the remaining stick attributed to that branch of the tree until the next split point (sticks afterwards will be of length proportional to $(1-B)$). We have, $B_i = 1 - (t_i/t_{p_i}) = s_i/t_{p_i}$.  The total prior over branch lengths can thus be written as:
\begin{equation}
\label{eq:branch_prior}
p(\{B_i\}|\psi) = \prod_{i=1}^{N-1} {\cal B}(B_i|\alpha,\beta)
\end{equation} 
See Figure \ref{fg:samples} for samples from this prior. Note that any distribution whose support is the unit interval may be used, and in fact more innovative schemes for sampling the times may be used as well; one of the major advantages of \ourprior over the Coalescent and DDT is that the times may be defined in a variety of ways.  

There is a single Beta random variable attributed to each internal node of the tree (except the root, which has $B$ set to 1).  Since the order in which we see the data does not affect the way in which we sample these stick proportions, the process remains exchangeable.  We denote pairs $(\psi_n, \{B_i\} )$ as $\pi$, i.e. a tree structure with branch lengths.


\subsection{Brownian Motion}
\label{sec:datagen}
Given a tree $\pi$ we can define a likelihood for continuous data $x_i\in \mathbb{R}^p$ using Brownian motion.  We denote the length of each branch segment of the tree $s_i$.  Data is generated as follows: we start at some unknown location in $\mathbb{R}^p$ at time $t=1$ and immediately split into two independent Wiener processes (with parameter $\Lambda$), each denoted $y_i$ where $i$ is the index of the relevant branch in $\pi$.  Each of the processes evolve for times $s_i$, then, a new independent Wiener process is instantiated at the time of each split, and this process continues until all processes reach the leaves of $\pi$ (i.e. $t=0$), at which point the $y_i$s at the leaves are associated with the data $\bx$.  This is a similar likelihood to the ones used for Dirichlet Diffusion Trees \cite{neal2003dft} and the Coalescent \cite{TehDauRoy2008} for continuous data.

\subsubsection{Likelihood Computation}
The likelihood $p(\bx|\pi)$ can be calculated using a single bottom-up sweep of message passing.  As in \cite{TehDauRoy2008}, by marginalizing out from the leaves towards the root, the message at an internal node $i$ is a Gaussian with mean $\hy_i$ and variance $\Lambda \nu_i$.  

The $\nu$ and $\hy$ messages can be written for any number of incoming nodes in a single form:
\begin{align*}
&\nu_i^{-1} = \sum_{j\in c(i)}  (\nu_j + s_j)^{-1}; ~~~~~~~~~~~~
\hy_i = \nu_i \sum_{j\in c(i)} \frac{\hy_j}{\nu_j+s_j}
\end{align*}
where $c(i)$ are the nodes sending incoming messages to $i$.  We can compute the likelihood using any arbitrary node as the root for message passing.  Fixing a particular node as root, we can write the total likelihood of the tree as:
\begin{equation}
p(\bx|\pi) = \prod_{i=1}^{n-1} Z_{c(i)}(\bx,\pi) 
\label{eq:pxpi}
\end{equation}
When $|c(i)| = 1$ (e.g. when passing through the root at $t=1$), $Z_{c(i)} = 1$. When $|c(i)| = 2$ and $|c(i)| = 3$ (when collecting at an arbitrary node $i$ chosen as the root): 
\begin{align}
&Z_{l_i,r_i}(\bx,\pi) = |2\pi \hL_i|^{-\frac{1}{2}} \exp\left(-\frac{1}{2} ||\hy_{r_i} - \hy_{l_i}||_{\hL_i}^2\right); ~~~~~~~
\hL_i = \Lambda(\nu_{l_i} + \nu_{r_i} + s_{l_i} + s_{r_i})\\
\label{eq:Z3} &Z_{p_i,l_i,r_i}(\bx,\pi) = |2\pi \Lambda|^{-1} {\nu^*}^{-\frac{k}{2}} e^{\left( -\frac{1}{2} \left( \nu_{p_i}^* ||\hy_{l_i} - \hy_{r_i}||_{\Lambda\nu^*}^2 + \nu_{r_i}^* ||\hy_{p_i} - \hy_{l_i}||_{\Lambda\nu^*}^2 + \nu_{l_i}^* ||\hy_{p_i} - \hy_{r_i}||_{\Lambda\nu^*}^2    \right) \right) }\notag \\
&\nu_{p_i}^* = \nu_{p_i} + s_i; ~~~~
\nu_{l_i}^* = \nu_{l_i} + s_{l_i}; ~~~~
\nu_{r_i}^* = \nu_{r_i} + s_{r_i}; ~~~~
\nu^* = \nu_{p_i}^*\nu_{l_i}^* + \nu_{l_i}^*\nu_{r_i}^* + \nu_{r_i}^*\nu_{p_i}^*
\end{align}
where $||.||_{\Lambda}$ corresponds to the Mahalanobis norm with covariance $\Lambda$. These messages are derived by using the product of Gaussian pdf identities.  

%In order to perform efficient MCMC steps wherein subtrees are pruned from and reattached to the tree, we can compute both the upwards and downwards messages of belief propagation so that we may consider any node as the root and make local modifications. 

%Using these likelihood terms the likelihood can be computed by considering any node of the tree as the root, which is needed for the MCMC moves in the next section where we are attaching a branch back into an arbitrary point of the tree.

\begin{figure}
\centering
\includegraphics[width=0.245\textwidth]{../figures/2dheavy_dp_density.eps} 
\includegraphics[width=0.245\textwidth]{../figures/2dheavy_dft_density.eps} 
\includegraphics[width=0.245\textwidth]{../figures/2dheavy_logdensity.eps} 
\includegraphics[width=0.245\textwidth]{../figures/2dheavy_noisemodel_logdensity.eps} 
\caption{ {\bf (left)} Approximated log-density using a DP mixture. {\bf (midleft)} Log-density using a Dirichlet Diffusion Tree model. {\bf (midright)} Log-density using our model directly.  {\bf (right)} Log-density using our model with a heavy-tailed noise model at the leaves.  Contours are spaced 1 apart, for a total of 15 contours.  In the probability domain the various densities look similar.}
\label{fg:synth} 
\end{figure}


\begin{figure}
\centering
\includegraphics[width=\textwidth]{../figures/leukemia_dendrogram.eps}
\caption{Posterior sample from our model applied to the leukemia dataset.  Best viewed in color.  Each pure subtree is painted a color unique to the class associated with it.  The OTHERS class is a set of datapoints to which no diagnostic label was assigned.} 
\label{fg:leukemia_dend}
\end{figure}



\subsection{MCMC Inference}
We propose an MCMC procedure that samples from the posterior distribution over $\pi$ as follows.  First, a random node $l$ is pruned from the tree (so that its parent $p_l$ has no parent and only one child), giving the pruned subtree $S_l$ and remaining tree $\pi_l$.  See Figure \ref{fg:mcmc_jump}.  We then consider all possible moves that would place $p_l$ into a valid location elsewhere in the tree. For each branch indexed by the node $i$ ``below'' it, we compute the posterior density function of where $p_l$ should be placed on that branch. We then slice sample on this collection of density functions.  See Figure \ref{fg:slice_sampler}.  By cycling through the nodes to prune and reattach, we achieve a Gibbs sampler over $\pi$.

We can efficiently compute the relative change in the likelihood $p(\bx|\pi)$ through a combination of belief propagation and local computations.  First we perform belief propagation on $\pi_l$ to give upward and downward messages, and on $S_l$ to give only upwards messages. Denote $\pi(S,i,t)$ as the tree formed by attaching $S$ above node $i$ at time $t$ in $\pi$. For the new tree we imagine collecting messages to node $p_l$ resulting in a new factor $Z_{i,l,p_i}(\bx,\pi_l(S_l,i,t))$. The messages directly downstream of this factor are $Z_{c(i)}(\bx,\pi_l)$ and $Z_{p_{p_i},r_{p_i}}(\bx,\pi_l)$ (if $l_{p_i}=i$, ie $i$ is the ``left'' child of its parent).  If we now imagine that the original likelihood was computed by collecting to node $p_i$, then we see that the first factor should replace the factor $Z_{l_{p_i},r_{p_i},p_{p_i}}(\bx,\pi_l)$ at node $p_i$ while the latter factor was already included in $Z_{l_{p_i},r_{p_i},p_{p_i}}(\bx,\pi_l)$. All other factors do no change. The total (multiplicative) change in the likelihood is thus,
\begin{equation}
\label{eq:like_add}
\Delta Z(\pi_l(S_l,i,t) ) = Z_{i,l,p_i}(\bx,\pi_l(S_l,i,t))\frac{Z_{p_{p_i},r_{p_i}}(\bx,\pi_l)}{Z_{l_{p_i},r_{p_i},p_{p_i}}(\bx,\pi_l)}
\end{equation}
The update in prior probability for adding the parent of $l$ in the segment $(i,p_i)$ (with times $t_i$ and $t_{p_i}$) at time $t$ is proportional to the product of the Beta pdfs in \eqref{eq:branch_prior} that arise when $\pi_l(S_l,i,t)$ is constructed, and inversely proportional to the Beta pdf that is removed from $\pi_l$, as well as being proportional to the overall prior probability over $\psi_n$:\footnote{Note that if either $l$ or $i$ is a leaf, then the prior term will be simpler than the one listed here}
\begin{equation}
\label{eq:prior_add}
p(\pi_l(S_l,i,t) ) \propto \frac{1}{{\cal B}(1-\frac{t_i}{t_{p_i}};\alpha,\beta)  }{\cal B}(1-\frac{t}{t_{p_i}};\alpha,\beta) {\cal B}(1-\frac{t_i}{t};\alpha,\beta) {\cal B}(1-\frac{t_l}{t};\alpha,\beta) p(\psi(\pi_l(S_l,i,t)) 
\end{equation}
Where $\psi(\pi)$ gives the structure part of $\pi = (\psi, \{B_i\})$.  $p(\psi(\pi_l(S_l,i,t))$ can be computed for all $i$ in linear time via dynamic programming (it does not depend on the actual value of $t$).  By taking the product of \eqref{eq:like_add} and \eqref{eq:prior_add} we get the joint posterior of $(i,t)$:
\begin{equation}
\label{eq:post}
p(\pi_l(S_l,i,t) | X) \propto \Delta Z(\pi_l(S_l,i,t) ) ~ p(\pi_l(S_l,i,t)) 
\end{equation}
$p(\pi_l(S_l,i,t) |X)$ defines the distribution from which we would like to sample.  We propose a slice sampler that can propose adding $S_l$ to any segment in $\pi_l$.  For a fixed $i$, $p(\pi_l(S_l,i,t) |X)$ is typically unimodal, and typically has a small number of modes at most.  If we can find all of the modes of the posterior, we can easily find the intervals that contain positive probability density for slice sampling moves (see Figure \ref{fg:slice_sampler}).  Thus this slice sampling procedure will mix as quickly as slice sampling on a single unimodal distribution.  We find the modes of these functions using Newton methods.

The overall sampling procedure is then to sample a new location for each node (both leaves and internal nodes) of the tree using the Gibbs sampling scheme explained above.

\subsection{Hyperparameter Inference}

As we do not know the structure of the data beforehand, we may not want to predetermine the specific values of $\alpha,\beta$ and $\Lambda$.  Thus we define hyperpriors on these parameters and infer them as well.
For simplicity we assume the form $\Lambda = kI$ for the Brownian motion covariance parameter.  We use an Inverse-Gamma prior on $k$, so that $k^{-1} \sim {\cal G}(\kappa,\theta)$.  

%After sampling in node locations $y_i$\footnote{It is possible to update $k$ with the $y$s marginalized out, but we found sampling the $y$s gives better mixing}, the posterior for $k^{-1}$ is given by:

%\[ k^{-1} | X \sim Gamma\left( \frac{(2N-1)p}{2} + \kappa, \frac{1}{2} \sum_{i=1}^{2N-1} d_i + \theta\right) \]
\[ k^{-1} | X \sim {\cal G}\left( \frac{(N-1)p}{2} + \kappa, \frac{1}{2} \sum_{i=1}^{N-1} d_i + \theta\right) \]

where $N$ is the number of datapoints, $p$ is the dimension, and $d_i = \frac{||\hy_{l_i} - \hy_{r_i}||^2}{ \nu_{l_i}+\nu_{r_i} + s_{l_i}+s_{r_i} }$, $||.||$ is the Euclidean norm.

%$d_i = \frac{||y_i - y_{p_i}||^2}{s_i }$.


By putting a ${\cal G}(\xi,\lambda)$ prior on $\alpha-1$ and $\beta-1$, we achieve a posterior for these parameters:
\[ p(\alpha, \beta| \xi,\lambda,X) \propto (\alpha-1)^\xi (\beta-1)^\xi e^{-\lambda(\alpha-1 + \beta-1)}     \prod_{i=1}^{N-1} \frac{1}{B(\alpha,\beta)} \left(1-\frac{t_i}{t_{p_i}}\right)^{\alpha-1} \left(\frac{t_i}{t_{p_i}}\right)^{\beta-1} \]
This posterior is log-concave and thus unimodal.  We perform slice sampling to update $\alpha$ and $\beta$.

\subsection{Predictive Density}
Given a set of samples from the posterior, we can approximate the posterior density estimate by sampling a test point located at $y_t$ into each of these trees repeatedly (giving new trees $\pi' = \pi(y_t,i,s)$ for various values of $i$ and $s$) and approximating $p(y_t | X)$ as:
\begin{align*}
p(y_t |X ) = \int p(\pi|X)p(y_t|\pi,X)  d\pi &= \int d\pi p(\pi|X) \int d\pi' p(\pi',y_t|\pi,X)\\
 &\approx \sum_{\pi_i \sim \pi|X} \sum_{\pi'_j \sim p(\pi'_j|\pi_i,X) } p(y_t|\pi_j',X) 
\end{align*}
where $p(\pi' | \pi,X) = \int p(\pi',y_t|\pi,X )dy_t$.  By integrating out $y_{l_i}$ in \eqref{eq:Z3}, we get a modification of \eqref{eq:post} that is proportional to $p(\pi' | \pi,X)$.  Slice sampling from this gives us several new trees $\pi_j'$ for each $\pi_i$, where one of the leaves is not observed.  $p(y_t|\pi'_j,X)$ is then available by message passing to the leaf associated with $y_t$ (denoted $l$), which results in a Gaussian over $y_t$.  Thus the final approximated density is a mixture of Gaussians with a component for each of the $\pi'_j$.

Performing the aforementioned integration (after replacing $\hy_{l_i}$ with $y_t$), we get:
\[Z_{pred}(i,t) \propto \int dy_t Z_{i,p_i,l}(\bx,\pi'(y_t)) = |2\pi \Lambda|^{-\frac{1}{2}} (\nu_{i}^* + \nu_{p_i}^*)^{-\frac{k}{2}} \exp\left(  -\frac{1}{2} (\nu_{l}^* + ({\nu_{i}^*}^{-1} + {\nu_{p_i}^*}^{-1})^{-1}) d_{i,p_i}  \right)  \]
where $d_{i,p_i} = ||\hy_{i} - \hy_{p_i}||_{\Lambda \nu^*}^2$. This gives the posterior density for the location of the unobserved point:
\[ p(\pi' = \pi(\{l\},i,t) |\pi, X) \propto Z_{pred}(i,t) \frac{Z_{l_i,r_i}(\bx,\pi_l)}{Z_{l_i,r_i,p_i}(\bx,\pi_l)} p(\pi(\{l\},i,t))\] 
where $p(\pi(\{l\},i,t))$ is as in \eqref{eq:prior_add}.
 
\section{Experiments}
\label{sec:experiments}
We compare our model to Dirichlet Diffusion Trees (DDT) \cite{neal2003dft} and to Dirichlet Process Mixtures (DPM) \cite{ferguson1983bayesian,lo1984class}.  We used Radford Neal's Flexible Bayesian Modeling package \cite{neal2003software} for both the DDT and the DPM experiments.  All algorithms were run with vague hyperpriors, except for the DPM concentration parameter which we set to .1 as we did not expect many modes for these experiments. 

\subsection{Synthetic Data}
To qualitatively compare our method to Dirichlet Process Mixtures and Dirichlet Diffusion Trees, we ran all three methods on a simulated dataset with $N=200$, $p=2$.  The data is generated from a mixture of heavy tailed distributions to demonstrate the differences between these algorithms when presented with outliers.  As can be seen in Figure \ref{fg:synth}, the DDT fits a density with reasonably heavy tails, whereas our model fits a narrower distribution.  This is a result of the fact that the divergence function of the DDT strongly encourages the branch lengths to be small, and thus a larger variance is required to explain the data.  Our model can be combined with a heavy-tailed observation model to produce densities with heavier tails -- see the rightmost panel of Figure \ref{fg:synth}.


\begin{figure}
\centering
\includegraphics[width=.32\textwidth]{../figures/gene_cluster200k10.eps}
\includegraphics[width=.32\textwidth]{../figures/gene_clustering1000k10.eps}
\includegraphics[width=.32\textwidth]{../figures/birds_comparisonk10.eps}
\caption{ A comparison of our method to the DDT and DPM, using predictive log likelihood on test data.  Plots show performance over time, except the DPM which shows the result after convergence. {\bf (left)} the comparison on a $p=200$ version of the St. Jude's Leukemia dataset.  The ``TMC - $k$'' runs are with $k$ fixed throughout the run.  {\bf (middle)} the comparison on the $p=1000$ version of the Leukemia dataset  {\bf (right)} comparison on a $N=1400$, $p=200$ bag of visual words dataset. }
\label{fg:gene_compare}
\end{figure}
\subsection{Gene Clustering}
We applied our model to the St. Jude's Leukemia dataset \cite{yeoh2002stjude}, which has $N_{train}=215$ datapoints, $N_{test} = 112$, and preprocessed\footnote{We simply took the 1000 dimensions with the highest variance.} to have $p=1000$ dimensions.  We preprocessed the data so that each dimension had unit variance.  Associated with each datapoint is one of 6 classifications of leukemia, or a 7$^{th}$ class with which no diagnosis was attributed.  We applied our method to the full dataset to see if it could recover these classes.  Figure \ref{fg:leukemia_dend} shows the posterior tree sampled after about 28 Gibbs passes (about 10 minutes).  We also compared our method against the DDT and DPM on these models' abilities to predict test data on a $p=200$ subset of the $p=1000$ dataset, as well as on the $p=1000$ datset, see Figure \ref{fg:gene_compare}.  On the $p=200$ dataset, both the DDT and the TMC outperform the DPM, with the TMC performing slightly worse.  We attribute this difference in performance due our model's weaker prior on the branch lengths, which causes our model to overfit slightly; if we preset the diffusion variance of our model to a value somewhat larger than the data variance, our performance improves.  In the $p=1000$ dataset, the same phenomenon is observed again. 

\subsection{Computer Vision Features}
We also cluster visual bag of words features collected from birds images from Visipedia \cite{WelinderEtal2010}.  We worked on a dataset of size $N=1400$, $N_{test}=1412$, where each observation belongs to one of 200 classes of birds, see Figure \ref{fg:gene_compare}.  Again our method is better than DPM yet worse than DDT.  Fixing the variance does improve the performance of our algorithm but not enough to improve over the DDT. 

\section{Conclusion}
\label{sec:conclusion}
We introduced a new prior for use in NPB hierarchical clustering, one that can be used in a variety of ways to define a generative model for data.  By marginalizing out the time of the coalescent, we achieve a prior from which data can be either generated directly via a graphical model living on trees, or by a CTMC lying on a distribution over times for the branch lengths -- in the style of the coalescent and DDT.  However, unlike the coalescent and DDT, in our model the times are generated conditioned on the tree structure; giving potential for more interesting models or more efficient inference.  The simplicity of the prior allows for efficient Gibbs style inference, and we provide an example model and demonstrate that it can achieve similar performance to that of the DDT.  However, to achieve that performance the diffusion variance must be set in advance, suggesting that alternative distributions over the branch lengths may provide better performance than the one explored here.

\bibliographystyle{plain}
\bibliography{biblio.bib}

\end{document}
