#!python
#cython: cdivision=True
#cython: profile=True
#cython: wraparound=False
#cython: boundscheck=True
import numpy as np
cimport numpy as np
import scipy.stats as sts
import scipy.linalg as lin
import utilities as util
cimport utilities as util

from libc.stdlib cimport malloc,free
from libc.stdlib cimport rand,srand,RAND_MAX 
from libc.math cimport log,exp,fmod
from libc.float cimport DBL_MAX

#ctypedef util.DTYPE_t DTYPE_t
#ctypedef util.ITYPE_t ITYPE_t


#from libc.math cimport log,M_PI

#cdef inline double d_max(double a, double b): return a if a >= b else b
#cdef inline double d_min(double a, double b): return a if a <= b else b

#ctypedef np.double_t DTYPE_t
#ctypedef np.longdouble_t LDTYPE_t

"""
Slice sample from a given target distribution, described as a collection of pdfs

T: List of pairs who define the bounds on each function of F
cur_i: Index into F of the current function
cur_t Current state in T[cur_i]
n_iter: Number of iterations to perform
n_init_iter: Number of iterations to sample from each function of F for initialization
"""

cdef void sample_multiple_particle(  double (*eval_pdf)(double,int,double[:,:], double[:,:],double,double,double[:], double, int )  , np.ndarray[DTYPE_t,ndim=2]  T, int cur_i, double cur_t, int n_iter, int n_init_iter, double[:,:] pC, double[:,:] lC, double al,double be, double[:] v, double dL, int p ,int seed, int* iiout, double* ttout):

    cdef int n = len(T)

    cdef int seg = cur_i
    cdef double[:] xx = np.zeros(n)
    cdef double[:] yy = np.zeros(n)
    cdef double[:] Zy = np.zeros(n)
    cdef double[:,:] I = np.zeros( (n,2) )
    cdef int i,j
   
    cdef double w
    cdef double Zsum,qx,xx_tmp,fx
 
    cdef double[:] tmp = np.zeros(2)
    cdef double[:] pr = np.zeros(n)

    set_seed(seed) 


    for i in range(n):

        if i != seg:
        
            xx[i] = T[i,0] + uniform()*(T[i,1]-T[i,0]) 

#            initialize each xx by slice sampling from conditional
#            for i in range(n_init_iter): 
#                
#
#
#            #end
        else:
            xx[i] = cur_t

        if T[i,0] == T[i,1]:
            Zy[i] = 0
        else:
            fx = eval_pdf(xx[i],i, pC,lC,al,be,v,dL,p)
            yy[i] = fx-expon()       

     
            w = (T[i,1]-T[i,0])/20

            step_out(eval_pdf,yy[i],i ,xx[i],T[i,0],T[i,1],w,pC,lC,al,be,v,dL,p, tmp )
            I[i,0] = tmp[0]
            I[i,1] = tmp[1]

    #end
  
 
    cdef int max_ind = argmax(yy,n)
    cdef double mx = yy[max_ind]
    for i in range(n):
        Zy[i] = (I[i,1]-I[i,0])*exp(yy[i]-mx)
    #end  

#    fx = Zy[seg]

    cdef int itr 

    for j in range(n_iter):

        qx = -DBL_MAX

        itr = 0
        while qx < yy[seg]:

            Zsum = util.npsum(Zy,n) 
#            print Zsum
#            print yy[seg]
#            print exp(yy[seg])
#            print I[seg,1]-I[seg,0]

            pr = Zy 
            scalar_mult(pr,1.0/Zsum,n)

            seg = multinomial(pr)

            u = uniform()*(I[seg,1]-I[seg,0]) #sts.uniform.rvs(scale=Zy[seg])

            #print seg
            #print I[seg][0]

            xx_tmp = u+I[seg,0]
            qx = eval_pdf(xx_tmp,seg,pC,lC,al,be,v,dL,p)  

            if qx < yy[seg]:

                if xx_tmp > xx[seg]:
                    I[seg,1] = xx_tmp
                else:
                    I[seg,0] = xx_tmp

                Zy[seg] = (I[seg,1]-I[seg,0])*exp(yy[seg]-mx)
            
        
            #end

        #end

        xx[seg] = xx_tmp
        yy[seg] = qx-expon()
        w = (T[seg,1]-T[seg,0])/20
        step_out(eval_pdf,yy[seg],seg ,xx[seg],T[seg,0],T[seg,1],w,pC,lC,al,be,v,dL,p, tmp )
        I[seg,0] = tmp[0]
        I[seg,1] = tmp[1]

        Zy[seg] = (I[seg,1]-I[seg,0])*exp(yy[seg]-mx)
        

    #end

#    print Zy[seg]
#    print fx
#    print log(Zy[seg]) - log(fx)

#    print yy[seg]
#    print exp(yy[seg]-mx)
#    print I[seg,1]-I[seg,0]
    iiout[0] = seg
    ttout[0] = xx[seg]


cdef void va_sample( double (*eval_pdf)(double, int, vl_wrapper*), np.ndarray[ITYPE_t,ndim=1] Ind, np.ndarray[DTYPE_t,ndim=2]  T, np.ndarray[DTYPE_t, ndim=1] Mx, int cur_i, double cur_t, int n_iter, int seed, int* iiout, double* ttout, ...):
#double[:,:] pC, double[:,:,:] lC, double al,double be, double[:] v, double dL, int p ,int seed, int* iiout, double* ttout):

    cdef int n = len(T)

    cdef int seg = cur_i
    cdef double xx = cur_t
    cdef int seg0

    cdef va_list vlist

    cdef vl_wrapper vlw_o
    cdef vl_wrapper* vlw = &vlw_o  
    va_start(vlw.v,<void*>ttout)


    cdef double f0,x0
    cdef double fx,y,w,qx,Zsum,u
    cdef int qitr
    cdef bint safe_mode = 0

    cdef np.ndarray[DTYPE_t,ndim=1] Zy,pr
    cdef np.ndarray[DTYPE_t,ndim=2] I

    set_seed(seed) 
   
    Zy = np.zeros(n)
    I = np.zeros( (n,2) ,dtype=np.double)
    cdef np.ndarray[DTYPE_t,ndim=1] tmp = np.zeros(2)

    f0 = eval_pdf(xx,Ind[seg],vlw)#Ind[seg],pC,lC,al,be,v,dL,p)

    #print f0 
 
    for itr in range(n_iter):

        x0 = xx
        seg0 = seg
        fx = eval_pdf(xx,Ind[seg],vlw)#pC,lC,al,be,v,dL,p) #    F[seg](xx)

        y = fx-expon() #sts.expon.rvs()
    
            

        for i in range(n):


            f = eval_pdf(Mx[i],Ind[i],vlw)#, pC,lC,al,be,v,dL,p) #  F[i](Mx[i])
            if f > y and i != seg:
                w = (T[i,1]-T[i,0])/20

                va_step_out(eval_pdf,y,Ind[i] ,Mx[i],T[i,0],T[i,1],w,tmp,vlw) #pC,lC,al,be,v,dL,p, tmp )
                I[i,0] = tmp[0]
                I[i,1] = tmp[1]
                Zy[i] = I[i,1]-I[i,0]
            elif i == seg:
                w = (T[i,1]-T[i,0])/20
                va_step_out(eval_pdf,y,Ind[i] ,xx,T[i,0],T[i,1],w,tmp,vlw) #pC,lC,al,be,v,dL,p, tmp )
                I[i,0] = tmp[0]
                I[i,1] = tmp[1]
                Zy[i] = I[i,1]-I[i,0]
 #               print (I[i,0],xx,I[i,1])
                #print "huh"
            else:
                Zy[i] = 0
                
            #end 

        #end

        qx = -DBL_MAX

        qitr = 0

        while qx < y:
            Zsum = util.npsum(Zy,n) #np.sum(Zy)

#            if Zsum == 0:
#                print Zy
#                print "iteration: %d" % itr
#                print "qiteration: %d" % qitr
#                print n
#                print I
#                print T
#                print Mx 
#                print y

            qitr += 1
            if qitr == 10000:
                print "Warning: va slice sampler taking over 10000 iterations, aborting this iteration and turning on safe mode"
                safe_mode = 1
                break

            pr = Zy / Zsum

            seg = multinomial(pr) #np.nonzero(np.random.multinomial(1,pr))[0][0]
            
            u = uniform()*Zy[seg] #sts.uniform.rvs(scale=Zy[seg])



            xx = u+I[seg,0]
            qx = eval_pdf(xx,Ind[seg],vlw) #pC,lC,al,be,v,dL,p)  #F[seg](xx)

#            print (I[seg,0],xx,I[seg,1])
#            print (qx,y,fx)

            #handles both cases in which we are sampling from a single function (with 
            #no modes computed), and many functions with the modes computed
            #may remove intervals of positive probability if the computed maximum is off, hence safe mode
            if qx < y and not safe_mode:
                if (xx > Mx[seg] and seg0 != seg) or (xx > x0 and seg0 == seg):
                    I[seg,1] = xx
                else:
                    I[seg,0] = xx

                Zy[seg] = I[seg,1]-I[seg,0]
            #end

        #end

    #end    

    #print eval_pdf(xx,Ind[seg],pC,lC,al,be,v,dL,p)-f0
    iiout[0] = seg
    ttout[0] = xx
#    return (seg,xx)




"""
Slice sample from a given target distribution

F: List of unimodal functions from which to sample (log pdfs)
T: List of pairs who define the bounds on each function of F
Mx: Location of the maximum of each function F
cur_i: Index into F of the current function
cur_t: Current location in T[cur_i]
n_iter: Number of steps to perform

"""

cdef void sample( double (*eval_pdf)(double,int,double[:,:], double[:,:],double,double,double[:], double, int ),  np.ndarray[ITYPE_t,ndim=1] Ind, np.ndarray[DTYPE_t,ndim=2]  T, np.ndarray[DTYPE_t, ndim=1] Mx, int cur_i, double cur_t, int n_iter, double[:,:] pC, double[:,:] lC, double al,double be, double[:] v, double dL, int p ,int seed, int* iiout, double* ttout):

    cdef int n = len(T)

    cdef int seg = cur_i
    cdef double xx = cur_t

    cdef double f0
    cdef double fx,y,w,qx,Zsum,u
    cdef int qitr
    cdef bint safe_mode = 0

    cdef np.ndarray[DTYPE_t,ndim=1] Zy,pr
    cdef np.ndarray[DTYPE_t,ndim=2] I

    set_seed(seed) 
   
    Zy = np.zeros(n)
    I = np.zeros( (n,2) ,dtype=np.double)
    cdef np.ndarray[DTYPE_t,ndim=1] tmp = np.zeros(2)

    f0 = eval_pdf(xx,Ind[seg],pC,lC,al,be,v,dL,p)
 
    for itr in range(n_iter):

        fx = eval_pdf(xx,Ind[seg],pC,lC,al,be,v,dL,p) #    F[seg](xx)

        y = fx-expon() #sts.expon.rvs()
    
            

        for i in range(n):


            f = eval_pdf(Mx[i],Ind[i], pC,lC,al,be,v,dL,p) #  F[i](Mx[i])
            if f > y:
                w = (T[i,1]-T[i,0])/20
                step_out(eval_pdf,y,Ind[i] ,Mx[i],T[i,0],T[i,1],w,pC,lC,al,be,v,dL,p, tmp )
                I[i,0] = tmp[0]
                I[i,1] = tmp[1]
                Zy[i] = I[i,1]-I[i,0]
            elif i == seg:
                w = (T[i,1]-T[i,0])/20
                step_out(eval_pdf,y,Ind[i] ,xx,T[i,0],T[i,1],w,pC,lC,al,be,v,dL,p, tmp )
                I[i,0] = tmp[0]
                I[i,1] = tmp[1]
                Zy[i] = I[i,1]-I[i,0]
                #print "huh"
            else:
                Zy[i] = 0
                
            #end 

        #end

        qx = -DBL_MAX

        qitr = 0

        if util.npsum(Zy,n) == 0:
            print "Warning: Zy all zeros"


        while qx < y:
            Zsum = util.npsum(Zy,n) #np.sum(Zy)

#            if Zsum == 0:
#                print Zy
#                print "iteration: %d" % itr
#                print "qiteration: %d" % qitr
#                print n
#                print I
#                print T
#                print Mx 
#                print y

            qitr += 1

            if qitr == 10000:
                print "Warning: slice sampler taking over 10000 iterations, aborting this iteration and turning on safe mode"
                print Zsum
                print Zy
                nanind = np.nonzero(np.isnan(Zy))[0]
                infind = np.nonzero(np.isinf(Zy))[0]

                if len( nanind ) >0:
                    print Ind[nanind]
                    print Mx[nanind]
                    print T[nanind]
                    print v[Ind[nanind]]
                if len(infind) > 0:
                    print Ind[infind]
                    print Mx[infind]
                    print T[infind]
                    print v[Ind[infind]]

                print T
                safe_mode = 1
                break

            pr = Zy / Zsum

            seg = multinomial(pr) #np.nonzero(np.random.multinomial(1,pr))[0][0]
            
            u = uniform()*Zy[seg] #sts.uniform.rvs(scale=Zy[seg])

            #print seg
            #print I[seg][0]

            xx = u+I[seg,0]
            qx = eval_pdf(xx,Ind[seg],pC,lC,al,be,v,dL,p)  #F[seg](xx)

            if qx < y and not safe_mode:
                if xx > Mx[seg]:
                    I[seg,1] = xx
                else:
                    I[seg,0] = xx

                Zy[seg] = I[seg,1]-I[seg,0]
            #end

        #end

    #end    

    #print eval_pdf(xx,Ind[seg],pC,lC,al,be,v,dL,p)-f0
    iiout[0] = seg
    ttout[0] = xx
#    return (seg,xx)


cdef void va_step_out( double (*eval_pdf)(double,int, vl_wrapper*) ,double y, int seg, double x, double xmin, double xmax, double w, double[:] out, vl_wrapper* vlw):
# double[:,:] pC, double[:,:,:] lC, double al, double be, double[:] v, double dL, int p , double[:]  out):


    cdef double t0 = x
    cdef double t1 = x

    cdef double u = uniform()*w
    t0 = x - u
    t1 = t0 + w

    t0 = util.d_max(xmin,t0)
    t1 = util.d_min(xmax,t1)

    #check for numerical issues, if so return full interval
    if t0-w == t0 or t1+w == t1:
        out[0] = xmin
        out[1] = xmax
        return
    #end

    cdef double f0 = eval_pdf(t0,seg,vlw) #pC,lC,al,be,v,dL,p)  #f(x)
    cdef double f1 = eval_pdf(t1,seg,vlw) #pC,lC,al,be,v,dL,p)  #f(x)

    cdef int qitr = 0

    while f0 > y and t0 > xmin:  
        t0 = util.d_max(xmin,t0-w)
        f0 = eval_pdf(t0,seg,vlw) #pC,lC,al,be,v,dL,p) #f(t0)

    while f1 > y and t1 < xmax:    
        t1 = util.d_min(xmax,t1+w)
        f1 = eval_pdf(t1,seg,vlw) #pC,lC,al,be,v,dL,p)  #f(t1)


    #cdef np.ndarray[DTYPE_t,ndim=1] out = np.zeros(2,dtype=np.double)
    #cdef double* out = <double*>malloc(sizeof(double)*2) #np.zeros(2,dtype=np.double)
    out[0] = t0
    out[1] = t1
   
"""
Step out from location x at height y
(t0,t1) = step_out(f,y,x)

f: Function on which to perform stepping out
y: Height
x: Location from which to step out
xmin: Lower bound for t0
xmax: Upper bound for t1
w: Step size

t0: left endpoint 
t1: right endpoint
"""
cdef void step_out( double (*eval_pdf)(double,int,double[:,:], double[:,:],double,double,double[:], double, int )  ,double y, int seg, double x, double xmin, double xmax, double w, double[:,:] pC, double[:,:] lC, double al, double be, double[:] v, double dL, int p , double[:]  out):


    cdef double t0 = x
    cdef double t1 = x

    cdef double u = uniform()*w
    t0 = x - u
    t1 = t0 + w

    t0 = util.d_max(xmin,t0)
    t1 = util.d_min(xmax,t1)

    #check for numerical issues, if so return full interval
    if t0-w == t0 or t1+w == t1:
        out[0] = xmin
        out[1] = xmax
        return
    #end

    cdef double f0 = eval_pdf(t0,seg,pC,lC,al,be,v,dL,p)  #f(x)
    cdef double f1 = eval_pdf(t1,seg,pC,lC,al,be,v,dL,p)  #f(x)

    cdef int qitr = 0

    while f0 > y and t0 > xmin:  
        t0 = util.d_max(xmin,t0-w)
        f0 = eval_pdf(t0,seg,pC,lC,al,be,v,dL,p) #f(t0)

    while f1 > y and t1 < xmax:    
        t1 = util.d_min(xmax,t1+w)
        f1 = eval_pdf(t1,seg,pC,lC,al,be,v,dL,p)  #f(t1)


    #cdef np.ndarray[DTYPE_t,ndim=1] out = np.zeros(2,dtype=np.double)
    #cdef double* out = <double*>malloc(sizeof(double)*2) #np.zeros(2,dtype=np.double)
    out[0] = t0
    out[1] = t1

#    return out
cdef int argmax(double[:] vals, int size):
    cdef int i,amax
    cdef double mx=-DBL_MAX
    for i in range(size):
        if vals[i] > mx:
            amax = i
            mx = vals[i]
           
    return amax 

cdef void set_seed(int seed):
    srand(seed)

cdef double uniform():
    return (<double>rand())/RAND_MAX
    
cdef double expon():
    return -log(uniform())

cdef int multinomial( double[:] pr):
    cdef double u = uniform()
    cdef double cumsum = pr[0]
    cdef int i = 0
    while cumsum < u:
        i += 1
        cumsum += pr[i]
 
    return i 

cdef void scalar_mult( double[:] vec, double sc, int p):
    cdef int i
    for i in range(p):
        vec[i] *= sc
    #end

