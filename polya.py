import numpy as np
import scipy.stats as sts
from heapq import *

def beta_htree( alpha, beta, n, treetype='unordered'):
    Z = np.zeros( (n-1,4) )

    valid_points = np.arange(n)
    #cluster points uniformly
    
    Betas = sts.beta.rvs(alpha,beta,size=n-1)

    Z[:,2] = 1


    if treetype == 'unordered':
        beta_tree_helper(n,Z,[n-2],[0])
    elif treetype == 'ordered':
        for i in range(n-1):
            indmax = n-i
            ind1 = np.random.randint(indmax)
            ind2 = np.random.randint(indmax)
            while ind1 == ind2:
                ind2 = np.random.randint(indmax)
            #endwhile 

            n_new = n+i
            i_l = valid_points[ind1]
            i_r = valid_points[ind2]
            valid_points[ind1] = n_new
            valid_points[ind2] = valid_points[indmax-1] 

            Z[i,0] = i_l
            Z[i,1] = i_r     
        #end i
    #endif


    P = np.zeros(2*n-1, dtype=np.int)
    d = np.zeros(n-1)
    v = np.zeros(n-1)
 
    for i in range(n-1):
        P[Z[i,0]] = i+n
        P[Z[i,1]] = i+n
    #end i 
   
    Z[n-2,2] = 1
    d[n-2] = 1
    v[n-2] = 0

    for i in reversed(range(n-2)):
        dtmp = d[P[n+i]-n]
        d[i] = (1-Betas[i])*dtmp
        v[i] = Betas[i]*dtmp
        Z[i,2] = d[i] #Z[P[i]-n,2]*Betas[i] 
    #end i

    for i in range(n-1):
        nl = Z[i,0]
        nr = Z[i,1]

        if nl < n:
            num_l = 1
        else:
            num_l = Z[nl-n,3]
        #endif

        if nr < n:
            num_r = 1
        else:
            num_r = Z[nr-n,3]
        #endif

        Z[i,3] = num_l+num_r

    #end i
    
    return (Z,P) 

def beta_tree_helper(n,Z,p,i):
    if n > 1:
        (n1,n2) = sample_partition(n)
        pp = p[0]
        p[0] -= 1
        Z[pp,0] = beta_tree_helper(n1,Z,p,i) 
        Z[pp,1] = beta_tree_helper(n2,Z,p,i)
        return pp+len(Z)+1
    else:
        ii = i[0]
        i[0] += 1
        return ii
    #endif
    
def sample_partition(n):

    if n == 2:
        return (1,1)
    elif n == 1:
        return 1
    else:
        c1 = 1
        c2 = 1
        for i in range(n-2):
            c = 2*c1+2*c2-1
            u = np.random.randint(c)
            if u < 2*c1-1:
                c1 += 1
            elif u < 2*c1+2*c2-2:
                c2 += 1
            else:
                c1 = c1+c2
                c2 = 1
            #endif
        #end i
        return (c1,c2)
    #endif


def polya_htree( alpha, beta, n):
    Z = np.zeros( (n-1,4) )

    PQ = []

    cur_i = 2*n-2
    heappush(PQ, (0, cur_i ) )

   
    while len(PQ) > 0:

        (d, ind) = heappop(PQ)
        [bl,br] = sts.beta.rvs(alpha,beta,size=2)
      
        dbl = bl*(1-d)
        dbr = br*(1-d)

        cur_i -= 2 
        nl = cur_i+1
        nr = cur_i

        Z[ind-n,0] = nl
        Z[ind-n,1] = nr
        Z[ind-n,2] = (1-d) #np.max( (1-(d+dbl),1-(d+dbr)) )

        if nl >= n:
            heappush(PQ, ( d+dbl, nl) )
        if nr >= n:
            heappush(PQ, ( d+dbr, nr) )

    #endwhile
 

    for i in range(n-1):
        nl = Z[i,0]
        nr = Z[i,1]

        if nl < n:
            num_l = 1
        else:
            num_l = Z[nl-n,3]
        #endif

        if nr < n:
            num_r = 1
        else:
            num_r = Z[nr-n,3]
        #endif

        Z[i,3] = num_l+num_r

    #end i
    
    return Z 

def compute_coalesce_time(Z,n1,n2):
    (nm1,_) = np.shape(Z)
    n = nm1+1

    P = np.zeros(2*n-1, dtype=np.int)
   
    for i in range(nm1):
        P[Z[i,0]] = i+n
        P[Z[i,1]] = i+n
    #end i 
   
    p1 = P[n1]
    p2 = P[n2]

    while p1 != p2:
        if p1 < 0 or p2 < 0:
            p1 = -1
            p2 = -1
        #endif

        if p1 < p2:
            p1 = P[p1]
        elif p2 < p1:
            p2 = P[p2]
        #endif
    #endwhile

    if p1 == -1:
        d = 1
    else:
        d = Z[p1-n,2]
    #endif

    return d 

def pairwise_coalesce_times(alpha,beta,n,nsamp):

    T = np.zeros(nsamp)

    for i in range(nsamp):
        Z = polya_htree(alpha,beta,n)

        ind1 = np.random.randint(n)
        ind2 = np.random.randint(n)
        while ind1 == ind2:
            ind2 = np.random.randint(n)
        #endwhile 
        t = compute_coalesce_time(Z,ind1,ind2)
        T[i] = t
    #end i
   
    return T 
