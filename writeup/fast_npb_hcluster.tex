\documentclass[12pt]{article}

\usepackage{amsmath,amssymb,fancyhdr,graphicx,subfig}


\oddsidemargin0cm
\topmargin-2cm     %I recommend adding these three lines to increase the 
\textwidth16.5cm   %amount of usable space on the page (and save trees)
\textheight23.5cm 
\headheight41.7pt 

\pagestyle{plain}

\newcommand{\Mt}{\tilde{M}}
\newcommand{\Poisson}{\mathrm{Poisson} }
\newcommand{\Beta}{\mathrm{Beta} }
\newcommand{\ancestors}{\mathrm{An} }
\newcommand{\descendants}{\mathrm{De} }

\newcommand{\bx}{{\bf x}}
\newcommand{\by}{{\bf y}}
\newcommand{\hy}{\hat{y}}
\newcommand{\hL}{\hat{\Lambda}}

\newcommand{\tnu}{\tilde{\nu}}
\newcommand{\td}{\tilde{d}}
\newcommand{\tv}{\tilde{t}}

\begin{document}

\section{Process on Tree Structures}
\label{sec:uniform_trees}

Consider a process in which we partition a dataset hierarchically as follows (for now, ignoring the branch lengths):  First, an individual point resides in a tree by itself with a single terminal branch (branch attached to a leaf).  Then, subsequent points travel down the tree from the top to bottom.  If the point arrives to a terminal branch, then it splits and forms a new (terminal) branch of the tree.  If the point reaches a split in the tree, each branch with a number of descendants $n_l$ and $n_r$, then it chooses between three possibilities with probabilities proportional to $2n_l-1$, $2n_r-1$, and $1$.  It follows the left branch with probability proportional to $2n_l-1$, the right with $2n_r-1$, and its own new terminal branch with $1$ (creating a split in the tree between the newly included point and the existing $n_l+n_r$ points).  See Figure \ref{fg:treeproc}.  This process corresponds to placing the new point branching from one of the tree's existing $2n-1$ branches uniformly at random.  Thus, for a given sequence of points, the probability of a given tree structure on $n$ points (denoted $\psi_n$) is simply
\[p(\psi_n) = \prod_{i=1}^{n-1}  \frac{1}{2i-1}\]
\begin{figure}
\centering
\includegraphics[width=8cm]{TreeProcess.eps}
\caption{The new point chooses among paths $A$,$B$, and $C$ as the point approaches a split in the tree.  It chooses paths $A$ and $B$ with probabilities proportional to $2n_l-1$ and $2n_r-1$, and its own path $C$ with probability proportional to 1.  If it chooses $A$ or $B$, then this evaluation will be repeated at the next split point reached.}
\label{fg:treeproc}
\end{figure}
Given a particular tree structure and sequence of data, there is exactly one process as described above that gives the tree structure from the data sequence.  Thus, as the probability of the given tree does not depend on the particular sequence of data given, the described process is exchangeable.  Since the process is described sequentially, we have the conditional distributions of a tree on $n+1$ points given $n$ points, so, for any particular set of $m < n$ points, we can reorder the $n$ points so that the set of $m$ points are sequentially added first in the process, and marginalize out the remaining $m+1$st to $n$th points (marginalizing the $n$th first), giving the process as described on $m$ points.  Thus, the process is consistent.

Interestingly, the uniform distribution on tree structures gives the CRP-like property of adding points to clusters that are already popular.  Also note that this process will not give rise to vacuous split points where all datapoints travel down a single path, unlike the nCRP.

However, the prior described above puts large probability mass on unbalanced trees, an undesirable property for hierarchical clustering.  See Figure \ref{fg:unordered_samples}.

\begin{figure}
\centering
\subfloat{ \includegraphics[width=.3\textwidth]{beta22_unordered1.eps}} 
\subfloat{ \includegraphics[width=.3\textwidth]{beta22_unordered2.eps}}
\subfloat{ \includegraphics[width=.3\textwidth]{beta22_unordered3.eps}}
\caption{ Three representative samples from the uniform prior on trees (branch lengths sampled as in Section \ref{sec:branches})}
\label{fg:unordered_samples}
\end{figure}

\subsection{Coalescent Tree Structures}

The tree structures given by Kingman's Coalescent are more balanced than those in Figure \ref{fg:unordered_samples}, so we would prefer to use coalescent structures rather than uniform trees.  Fortunately, the trees given by the coalescent can also be described in terms of uniform distributions over particular structures.  Consider a process in which a given state $\phi_n$ is a tree structure and an $ordering$ of the internal nodes of that tree structure.  Then, we add a new point to each subbranch uniformly at random, where a subbranch refers to an interval on a branch that lies between two consecutive nodes (which may reside in entirely different subtrees than the branch in question).  See Figure \ref{fg:coalescent_structure}.  Thus, a branch added at the root of a tree with $n$ leaves will have $n$ subbranches, as there are $n$ internal nodes in the newly formed tree with $n+1$ leaves. 

Thus, when adding the $n$th point, there are $n-1$ subbranches that occur before the first coalescent event (the first internal node), an additional $n-2$ before the second, and so on until there are an additional 2 subbranches before the root, and the one branch point that occurs above the root, giving a total of $\frac{n (n-1)}{2} = { n \choose 2} $ choices for adding a new point to the tree.  Multiplying over the $n-1$ choices that were made to build a particular tree, we have the probability of a tree:
\[p(\phi_n) =  \prod_{i=2}^{n} { i \choose 2}^{-1} \]
which is identical to the coalescent probability formed by repeatedly choosing pairs to coalesce uniformly at random, $\prod_{i=1}^{n-1} {n-i+1 \choose 2}^{-1}$.  Sampling $n-1$ Exponential random variables with rates ${i \choose 2}$, $i=2...n$ gives the full coalescent.  The process defined over the $\phi_n$s is exchangeable, again the final probability of the tree structure only depends on $n$, and there are no "collisions" or multiple paths to the same tree state for a given sequence of data.  Furthermore, the Exponential r.v.s sampled for the branch lengths do not depend on the particular tree structure $\phi_n$, so the entire process remains exchangeable.  Thus, this view of the coalescent provides another proof of its consistency. 

\subsection{Nonuniform Distributions over Trees}
It is possible to marginalize out the ordering information in the coalescent tree structures $\phi_n$ to derive exchangeable, consistent, and nonuniform priors on "unordered" tree structures $\psi_n$.  We can perform this marginalization by counting how many ordered tree structures $\phi_n$ are consistent with a particular unordered tree structure $\psi_n$.  

This can be done with a recursion relation.  Consider an unordered tree $\psi_n$ with $m_l=n_l-1$ $internal$ nodes in the left subtree, $m_r$ in the right.  Additionally, there are $k_l$ possible orderings of the left subtree's internal nodes, and $k_r$ possible orderings of the right subtree's internal nodes.  Consider the case where $k_l=k_r=1$, where we need to count the number of ways in which the two sequences of length $m_r$ and $m_l$ can be "interleaved".  There are $(m_r+m_l)!$ possible orderings of the $m_r+m_l$ nodes if we have no constraints on the orderings of the right nodes with respect to each other, and no such constraints for the left nodes.  Thus, the total number of ways to interleave the two sequences is $\frac{(m_r+m_l)!}{m_r! m_l!} = {m_r+m_l \choose m_r}$.  For $k_r,k_l > 1$, we simply multiply these in, giving the total number of orderings as:
\[ {m_r +m_l \choose m_r} k_r k_l \]
Note we can apply the same logic to the right and left subtrees to determine $k_r$ and $k_l$.  Given the children of node $i$ are $l_i$ and $r_i$, the number of orderings $k_i$ consistent with a particular subtree $i$ is:
\[  k_i = {m_{r_i} + m_{l_i} \choose m_{r_i}} k_{r_i} k_{l_i} \]
So we can write out the total number of orderings as a product over all internal nodes of the tree:
\[ T(\psi_n) = \prod_{i=1}^{n-1} {m_{r_i} + m_{l_i} \choose m_{r_i} } \]
Furthermore, note that $m_i = m_{l_i}+ m_{r_i}+1$ (the number of internal nodes in a tree is the sum of those in the subtrees, plus the root), so the denominator of the binomial coefficients of a parent node will cancel the numerators of the coefficients of its children, however leaving a term proportional to $\frac{1}{m_i}$, and the numerator of the root will not be canceled.  Thus we have:
\[ T(\psi_n) = \frac{(n-1)!}{\prod_{i=1}^{n-1} m_i } \]
This is in agreement with what we would expect: for an unbalanced tree, $m_i = \{n-1,n-2,n-3...\}$ for $i=\{1,2,3...\}$, so this gives $T=1$.  Since an unbalanced tree imposes a full ordering on the internal nodes, there can only be one unbalanced ordered tree that maps to the corresponding unbalanced unordered tree.  Furthermore, as the tree becomes more balanced, the $m_i$s decrease, increasing $T$.

Thus the probability of a particular $\psi_n$ is $T(\psi_n)$ times the probability of a ordered tree $\phi_n$ under the coalescent:
\[ p(\psi_n) = T(\psi_n) \prod_{i=2}^{n} {i \choose 2}^{-1} = \frac{(n-1)!}{\prod_{i=1}^{n-1} m_i} \prod_{i=2}^{n} \frac{2}{i(i-1)} = \frac{2^{n-1} }{ n!\prod_{i=1}^{n-1} m_i } \]

Since we still have a distribution that doesn't depend on the order of data seen, and the conditional prior distribution is still well defined (all we have done is marginalize out part of the state space), this defines an exchangeable and consistent prior.  We can verify that this is a consistent process by showing $\sum_{\psi_{n+1} \in C(\psi_n) } p(\psi_{n+1}) = p(\psi_n)$, where $C(\psi_n)$ is the set of $\psi_{n+1}$ structurally consistent with a particular $\psi_n$.  This is equivalent to showing:
\[ \sum_{\psi_{n+1} \in C(\psi_n) } T(\psi_{n+1}) = {n+1 \choose 2} T(\psi_n) \]
which can be proven by induction, and we take this as our inductive hypothesis for all $l < n$.  The base case for $n=1$: 
\[ T(\psi_{2}) = \frac{(2-1)!}{1! 1!} = 1 = {2 \choose 2} = {2 \choose 2} T(\psi_1) \]
Consider the tree $\psi_n$ and its two subtrees $\psi_{n_l}$ and $\psi_{n_r}$.  As listed before, we have $T(\psi_n) = {m_l + m_r \choose m_l} T(\psi_{n_l}) T(\psi_{n_r})$.  Depending on where we add the $n+1$st point, (denoted $x_{n+1}$) we have:
\[ T(\psi_{n+1}) = \begin{cases} 
{m_l+m_r+1 \choose m_l+1} T(\psi_{n_l+1}) T(\psi_{n_r}) & \mbox{if } x_{n+1} \mbox{ is added to the left subtree}\\
{m_l+m_r+1 \choose m_r+1} T(\psi_{n_l}) T(\psi_{n_r+1}) & \mbox{if } x_{n+1} \mbox{ is added to the right subtree}\\
{m_l + m_r+1 \choose m_l+m_r+1} {m_l+ m_r \choose m_r} T(\psi_{n_l}) T(\psi_{n_r}) &\mbox{if } x_{n+1} \mbox{ is added above the root}
\end{cases}
 \]
Summing over all such $\psi_{n+1}$, and invoking the inductive hypothesis, we have:
\begin{align*} \sum_{\psi_{n+1} \in C(\psi_n)} T(\psi_{n+1}) &= {m_l+m_r+1 \choose m_l+1} {m_l+1 \choose 2} T(\psi_{n_l}) T(\psi_{n_r}) \\
&+ {m_l + m_r +1 \choose m_r +1} {m_r +1 \choose 2} T(\psi_{n_l}) T(\psi_{n_r})\\
&+ (m_l+m_r+1) {m_l + m_r \choose n_l} T(\psi_{n_l}) T(\psi_{n_r})
\end{align*}
\begin{align*}
&= {m_l + m_r \choose m_l} T(\psi_{n_l}) T(\psi_{n_r}) \left(\frac{m_l + m_r + 1}{m_l+1} \frac{(n_l+1)n_l}{2} + \frac{m_l+m_r+1}{m_r+1} \frac{(n_r+1)n_r}{2} + 1  \right)\\
&= T(\psi_n) \left( \frac{ (n-1)(n_l+n_r+2)}{2} + 1 \right)\\
&= T(\psi_n) \left( \frac{ (n-1)(n+2)}{2} + \frac{2}{2} \right)\\
&= {n+1 \choose 2} T(\psi_n)
\end{align*}


\begin{figure}
\centering
\subfloat{ \includegraphics[width=.4\textwidth]{UnorderedSeq.eps} }
\subfloat{ \includegraphics[width=.4\textwidth]{OrderedSeq.eps} }
\caption{ {\bf (left)} The tree building process in Section \ref{sec:uniform_trees} has 5 locations in which to add the new point. {\bf (right)}  Since the order of the internal nodes matters for the coalescent, adding a new point on the rightmost branch would have two different meanings depending on whether it joined before or after C and D do, so branch E in the left figure must be split into two.  }
\label{fg:coalescent_structure}
\end{figure}



\section{Branch Lengths}
\label{sec:branches}

Given a tree structure $\psi$, we can sample branch lengths.  Consider the following construction similar to a stick-breaking process:  Start with a stick of unit length.  Travelling down the given $\psi$, at each split point duplicate the current stick into two sticks.  Then, sample a Beta random variable $B$ for each of the two sticks where the corresponding children are not leaves.  $B$ will be the proportion of the remaining stick attributed to that branch of the tree until the next split point (sticks afterwards will be of length proportional to $(1-B)$).  Note that any distribution that samples from the unit interval may be used.  See Figure \ref{fg:samples} for samples from this prior.  

There is a single Beta random variable attributed to each internal node of the tree (except the root, which has $B$ set to 1).  Since the order in which we see the data does not affect the way in which we sample these stick proportions, the process remains exchangeable.  Denote the Beta variable associated with the internal node $i$ as $B_i$, and denote pairs $(\psi_n, \{B_i\} )$ as $\pi$, ie a tree structure with branch lengths.  For notational convenience, we will define $B_i = 1$ for leaf nodes.


\begin{figure}[ht]
\centering
\subfloat{ \includegraphics[width=0.4\textwidth]{beta11_sample.eps} }
\subfloat{ \includegraphics[width=0.4\textwidth]{beta1-1_-9_sample.eps} }

\subfloat{ \includegraphics[width=0.4\textwidth]{beta22_sample.eps} }
\subfloat{ \includegraphics[width=0.4\textwidth]{beta42_sample.eps} }
\caption{ {\bf (top left)} A sample from the described prior with stick-breaking parameter Beta(1,1) (uniform). {\bf (top right)} A sample using Beta(1.1,0.9). {\bf (bottom left)} A sample using Beta(2,2). {\bf (bottom right)} A sample using Beta(4,2). }
\label{fg:samples} 
\end{figure}

%\section{Likelihoods and Inference}

\section{Brownian Motion Likelihood and Inference}
Given a tree $\pi$ we can define a likelihood for continuous data $x_i\in \mathbb{R}^p$ using Brownian motion.  We denote the length of each branch segment of the tree $s_i$.  Data is generated as follows: we start at some unknown location in $\mathbb{R}^p$ at time $t=0$ and immediately split into two independent Wiener processes (with parameter $\Lambda$), each denoted $y_i$ where $i$ is the index of the relevant branch in $\pi$.  Each of the processes evolve for times $s_i$, then, a new independent Wiener process is instantiated at the time of each split, and this process continues until all process reach the leaves of $\pi$ (i.e. $t=1$), at which point the $y_i$s at the leaves are associated with the data $\bx$.  This is a similar likelihood to the ones used for Dirichlet Diffusion Trees and the Coalescent for continuous data.

The likelihood $p(\bx|\pi)$ can be calculated using message passing.  As in \cite{BayesianAgglomerative}, by marginalizing out from the leaves towards the root, the message at an internal node $i$ is a Gaussian with mean $\hy_i$ and variance $\Lambda \nu_i$, where
\begin{align*}
\nu_i^{-1} &= (\nu_{l_i} + s_{l_i})^{-1} + (\nu_{r_i} + s_{r_i})^{-1} \\
\hy_i &= (\frac{ \hy_{l_i}}{\nu_{l_i}+s_{l_i}  } + \frac{\hy_{r_i}}{\nu_{r_i} + s_{r_i}}) \nu_i 
\end{align*}
We can write the likelihood as:
\[ p(\bx|\pi) = \prod_{i=1}^{n-1} Z_i(\bx,\pi) \]
where
\begin{align*}
Z^{(2)}_i(\bx,\pi) &= |2\pi \hL_i|^{-\frac{1}{2}} \exp\left(-\frac{1}{2} ||\hy_{r_i} - \hy_{l_i}||_{\hL_i}^2\right) \\
\hL_i &= \Lambda(\nu_{l_i} + \nu_{r_i} + s_{l_i} + s_{r_i})
\end{align*}

However, message passing needn't be done from leaves to root.  To pass a message from a single node to another (say from the left subtree to the root):
\begin{align*}
\nu_i &= \nu_{l_i} + s_{l_i}\\
\hy_i &= \hy_{l_i}\\
Z^{(1)}_i(\bx,\pi) &= 1
\end{align*}
To pass a message from three nodes to one node (say to an internal node from its parent and two children):
\begin{align}
\nu_i^{-1} &= (\nu_{p_i}+s_i)^{-1} + (\nu_{l_i} + s_{l_i})^{-1} + (\nu_{r_i} + s_{r_i})^{-1} \\
\hy_i &= \left(\frac{\hy_{p_i}}{\nu_{p_i}+s_i} + \frac{\hy_{l_i}}{\nu_{l_i}+s_{l_i}} + \frac{\hy_{r_i}}{\nu_{r_i}+s_{r_i}}\right)\nu_i \\
\label{eq:Z3} Z^{(3)}_i(\bx,\pi) &= |2\pi \Lambda|^{-1} {\nu^*}^{-\frac{k}{2}} \exp\left( -\frac{1}{2} \left( \nu_{p_i}^* ||\hy_{l_i} - \hy_{r_i}||_{\Lambda\nu^*}^2 + \nu_{r_i}^* ||\hy_{p_i} - \hy_{l_i}||_{\Lambda\nu^*}^2 + \nu_{l_i}^* ||\hy_{p_i} - \hy_{r_i}||_{\Lambda\nu^*}^2    \right)   \right) \\
\nu_{p_i}^* &= \nu_{p_i} + s_i \\
\nu_{l_i}^* &= \nu_{l_i} + s_{l_i} \\
\nu_{r_i}^* &= \nu_{r_i} + s_{r_i} \\
\nu^* &= \nu_{p_i}^*\nu_{l_i}^* + \nu_{l_i}^*\nu_{r_i}^* + \nu_{r_i}^*\nu_{p_i}^*
\end{align}

An MCMC procedure that samples from the posterior distribution over $\pi$ can operate as follows.  First, a random internal node $l$ is pruned from the tree (so that its parent $p_l$ has no parent and only one child), giving the pruned subtree $S_l$ and remaining tree $\pi_l$.  Message passing is performed on the remaining tree, but with additional ``downward'' messages.  

Consider a node $i$ in the tree, if the nodes $u(i)$ are the descendants of $i$ and the nodes $d(i)$ are the nodes obtained by pruning $u(i) \cup \{i\}$ from the original tree, then $u(i)$ correspond to the nodes that are integrated out by the upwards messages, and the $d(i)$ are the nodes integrated out by the downwards messages.  Thus, to calculate the likelihood of adding a pruned subtree back into the original tree above node $i$, we only need the message from $i$ and its parent $p_i$, as well as the message from $l$.  The prior probability for adding the parent of $l$ in the segment of $(i,p_i)$ (with times $t_i$ and $t_p$) at time $t$ is:

\begin{equation}
\label{eq:prior_add}
p(\pi_l(S,i,t) | c_{p_l} = \{l,i\} ) \propto \left(1-\frac{t}{t_p}\right)^\alpha \left(\frac{t}{t_p}\right)^\beta \left(1-\frac{t_i}{t}\right)^\alpha \left(\frac{t_i}{t}\right)^\beta \left(1-\frac{t_l}{t}\right)^\alpha \left(\frac{t_l}{t}\right)^\beta 
\end{equation}

Where $\pi_l( S, i, t)$ is the tree formed by attaching $S$ above node $i$ at time $t$ and $c_i$ denotes of the children of $i$.  By accounting for the terms of the probability of $\pi_l$ that are lost when adding $S_l$ into the segment of $(i,p_i)$ (which are constants w.r.t. $t$), and taking the product of \ref{eq:Z3} and \ref{eq:prior_add} we get the joint posterior of $\pi_l$:


\begin{equation}
\label{eq:post}
p(\pi_k(S_l,i,t) | X) \propto Z^{(3)}_{p_l}(\bx,\pi_l(S_l, i, t) ) \frac{Z^{(2)}_i(\bx,\pi_l)}{Z^{(3)}_i(\bx,\pi_l) } \frac{p(\pi_k(S_l,i,t)|c_{p_l} = \{l,i\} ) }{ Beta( 1 - \frac{t_i}{t_p}; \alpha,\beta)}
\end{equation}

$p(\pi_l(S_l,i,t) |X)$ defines the distribution from which we would like to sample.  Inference methods for the Dirichlet Diffusion Tree involve Metropolis-Hastings moves in which samples from the prior are proposed, and also slice sampling moves in which an internal node is sampled a different divergence time along the path from the root to a particular terminal node.  However, these slice sampling steps do not allow arbitrary moves across the tree, the MH steps are required for efficient exploration of the space of trees.

We propose a slice sampler that can propose adding $S_l$ to any segment in $\pi_l$.  For a fixed $i$, $p(\pi_l(S_l,i,t) |X$ is typically unimodal, and only can have a small number of modes at most.  If we can find all of the modes of the posterior, we can easily find intervals that contain positive probability density for slice sampling moves.  Thus this augmented slice sampling procedure will mix as quickly as slice sampling on a single unimodal distribution.

Taking the derivative of the log posterior, simplifying and removing the common denominator, we arrive at a 7th degree polynomial whose zeros indicate the local extrema of the posterior for a given segment:

\begin{align}
\label{eq:poly}
A(t) &= (a_3 t^3+a_2 t^2 + a_1 t + a_0)(b_4 t^4 + b_3 t^3 + b_2 t^2 + b_1 t)\\
&+ (c_4 t^4 + c_3 t^3 + c_2 t^2  + c_1 t + c_0)(d_3 t^3+d_2 t^2 + d_1 t + d_0)
\end{align}

Where the coefficients are defined as:


\begin{center}
\begin{tabular}{c|ccccc}
 & 4 & 3 & 2 & 1 & 0\\
\hline
$a$ & $0$ & $-k$ & $3k\tnu_p-\frac{\td}{2}$ & $k\tnu-2k\tnu_p^2-\nu_d$ & $\tnu_p\nu_d-k\tnu\tnu_p-\frac{\tnu\td}{2}$ \\
$b$ & $1$ & $-\tv_3$ & $\tv_2$ & $-\tv_1$ & $0$\\
$c$ & $1$ & $-4\tnu_p$ & $4\tnu_p^2-2\tnu$ & $4\tnu_p\tnu$ & $\tnu^2$\\
$d$ & $0$ & $\alpha-\beta$ & $(\beta-1)\tv_3$ & $-(\alpha+\beta-2)\tv_2$ & $(2\alpha+\beta-3)\tv_1$
\end{tabular}

\end{center}

And where
\begin{align*}
\tnu &= \tnu_p\tnu_l + \tnu_l\tnu_r + \tnu_r\tnu_p\\
\td &= (d_{pr} + d_{pl} - d_{lr})\\
\nu_d &= \tnu_p d_{lr} + \tnu_l d_{pr} + \tnu_r d_{pl}\\
\tnu_p &= \nu_p+t_p\\
\tnu_l &= \nu_l-t_l\\
\tnu_r &= \nu_r-t_r\\
d_{ij} &= ||\hy_i -\hy_j||^2_{\Lambda}\\
\tv_1 &= t_p t_l t_r\\
\tv_2 &= t_p t_l + t_l t_r + t_r t_p\\
\tv_3 &= t_p+t_l+t_r
\end{align*}

Thus we can compute $i$th coefficient of $A(t)$ by computing the matrix $ab^T+cd^T$ and summing along the elements whose indices sum to $i$.  Using standard tools for finding roots of polynomials, all of the roots of $A(t)$ can be found, and the real roots in the range $(\max(t_l,t_i), t_p)$ signify the extrema of the posterior for the segment above $i$.

The above table only applies when both the removed subtree and proposed insertion point are not segments direcly above leaves.  As the lengths of segments directly above leaves are fully determined by the Beta r.v.s of the internal nodes, there is no prior contribution for the length of that segment, which results in simpler forms of $A(t)$, which manifests itself as simpler forms of $b$ and $d$.  If the proposed segment and pruned subtree have only one non-leaf child among them, which sits at time $t_n$, then $b$ and $d$ are defined by:

\begin{center}
\begin{tabular}{c|cccc}
 & 3 & 2 & 1 & 0\\
\hline
$b$ & $1$ & $-(t_p+t_n)$ & $t_pt_n$ & $0$\\
$d$ & $0$ & $\alpha-1$ & $0$ & $-(\alpha-1)t_p t_n$
\end{tabular}

\end{center}

If both children are leaves:

\begin{center}
\begin{tabular}{c|cccc}
 & 2 & 1 & 0\\
\hline
$b$ & $1$ & $-t_p$ & $0$\\
$d$ & $0$ & $\alpha+\beta-2$ & $-(\beta-1)t_p $
\end{tabular}

\end{center}


Alternatively, we can simply use optimization techniques to find the local modes and rule out that we haven't missed any.

\subsection{Hyperparameter Inference}

As we do not know the structure of the data beforehand, we may not want to determine the specific values of $\alpha,\beta$ and $\Lambda$ beforehand.  Thus we define hyperpriors on these parameters and infer them as well.

For computational simplicity we assume the form $\Lambda = kI$ for the Brownian motion covariance parameter.  We use an Inverse-Gamma prior on $k$, so that $k^{-1} \sim Gamma(\kappa,\theta)$.  Then, the posterior for $k^{-1}$ is given by:

\[ k^{-1} | X \sim Gamma\left( \frac{(N-1)p}{2} + \kappa, \frac{1}{2} \sum_{i=1}^{N-1} d_{l_i,r_i} + \theta\right) \]

where $N$ is the number of datapoints, $p$ is the dimension, and $d_{l_i,r_i} = \frac{||\hy_{r_i} - \hy_{l_i}||^2}{\nu_{l_i} + \nu_{r_i} + s_{l_i} + s_{r_i}}$.


By putting a $Gamma(\xi,\lambda)$ prior on $\alpha-1$ and $\beta-1$, we achieve a posterior for these parameters:

\[ p(\alpha, \beta| \xi,\lambda,X) \propto (\alpha-1)^\xi (\beta-1)^\xi e^{-\lambda(\alpha-1 + \beta-1)}     \prod_{i=1}^{N-1} \frac{1}{B(\alpha,\beta)} \left(1-\frac{t_i}{t_{p_i}}\right)^{\alpha-1} \left(\frac{t_i}{t_{p_i}}\right)^{\beta-1} \]

This posterior is log-concave and is thus unimodal.  We perform slice sampling to update $\alpha$ and $\beta$.
\subsection{Predictive Density}
Given a set of samples from the posterior, we can approximate the posterior density estimate by sampling a test point located at $y_t$ into each of these trees repeatedly and approximating $p(y_t | X)$ as:
\[ p(y_t |X ) = \int p(y_t|\pi,X) p(\pi|X) d\pi = \int d\pi p(\pi|X) \int d\pi' p(\pi',y_t|\pi)  \approx \sum_{\pi_i \sim \pi|X} \sum_{\pi'_j \sim p(\pi'_j|\pi_i) } p(y_t|\pi_j')  \]

where $p(\pi' | \pi) = \int p(\pi',y_t|\pi dy_t)$.  By integrating out $y_{l_i}$ in \eqref{eq:Z3}, we get an modification of \eqref{eq:post} that is proportional to $p(\pi' | \pi)$.  Slice sampling from this gives us several new trees $\pi_j'$ for each $\pi_i$, where one of the leaves is not observed.  $p(y_t|\pi'_j)$ is then available by message passing to the leaf associated with $y_t$, which results in a Gaussian over $y_t$.  Thus the final approximated density is a mixture of Gaussians with a component for each of the $\pi'_j$.


Performing the aforementined integration (after replacing $\hy_{l_i}$ with $y_t$), we get:

\[\int dy_t Z_i^{(3)}(\bx,\pi') = |2\pi \Lambda|^{-\frac{1}{2}} (\nu_{r_i}^* + \nu_{p_i}^*)^{-\frac{k}{2}} \exp\left(  -\frac{1}{2} (\nu_{l_i}^* + ({\nu_{r_i}^*}^{-1} + {\nu_{p_i}^*}^{-1})^{-1}) ||\hy_{r_i} - \hy_{p_i}||_{\Lambda \nu^*}^2  \right)  \]

\section{Infinite Sites Model}

Given a tree $\pi$, the infinite-sites model gives a prior over binary matrices, which can be used as a latent feature model akin to the Indian Buffet Process.  The infinite-sites model is a mutation process in which mutations can only occur in a locus at most once.  Given the assumption that each datapoint starts at a state of an infinite length vector of all 0s, flipping elements to 1 for each mutation, a tree can be constructed deterministically from a dataset, assigning mutations to particular branch segments.  Given data $x$ and a likelihood model $P(x|z)$, we want to sample tree structures (and thus binary matrices $z$) from the corresponding posterior.  The binary matrix $z$ can be characterized by the number of mutations $m$ found on each branch of the tree.

Under the infinite-sites model the number of mutations $m_i$ on a particular branch of length $V_i = v_i$ is $\Poisson(\theta v_i)$.  Thus, using the above model for trees, we get the probability for a branch $i$ of length $v_i$ having $m_i$ mutations: 

\begin{align*}  P(M_i = m_i | V_i = v_i )  &=  \frac{(\theta v_i)^{m_i}}{m_i!} e^{-\theta v_i}  \\ 
&=  \left(\theta b_i \prod_{j \in \ancestors(i)} (1-b_j) \right)^{m_i} \frac{1}{m_i!}\exp\left(-\theta b_i \prod_{j \in \ancestors(i)} (1-b_j) \right) \\
&= \frac{\theta^{m_i}}{m_i!} b_i^{m_i} \prod_{j\in \ancestors(i)} (1-b_j)^{m_i}  \exp\left(-\theta b_i \prod_{j \in \ancestors(i)} (1-b_j) \right) 
\end{align*}
where $\ancestors(i)$ is the set of ancestor nodes of node $i$, and $\descendants(i)$ is the set of descendants, including leaf nodes.
Multiplying across all branches and collecting all terms pertaining to a particular $b_i$, and multiplying in the $\Beta(\alpha,\beta)$ prior on $b_i$:
\[ p(B_i = b_i | B_{\neg i}) \propto \] 
\[b_i^{\alpha+m_i} (1-b_i)^\beta \exp\left(-b_i \theta \prod_{j \in \ancestors(i) } (1-b_j) \right) \prod_{j \in \descendants(i)} (1-b_i)^{m_j} \exp\left( - (1-b_i) \theta b_j \prod_{k \in \ancestors(j)\setminus i} (1-b_k)   \right)  \]
\[ = b_i^{\alpha + m_i} (1-b_i)^{\beta + l_i} e^{  -\theta \bar{v}_i b_i  - \theta L_i (1-b_i)}  \propto b_i^{\alpha + m_i} (1-b_i)^{\beta + l_i} e^{-\theta (\bar{v}_i - L_i) b_i}  \]
where $l_i = \sum_{j \in \descendants(i)} m_j$ is the total number of mutations originated after branch $i$ terminates, and $L_i$ is the total length of the subtree under branch $i$, scaled by $\frac{1}{1-b_i}$, and similarly $\bar{v}_i = \frac{v_i}{b_i}$.

Thus, the posterior conditional of $B_i$ can be written as the product of a Beta pdf and an exponential term, a unimodal distribution (for $\alpha,\beta>1$) confined on the unit interval, so it should be relatively easy to sample from the $B_i$s using Metropolis-Hastings.  Given the $B_i$s, we can sample posterior $M_i$s given a data observation likelihood $P(x|M)$.  We can propose to move datapoints to other of the regions of the tree using the likelihood $P(x|M)$ and a "local prior" based on $p(B_i | B_{\neg i})$.  Since moving a branch corresponds to joining a parent-child pair of branch segments in one region and splitting a branch segment in another, the update to the posterior (conditioned on the remaining branches) can be computed locally, so it should be efficient to sample in the space of tree structures.
\end{document}
