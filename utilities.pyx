#!python
#cython: cdivision=True
#cython: profile=True
#cython: wraparound=False
#cython: boundscheck=True
import cython
cimport cython
cimport cython.view
import numpy as np
cimport numpy as np

import time
import scipy.stats as sts
import scipy.special as sp
import slice_sampler as ssamp
cimport slice_sampler as ssamp

from libc.stdlib cimport malloc
from libc.math cimport log,M_PI,fmod,exp,expm1
from libc.float cimport DBL_MAX

from collections import deque
#ctypedef np.double_t DTYPE_t

import matplotlib.pyplot as plt

def prune_branch(ind,C,P):
    pp = P[ind]
    gp = P[pp]
    
    gp_llrr = np.nonzero(C[gp,:] == pp)[0][0]
    pp_llrr = np.nonzero(C[pp,:] == ind)[0][0]

    #ll = list(C[pp,:])
    #ll.remove(ind)
    #pp_sibling = ll[0] #list(C[pp,:]).remove(ind)

    pp_sibling = C[pp,1-pp_llrr]

    C[gp,gp_llrr] = pp_sibling 
    P[pp_sibling] = gp

    C[pp, 1-pp_llrr] = -1
    P[pp] = -1

def insert_branch(pruned_ind,ins_ind,C,P):
    gp = P[ins_ind]
    pp = P[pruned_ind]

    gp_llrr = np.nonzero(C[gp,:] == ins_ind)[0][0]
    pp_llrr = np.nonzero(C[pp,:] != pruned_ind)[0][0]

    C[gp,gp_llrr] = pp
    C[pp,pp_llrr] = ins_ind

    P[ins_ind] = pp
    P[pp] = gp


def compute_M(C):
    (tnm1,_) = np.shape(C)

    M = np.zeros( tnm1, dtype=np.int)

    compute_M_h(tnm1-1,C,M)

    return M

def compute_M_h(ind,C,M):

    (tnm1,_) = np.shape(C)
    N = (1+tnm1)/2

    if ind < N:
        return 0

    M[ind] = compute_M_h(C[ind,0],C,M) + compute_M_h(C[ind,1],C,M)+1

    return M[ind]

#approximates the predictive density constants for a single tree
def predictive_density(X,nu0,C,P,varr,k,alpha,beta,nsamp,msamp):

    (N,p) = np.shape(X)
    cdef np.ndarray[ITYPE_t, ndim=2] Cn = C.copy()
    cdef np.ndarray[ITYPE_t, ndim=1] Pn = P.copy()

    M = compute_M(C)

    Cn = np.r_['0,2',Cn, np.zeros(2,dtype=np.int) ]
    Pn = np.r_[Pn, -2]

    varr = np.r_[varr,0]
    
    (logZ_u,Mu_u,nu_u) = compute_messages(X,nu0,Cn,varr,k)
    (logZ_d,Mu_d,nu_d) = compute_downward_messages(X,Cn,Pn,varr,k,Mu_u,nu_u)

    Mu_u = np.r_['0,2',Mu_u, np.zeros(p) ]
    nu_u = np.r_[nu_u, 0]

    Mu_d = np.r_['0,2',Mu_d, np.zeros(p) ]
    nu_d = np.r_[nu_d, 0]


    M = np.r_[M, 0]   

    cdef int testp_ind = 2*N-1

    cdef np.ndarray[DTYPE_t,ndim=2] pconsts_arr = jump_priors(testp_ind, Cn,Pn, varr, M, alpha, beta, predictive=True)

    cdef np.ndarray[DTYPE_t,ndim=2] consts_arr = jump_likelihoods(testp_ind, Cn, Pn, varr, k, Mu_u, nu_u, Mu_d, nu_d, Mu_u, nu_u, logZ_u, logZ_d, logZ_u, predictive=True) 


    cdef double[:,:] pconsts = pconsts_arr 
    cdef double[:,:] consts = consts_arr
    cdef double[:] v = varr

    cdef double logdetLam = p*log(k)

    cdef int mi
    cdef int t_ind = 0
    cdef double vmin,vmax

    cdef double[:] fpout = np.zeros(2)
   
    cdef double[:] maxima = -np.ones(7) 
    cdef double[:] minima = -np.ones(7) 
    cdef int mxi,mni
    
    cdef int i


    T = []
    I = []
    Mx = []


    while t_ind < 2*N-2:
 
        vmin = max(v[t_ind],0)
        vmax = v[Pn[t_ind]]



        mxi=0
        mni=0 
        compute_extrema(&eval_predictive_pdf,&eval_predictive_grad, p, v, vmin, vmax, alpha, beta, t_ind, pconsts, consts,1,10.0**-6,0,0,vmin,vmax,maxima,minima,&mxi,&mni,fpout)


        if mxi > 1:
#            print "multiple modes"

            #assert(mxi == mni-1)

            for mi in range(mxi):
                mm = maxima[mi]
                tl = minima[mi]
                tr = minima[mi+1]

                T.append( [tl,tr])
                I.append(t_ind)
                Mx.append(mm)



        elif mni == 2 and maxima[0] != minima[0] and maxima[0] != minima[1]: #and len(set(minima).difference([vmin,vmax])) == 0 : #only two minima, ie the endpoints of the interval #and not (prune_ind >= N and t_ind >= N): # and prune_ind != 150:
            T.append( [vmin,vmax] )
            I.append(t_ind)
            Mx.append(maxima[0])
        else:
           
            print np.asarray(maxima)
            print np.asarray(minima) 
            print vmin
            print vmax
            assert(0)        
            print "This shouldn't happen"
        #end

        t_ind += 1
#            print t_ind
#            print Pn[t_ind]
#            print pconsts[t_ind,4]
#            print v[Pn[t_ind]] 
    #end


    cdef int ii
    cdef double tt

    T = np.array(T)

    cdef int It = np.random.randint(len(T))
    t0 = np.random.uniform(low=T[It,0], high=T[It,1])
    I = np.array(I)
    Mx = np.array(Mx)

    segs = np.zeros(nsamp,dtype=np.int)
    vpred = np.zeros(nsamp)
    nupred = np.zeros( nsamp )
    Mupred = np.zeros( (nsamp,p) )


#    val = eval_predictive_pdf(t0,I[It], pconsts,consts, alpha,beta,v,logdetLam,p)
#    print val
#    print np.asarray(pconsts[I[It]])
#    print np.asarray(consts[I[It]])
#
    cdef int pp = <int>pconsts[I[It],2]
    cdef int ll = <int>pconsts[I[It],0]

#    print v[ll]
#    print v[pp]
#    print T[It]
#    print t0
#    print T
    for i in range(nsamp): 

        seed = np.random.randint(100000)
        ssamp.sample(&eval_predictive_pdf,I,T,Mx,It ,t0, msamp, pconsts,consts,alpha,beta,varr,logdetLam, p, seed, &ii, &tt   )

 #       print (It,t0)

        It = ii
        t0 = tt

        seg = I[It]

        segs[i] = seg
        vpred[i] = tt

        nus1 = nu_u[seg] + tt-v[seg]
        nus2 = nu_d[seg] + v[P[seg]]-tt

        nupred[i] = 1/(1/nus1+1/nus2)
        Mupred[i] = (Mu_u[seg]/nus1 + Mu_d[seg]/nus2)*nupred[i]

    #end


    return (Mupred,nupred,vpred,segs,nu_u,nu_d)

#a hackish use of the slice sampler I already have, but oh well...
def update_hyp_alpha_beta(P,v,alpha,beta,xia,lama, xib, lamb, nsamp, msamp ):

    cdef np.ndarray[ITYPE_t,ndim=1] I = np.zeros(1,dtype=np.int)
    cdef np.ndarray[DTYPE_t,ndim=2] T = np.array([1.0,40.0]).reshape((1,2)) #max interval size
    cdef np.ndarray[DTYPE_t,ndim=1] Mx = np.zeros(1)

    cdef double atmp = alpha
    cdef double btmp = beta

    cdef double xi_c 
    cdef double lam_c 

    cdef long[:] Pview = P
    cdef double[:] vview = v

    cdef int N = len(P)
    N = (N+1)/2
    cdef int i,ii
    cdef double tt

    for i in range(nsamp):
    
        seed = np.random.randint(100000)

        xi_c = xia
        lam_c = lama

        I[0] = 0
        Mx[0] = atmp
        ssamp.va_sample(&eval_abpdf,I,T,Mx,0,atmp,msamp,seed,&ii,&atmp,&Pview,&vview,atmp,btmp,xi_c, lam_c,N  )

        seed = np.random.randint(100000)

#        print "Alpha: %f" % alpha

        xi_c = xib
        lam_c = lamb

        I[0] = 1
        Mx[0] = btmp

        ssamp.va_sample(&eval_abpdf,I,T,Mx,0,btmp,msamp,seed,&ii,&btmp ,&Pview,&vview,atmp,btmp,xi_c, lam_c,N )

#        print "Beta: %f" % beta
    #end

    return (atmp,btmp)

#use update_df=False for non-heavy tailed noise model
def update_df_sig0(df,sig0_2,X,sigs,lam,a0,b0,nsamp,msamp,update_df=True):
    cdef np.ndarray[ITYPE_t,ndim=1] I = np.zeros(1,dtype=np.int)
    cdef np.ndarray[DTYPE_t,ndim=2] Tdf = np.array([10.0**-10,40.0]).reshape((1,2)) #max interval size
    cdef np.ndarray[DTYPE_t,ndim=2] Tsig = np.array([10.0**-10,40.0]).reshape((1,2)) #max interval size
    cdef np.ndarray[DTYPE_t,ndim=1] Mx = np.zeros(1)

    cdef int N,p
    (N,p) = np.shape(X)
    #cdef double[:] dists = np.sum( (X-Y)*(X-Y),1)

    #cdef double dsum = np.sum( (X-Y)*(X-Y))

    cdef double[:] sigs_c = sigs

    #print "dsum: %f" %dsum

    cdef double df_tmp = df
    cdef double s_tmp = sig0_2

    cdef double lamc = lam
    cdef double a0_c = a0
    cdef double b0_c = b0

    cdef int ii

    for i in range(nsamp):

        if update_df:        
            seed = np.random.randint(100000)
            I[0] = 0
            Mx[0] = df_tmp

            ssamp.va_sample(&eval_noisepdf,I,Tdf,Mx,0,df_tmp, msamp, seed, &ii, &df_tmp, df_tmp, s_tmp, a0_c, b0_c, lamc, &sigs_c, N,p )
        #end
 
        seed = np.random.randint(100000)
        I[0] = 1
        Mx[0] = s_tmp

        ssamp.va_sample(&eval_noisepdf,I,Tsig,Mx,0,s_tmp, msamp, seed, &ii, &s_tmp, df_tmp, s_tmp, a0_c, b0_c, lamc, &sigs_c, N,p )

 
    #end

    return (df_tmp,s_tmp)


def jump_structure_opt(X,nu0,int prune_ind,np.ndarray[ITYPE_t,ndim=2] C, np.ndarray[ITYPE_t,ndim=1] P,np.ndarray[DTYPE_t] varr, double k, double alpha, double beta,Mu_u,nu_u, logZ_u, printflag = False, sX = False, msamp=20):

    cdef int N,p
    (N,p) = np.shape(X)

    cdef np.ndarray[ITYPE_t, ndim=2] Cn = C.copy()
    cdef np.ndarray[ITYPE_t, ndim=1] Pn = P.copy()
    logZ_un = logZ_u.copy()

    prune_sib = C[P[prune_ind], np.nonzero(C[P[prune_ind]] != prune_ind)[0][0]]

    prune_branch(prune_ind,Cn,Pn)
    M = compute_M(Cn)

    cdef int i

    Mu_pd = np.zeros( np.shape(Mu_u))

    if sX != None:
     
        Mu_pu = sX
        Mu_u = sX

        #code using downward messages expects the messages to be stored at the relevant child
        inds = np.argsort(varr)
        for i in inds:
            Mu_pd[i] = sX[P[i]]
        #end

        (logZ_u, logZ_d) = compute_sl_messages(sX,C,P,varr,k)
        (logZ_pu, logZ_pd) = compute_sl_messages(sX,Cn,Pn,varr,k)



        nu_pu = np.zeros( 2*N-1)
        nu_pd = np.zeros( 2*N-1)
        nu_u = np.zeros( 2*N-1)
 
    else:
        (logZ_pu,Mu_pu,nu_pu) = compute_messages(X,nu0,Cn,varr,k)
        (logZ_pd,Mu_pd,nu_pd) = compute_downward_messages(X,Cn,Pn,varr,k,Mu_pu,nu_pu)
    #end

    cdef np.ndarray[DTYPE_t,ndim=2] pconsts_arr = jump_priors(prune_ind,Cn,Pn,varr,M,alpha,beta)

    cdef np.ndarray[DTYPE_t,ndim=2] consts_arr = jump_likelihoods(prune_ind,Cn,Pn,varr,k,Mu_pu,nu_pu,Mu_pd,nu_pd, Mu_u,nu_u, logZ_pu, logZ_pd, logZ_u, False)  

    cdef double[:,:] pconsts = pconsts_arr 
    cdef double[:,:] consts = consts_arr 




    cdef double[:] v = varr

    cdef double logdetLam = p*log(k)

    cdef int mi
    cdef int t_ind = 0
    cdef double vmin,vmax

    cdef double[:] fpout = np.zeros(2)
   
    cdef double[:] maxima = -np.ones(7) 
    cdef double[:] minima = -np.ones(7) 
    cdef int mxi,mni


    T = []
    I = []
    Mx = []

    

    while pconsts[t_ind,4] == 0  or v[Pn[t_ind]] < v[prune_ind] :
        t_ind += 1

    while t_ind < 2*N-2:
            
 
        vmin = max(v[t_ind],v[prune_ind])
        vmax = v[Pn[t_ind]]



        mxi=0
        mni=0 
        compute_extrema_c( p, v, vmin, vmax, alpha, beta, t_ind, pconsts, consts,1,10.0**-6,0,0,maxima,minima,&mxi,&mni,fpout)


        if mxi > 1:
#            print "multiple modes"

            #assert(mxi == mni-1)

            for mi in range(mxi):
                mm = maxima[mi]
                tl = minima[mi]
                tr = minima[mi+1]

                if np.isnan(tl) or np.isnan(tr):
                    print (vmin,vmax)
                    print np.asarray(maxima)
                    print np.asarray(minima)
                    print (tl,tr)
                    print (t_ind,v[t_ind])
                    print (prune_ind,v[prune_ind])
                    print (P[t_ind],v[P[t_ind]])


#                    print Mu_pu[t_ind] 
#                    print Mu_pd[t_ind] 
#                    print Mu_pu[prune_ind] 
#                    print Mu_pd[prune_ind] 
#                    print Mu_pu[P[t_ind]] 
#                    print Mu_pd[P[t_ind]] 
                    plot_func(vmin,vmax,(vmax-vmin)*10.0**-4,&eval_pdf, t_ind,pconsts,consts,alpha,beta,v,p)
                    
                    time.sleep(100)



                T.append( [tl,tr])
                I.append(t_ind)
                Mx.append(mm)

                if t_ind == prune_sib and v[P[prune_ind]] <= tr and v[P[prune_ind]] >= tl:
                    It = len(I)-1
                #end


        elif mni == 2 and maxima[0] != minima[0] and maxima[0] != minima[1]: #and len(set(minima).difference([vmin,vmax])) == 0 : #only two minima, ie the endpoints of the interval #and not (prune_ind >= N and t_ind >= N): # and prune_ind != 150:
            T.append( [vmin,vmax] )
            I.append(t_ind)
            Mx.append(maxima[0])
            if t_ind == prune_sib:
                It = len(I)-1
            #end
        else:
          
            print np.asarray(maxima)
            print np.asarray(minima) 
            print vmin
            print vmax
             
            #assert(0)        
            print "This shouldn't happen: attempting a hacky fix be wary of your results"
            minima[0] = vmin
            minima[1] = vmax


        #end

        t_ind += 1
        while t_ind < 2*N-2 and (pconsts[t_ind,4] == 0 or v[Pn[t_ind]] < v[prune_ind]) :
            t_ind += 1
#            print t_ind
#            print Pn[t_ind]
#            print pconsts[t_ind,4]
#            print v[Pn[t_ind]] 
    #end

    seed = np.random.randint(100000)

    cdef int ii
    cdef double tt
#    cdef double f0,fn,fn2






#    f0 = eval_pdf2(v[P[prune_ind]],I[It],pconsts,consts,alpha,beta,v,logdetLam,p  )

    #ssamp.va_sample(&va_eval_pdf,np.array(I),np.array(T),np.array(Mx),It ,v[P[prune_ind]] , 20,seed,&ii,&tt,&pconsts,&consts,alpha,beta,&v,logdetLam, p )

    ssamp.sample(&eval_pdf,np.array(I),np.array(T),np.array(Mx),It ,v[P[prune_ind]] , msamp,pconsts,consts,alpha,beta,varr,logdetLam, p, seed, &ii, &tt   )


    ins_ind = I[ii]
 
#    ins_ind = ii 
#    fn = eval_pdf2(tt,I[ii],pconsts,consts,alpha,beta,v,logdetLam,p  )


    if sX != None:

        ll = prune_ind
        rr = ins_ind
        pp = P[ins_ind]

        Mu_ll = sX[ll]
        Mu_rr = sX[rr]
        Mu_pp = sX[pp]

        s_ll = tt-v[ll]
        s_rr = tt-v[rr]
        s_pp = v[pp]-tt

        (lZ,Munew,nunew) = compute_message_p(np.r_['0,2',Mu_ll, Mu_rr, Mu_pp], np.zeros(3), k, np.r_[s_ll,s_rr,s_pp] )

        sX[P[prune_ind]] = sts.norm.rvs(loc=Munew,scale=np.sqrt(k*nunew))


#        vmin = max(v[ins_ind],v[prune_ind])
#        vmax = v[Pn[ins_ind]]
#
#        plot_func(vmin,vmax,(vmax-vmin)*10.0**-4,&eval_pdf, ins_ind,pconsts,consts,alpha,beta,v,p)
#
#        print sX[ins_ind]
#        print sX[prune_ind]
#        print sX[Pn[ins_ind]]
#
#
#        time.sleep(100)

    #end


    v[P[prune_ind]] = tt
    insert_branch(prune_ind,ins_ind,Cn,Pn)



#    dm_ind = Pn[prune_ind]
#    path = [dm_ind]
#    tmp_ind = dm_ind
#    while Pn[tmp_ind] != -1:
#        path.append(Pn[tmp_ind])
#        tmp_ind = Pn[tmp_ind]
#
#
#    Q = [prune_ind]
#    L = [prune_ind]
#    while len(Q) > 0:
#        cur = Q.pop()
#        ll = Cn[cur,0]
#        rr = Cn[cur,1]
#
#        L.append(ll)
#        L.append(rr)
#
#        if ll >= N:
#            Q.append(ll)
#        if rr >= N:
#            Q.append(rr)
#       
#    #end
#
#
#    
#
#    dsum = np.sum(logZ_pd[path])
#
#    rem = set(range(2*N-1)).difference(path).difference(L)
#           
#    usum = np.sum(logZ_pu[list(rem)  ]) + np.sum(logZ_u[L])
#
#
#
#
#
#
#
#
#
#
#
#    #rem.add(dm_ind)
#    pp = Pn[dm_ind]
#
#    prune_sib = list(Cn[dm_ind])
#    prune_sib.remove(prune_ind)
#    prune_sib = prune_sib[0]
#
#    ll = prune_sib
#    rr = prune_ind
#
#
#    Mu_m = np.c_[Mu_pd[pp],Mu_pu[ll],Mu_u[rr]].T
#    nu_m = np.r_[nu_pd[pp],nu_pu[ll],nu_u[rr]]
#    ss = np.array( [v[pp]-v[dm_ind], v[dm_ind]-v[ll], v[dm_ind]-v[rr]] )
#
#    (lZ3,blah,blah2) = compute_message_p( Mu_m, nu_m, k, ss) 
#
#    M = compute_M(Cn)
#    prior_prob = compute_prior(v,Cn,Pn,alpha,beta,M) #-np.sum(np.log(M[N:]))
#
#
#
#
#
#    (logZ,Mu,nu) = compute_messages(X,Cn,varr,k)
#
#
#
#    print (prune_ind, ins_ind)
#    print (np.sum(logZ)+prior_prob,usum+dsum+lZ3+prior_prob, usum+dsum+prior_prob, prior_prob)
#    print (fn-f0,fn,f0)

    return (Cn,Pn)


def sample_locations(X, Mus, nus, C,P, v,k, sample_leaves=False):

    (tnm1,p) = np.shape(Mus)
    N = (tnm1+1)/2

    inds = np.argsort(v)[N:]

    sX = np.zeros( (tnm1,p) )

    sX[:N] = X 

    Q = deque( [tnm1-1] )
    while len(Q) > 0:
        par = Q.popleft()
        
        ll = C[par,0]
        rr = C[par,1]

        Mu_ll = Mus[ll,:]
        Mu_rr = Mus[rr,:]

        nu_ll = nus[ll]
        nu_rr = nus[rr]

        s_ll = v[par] - v[ll]
        s_rr = v[par] - v[rr]


        if par == tnm1-1:
            (lZ,Muout,nuout) = compute_message_p(np.r_['0,2',Mu_ll, Mu_rr], np.r_[nu_ll,nu_rr], k, np.r_[s_ll,s_rr] )
            sX[par] = sts.norm.rvs(loc=Muout,scale=np.sqrt(k*nuout))
        elif par < N:
            pp = P[par]
            Mu_pp = sX[pp]
            Mu = Mus[par]
            nu = nus[par]

            (lZ,Muout,nuout) = compute_message_p(np.r_['0,2',Mu_pp,Mu], np.r_[0.0, nu], k, np.r_[v[pp]-v[par], 0.0])
            sX[par] = sts.norm.rvs(loc=Muout,scale=np.sqrt(k*nuout))

        else:
            pp = P[par] 
            Mu_pp = sX[pp]
            (lZ,Muout,nuout) = compute_message_p(np.r_['0,2',Mu_ll, Mu_rr, Mu_pp], np.r_[nu_ll,nu_rr, 0], k, np.r_[s_ll,s_rr,v[pp]-v[par]] )

            sX[par] = sts.norm.rvs(loc=Muout,scale=np.sqrt(k*nuout))

        if sample_leaves:
            if par > N:
                Q.append(ll)
                Q.append(rr)
        else:
            if ll >= N: 
                Q.append(ll)
            if rr >= N:
                Q.append(rr)


    #end

    return sX

#def sample_locations(X, Mu_d, nu_d, C,P, v,k):
#    (tnm1,p) = np.shape(Mu_d)
#    N = (tnm1+1)/2
#
#    inds = np.argsort(v)[N:]
#    sX = np.zeros( (tnm1,p) )
#
#    sX[:N] = X 
#    for par in inds:
#
#        ll = C[par,0]
#        rr = C[par,1]
#    
#        pp = P[par]
# 
#        Mu_ll = sX[ll]
#        Mu_rr = sX[rr]
#
#        nu_ll = 0.0
#        nu_rr = 0.0 
#
#        s_ll = v[par]-v[ll]
#        s_rr = v[par]-v[rr]
#
#        if par < tnm1-1: 
#            Mu_pp = Mu_d[par]
#            nu_pp = nu_d[par] 
#            (lZ,Muout,nuout) = compute_message_p(np.r_['0,2',Mu_ll, Mu_rr, Mu_pp], np.r_[nu_ll,nu_rr, 0], k, np.r_[s_ll,s_rr,v[pp]-v[par]] )
#
#            sX[par] = sts.norm.rvs(loc=Muout,scale=np.sqrt(k*nuout))
#        else:
#            (lZ,Muout,nuout) = compute_message_p(np.r_['0,2',Mu_ll, Mu_rr], np.r_[nu_ll,nu_rr], k, np.r_[s_ll,s_rr] )
#
#            sX[par] = sts.norm.rvs(loc=Muout,scale=np.sqrt(k*nuout))
#
#        #end
#
#    #end
#
#    return sX

def compute_sl_messages(sX, C, P, v , k):
    (tnm1,p) = np.shape(sX)
    N= (tnm1+1)/2

    inds = np.argsort(v)[N:]
    lZout = np.zeros( tnm1 )
    lZdout = np.zeros( tnm1 )

    for par in inds:

        ll = C[par,0]
        rr = C[par,1]
    
        pp = P[par]
 
        Mu_ll = sX[ll]
        Mu_rr = sX[rr]

        nu_ll = 0.0
        nu_rr = 0.0 

        s_ll = v[par]-v[ll]
        s_rr = v[par]-v[rr]

        if par < tnm1-1: 
            Mu_pp = sX[pp]
            nu_pp = 0.0
            (lZ,Muout,nuout) = compute_message_p(np.r_['0,2',Mu_ll, Mu_rr], np.r_[nu_ll,nu_rr], k, np.r_[s_ll,s_rr] )
            lZout[par] = lZ 


            (lZ,Muout,nuout) = compute_message_p(np.r_['0,2',Mu_ll, Mu_pp], np.r_[nu_ll, 0], k, np.r_[s_ll,v[pp]-v[par]] )
            lZdout[rr] = lZ
 
            (lZ,Muout,nuout) = compute_message_p(np.r_['0,2', Mu_rr, Mu_pp], np.r_[nu_rr, 0], k, np.r_[s_rr,v[pp]-v[par]] )
            lZdout[ll] = lZ

        else:
            (lZ,Muout,nuout) = compute_message_p(np.r_['0,2',Mu_ll, Mu_rr], np.r_[nu_ll,nu_rr], k, np.r_[s_ll,s_rr] )
            lZout[par] = lZ 


            #(lZ,Muout,nuout) = compute_message_p(np.atleast_2d(Mu_ll), np.atleast_1d(nu_ll), k, np.atleast_1d(s_ll) )
            lZdout[rr] = 0

            #(lZ,Muout,nuout) = compute_message_p(np.atleast_2d(Mu_rr), np.atleast_1d(nu_rr), k, np.atleast_1d(s_rr) )
            lZdout[ll] = 0

        #end
    #end

    return (lZout,lZdout)

def compute_prior(v,C,P,alpha,beta,M):
    tnm1 = len(v)
    N = (tnm1+1)/2
    #structure term
    prior_prob = -np.sum(np.log(M[N:]))

    Q = deque([2*N-2])

    while len(Q) > 0:

        pp = Q.popleft()
        ll = C[pp,0]
        rr = C[pp,1]

        vpp = v[pp]
        vll = v[ll]
        vrr = v[rr]

        if ll >= N:
    
            B1 = 1 - vll/vpp
            prior_prob += beta_logpdf(B1,alpha,beta) 
            Q.append(ll)
        if rr >= N:
            B1 = 1 - vrr/vpp
            prior_prob += beta_logpdf(B1,alpha,beta) 
            Q.append(rr)

    #end 
    
    return prior_prob 


#def jump_structure(X,int prune_ind,np.ndarray[ITYPE_t,ndim=2] C, np.ndarray[ITYPE_t,ndim=1] P,np.ndarray[DTYPE_t] varr, double k, double alpha, double beta,Mu_u,nu_u, logZ_u, printflag = None):
#
#    cdef int N,p
#    (N,p) = np.shape(X)
#
#    cdef np.ndarray[ITYPE_t, ndim=2] Cn = C.copy()
#    cdef np.ndarray[ITYPE_t, ndim=1] Pn = P.copy()
#    logZ_un = logZ_u.copy()
#
#    prune_sib = C[P[prune_ind], np.nonzero(C[P[prune_ind]] != prune_ind)[0][0]]
#
#    prune_branch(prune_ind,Cn,Pn)
#    M = compute_M(Cn)
#    (logZ_pu,Mu_pu,nu_pu) = compute_messages(X,Cn,varr,k)
#    (logZ_pd,Mu_pd,nu_pd) = compute_downward_messages(X,Cn,Pn,varr,k,Mu_pu,nu_pu)
#
#    cdef double[:,:] pconsts = jump_priors(prune_ind,Cn,Pn,varr,M,alpha,beta)
#    cdef double[:,:] consts = jump_likelihoods(prune_ind,Cn,Pn,varr,k,Mu_pu,nu_pu,Mu_pd,nu_pd, Mu_u,nu_u, logZ_pu, logZ_pd, logZ_u, False)  
#
#
#
#
#    cdef double[:] v = varr
#
#    cdef double logdetLam = p*log(k)
#
#    cdef int mi
#    cdef int t_ind = 0
#    cdef double vmin,vmax
#
#    cdef double[:] fpout = np.zeros(2)
#   
#    cdef double[:] maxima = -np.ones(7) 
#    cdef double[:] minima = -np.ones(7) 
#    cdef int mxi,mni
#    
#    cdef int i
#
#    cdef np.ndarray[DTYPE_t, ndim=2] T = np.zeros( (2*N-2,2) )
#
#    for t_ind in range(2*N-2):
#
#        if not (pconsts[t_ind,4] == 0  or v[Pn[t_ind]] < v[prune_ind]):
#          
#            vmin = max(v[t_ind],v[prune_ind])
#            vmax = v[Pn[t_ind]]
#
#            T[t_ind,0] = vmin
#            T[t_ind,1] = vmax
#
#    #end
#
##    T = []
##    I = []
##    Mx = []
##
##
##    while pconsts[t_ind,4] == 0  or v[Pn[t_ind]] < v[prune_ind] :
##        t_ind += 1
##
##    while t_ind < 2*N-2:
##            
## 
##        vmin = max(v[t_ind],v[prune_ind])
##        vmax = v[Pn[t_ind]]
##
##
##
##        mxi=0
##        mni=0 
##        compute_extrema_c( p, v, vmin, vmax, alpha, beta, t_ind, pconsts, consts,1,10.0**-6,0,0,maxima,minima,&mxi,&mni,fpout)
##
##
##        if mxi > 1:
###            print "multiple modes"
##
##            #assert(mxi == mni-1)
##
##            for mi in range(mxi):
##                mm = maxima[mi]
##                tl = minima[mi]
##                tr = minima[mi+1]
##
##                T.append( [tl,tr])
##                I.append(t_ind)
##                Mx.append(mm)
##
##                if t_ind == prune_sib and v[P[prune_ind]] <= tr and v[P[prune_ind]] >= tl:
##                    It = len(I)-1
##                #end
##
##
##        elif mni == 2 and maxima[0] != minima[0] and maxima[0] != minima[1]: #and len(set(minima).difference([vmin,vmax])) == 0 : #only two minima, ie the endpoints of the interval #and not (prune_ind >= N and t_ind >= N): # and prune_ind != 150:
##            T.append( [vmin,vmax] )
##            I.append(t_ind)
##            Mx.append(maxima[0])
##            if t_ind == prune_sib:
##                It = len(I)-1
##            #end
##        else:
##           
##             
##            assert(0)        
##            print "This shouldn't happen"
##        #end
##
##        t_ind += 1
##        while t_ind < 2*N-2 and (pconsts[t_ind,4] == 0 or v[Pn[t_ind]] < v[prune_ind]) :
##            t_ind += 1
###            print t_ind
###            print Pn[t_ind]
###            print pconsts[t_ind,4]
###            print v[Pn[t_ind]] 
##    #end
#
#    seed = np.random.randint(100000)
#
#    cdef int ii
#    cdef double tt
#
# #   ssamp.sample(np.array(I),np.array(T),np.array(Mx),It ,v[P[prune_ind]] , 20,pconsts,consts,alpha,beta,varr,logdetLam, p, seed, &ii, &tt   )
#
#    ssamp.sample_multiple_particle(&eval_pdf,T,prune_sib ,v[P[prune_ind]] , 100,20,pconsts,consts,alpha,beta,varr,logdetLam, p, seed, &ii, &tt   )
#
##    ins_ind = I[ii]
# 
#    ins_ind = ii 
#
#    v[P[prune_ind]] = tt
#    
#    insert_branch(prune_ind,ins_ind,Cn,Pn)
#
#
#    return (Cn,Pn)

#noisy leaves message computations
def compute_messages(np.ndarray[DTYPE_t,ndim=2] X, np.ndarray[DTYPE_t, ndim=1] nu0, np.ndarray[ITYPE_t,ndim=2] C, np.ndarray[DTYPE_t,ndim=1] v, double k):
    (N,p) = np.shape(X)

    Mu = np.zeros( (2*N-1,p) )
    nu = np.zeros( 2*N-1 )

    logZ = np.zeros( 2*N-1 )

    compute_messages_h(X,nu0,N,p,C,v,k, 2*N-2, Mu,nu,logZ)

    return (logZ,Mu,nu)



#def compute_messages(np.ndarray[DTYPE_t,ndim=2] X, np.ndarray[ITYPE_t,ndim=2] C, np.ndarray[DTYPE_t,ndim=1] v, double k):
#    (N,p) = np.shape(X)
#
#    Mu = np.zeros( (2*N-1,p) )
#    nu = np.zeros( 2*N-1 )
#    nu0 = np.zeros(N)
#
#    logZ = np.zeros( 2*N-1 )
#
#    compute_messages_h(X,nu0,N,p,C,v,k, 2*N-2, Mu,nu,logZ)
#
#    return (logZ,Mu,nu)

cdef void compute_messages_h(double[:,:] X, double[:] nu0, int N, int p, long[:,:] C, double[:] v, double k, int ind, double[:,:] Mu_a, double[:] nu_a, double[:] logZ ):
    
    if ind < N:
        Mu_a[ind,:] = X[ind,:]
        nu_a[ind] = nu0[0]
        return 
    #end

    cdef int ll = C[ind,0]
    cdef int rr = C[ind,1]

    compute_messages_h(X,nu0,N,p,C,v,k, ll, Mu_a, nu_a, logZ)
    compute_messages_h(X,nu0,N,p,C,v,k, rr, Mu_a, nu_a, logZ)

    cdef double t_l = v[ll]
    cdef double t_r = v[rr]
    cdef double t = v[ind]

    cdef double nu_l = nu_a[ll]
    cdef double nu_r = nu_a[rr]

    cdef double nu = 1/( 1/(nu_l + t-t_l) + 1/(nu_r + t-t_r))
    cdef int i
    for i in range(p):
        Mu_a[ind,i] = (Mu_a[ll,i]/(nu_l+t-t_l) + Mu_a[rr,i]/(nu_r+t-t_r))*nu

    #Mu = ( Mu_l/(nu_l+t-t_l) + Mu_r/(nu_r+t-t_r) )* nu

    #Mu_a[ind,:] = Mu
    nu_a[ind] = nu

    cdef double nu_s = nu_l+nu_r+2*t-t_l-t_r

    cdef double mdist = norm_ind(Mu_a,ll,rr,p)/k
    cdef double logdetLam = p*log(k)


    logZ[ind] = -.5*( logdetLam + p*log( 2*M_PI*nu_s ) + mdist/nu_s) 



def compute_downward_messages(X, C,P,v,k,Mu_u, nu_u):
    (N,p) = np.shape(X)

    Mu_d = np.zeros((2*N-1,p))
    nu_d = np.zeros(2*N-1)

    logZ = np.zeros(2*N-1)

    Mull_in = np.zeros( (2,p))
    Murr_in = np.zeros( (2,p))
    sll = np.zeros(2)
    srr = np.zeros(2)

    cdef double[:] Mu_ll = np.zeros(p)
    cdef double[:] Mu_rr = np.zeros(p)
    cdef double[:] nutmp = np.zeros(2)

    compute_downward_messages_h(N,p,C,P,v,k,2*N-2,Mu_u,nu_u,Mu_d,nu_d,logZ, Mull_in, Murr_in, sll, srr, Mu_ll, Mu_rr, nutmp)

    return (logZ,Mu_d,nu_d)


cdef void compute_downward_messages_h(int N, int p, long[:,:] C, long[:] P, double[:] v, double k, int ind, double[:,:] Mu_u, double[:] nu_u, double[:,:] Mu_d, double[:] nu_d, double[:] logZ, double[:,:] Mull_in, double[:,:] Murr_in, double[:] sll, double[:] srr , double[:] Mu_ll, double[:] Mu_rr, double[:] nutmp):

    cdef int ll,rr,pp

    if ind < N:
        return
    #end

    ll = C[ind,0]
    rr = C[ind,1]
   
    cdef double lZll,lZrr,nu_ll,nu_rr
#    cdef double[:] Mu_ll = np.zeros(p)
#    cdef double[:] Mu_rr = np.zeros(p)

 
    if ind == 2*N-2:
#        (lZll, Mu_ll, nu_ll) = compute_message(Mu_u[[rr],:], nu_u[[rr]], k, v[[ind]]-v[rr])
#        (lZrr, Mu_rr, nu_rr) = compute_message(Mu_u[[ll],:], nu_u[[ll]], k, v[[ind]]-v[ll])
        compute_message1d(Mu_u[rr,:], nu_u[rr], k, v[ind]-v[rr], &lZll, Mu_ll, &nu_ll, p)

        compute_message1d(Mu_u[ll,:], nu_u[ll], k, v[ind]-v[ll], &lZrr, Mu_rr, &nu_rr, p)


    else:

        Mull_in[0,...] = Mu_u[rr,:]
        Mull_in[1,...] = Mu_d[ind,:]

        Murr_in[0,...] = Mu_u[ll,:]
        Murr_in[1,...] = Mu_d[ind,:]

        #Mull_in = np.c_[Mu_u[rr,:] , Mu_d[ind,:]].T
        #Murr_in = np.c_[Mu_u[ll,:] , Mu_d[ind,:]].T

        pp = P[ind]

        sll[0] = v[ind]-v[rr]
        sll[1] = v[pp]-v[ind]


        srr[0] = v[ind]-v[ll]
        srr[1] = v[pp]-v[ind]


#        sll = np.array( [ v[ind]-v[rr], v[pp]-v[ind] ] )
#        srr = np.array( [ v[ind]-v[ll], v[pp]-v[ind] ] )

        nutmp[0] = nu_u[rr]
        nutmp[1] = nu_d[ind]

        compute_message(Mull_in, nutmp, k, sll , &lZll, Mu_ll, &nu_ll, 2,p)

        nutmp[0] = nu_u[ll]
        nutmp[1] = nu_d[ind]

        compute_message(Murr_in, nutmp, k, srr , &lZrr, Mu_rr, &nu_rr, 2,p)

    #end

    #assert not np.any( np.isnan(Mu_ll))
    #assert not np.any( np.isnan(Mu_rr))

    Mu_d[ll,...] = Mu_ll
    Mu_d[rr,...] = Mu_rr

    nu_d[ll] = nu_ll
    nu_d[rr] = nu_rr

    logZ[ll] = lZll
    logZ[rr] = lZrr

    compute_downward_messages_h(N,p,C,P,v,k,ll,Mu_u,nu_u,Mu_d,nu_d,logZ,Mull_in,Murr_in,sll,srr,Mu_ll, Mu_rr, nutmp)
    compute_downward_messages_h(N,p,C,P,v,k,rr,Mu_u,nu_u,Mu_d,nu_d,logZ,Mull_in,Murr_in,sll,srr, Mu_ll, Mu_rr, nutmp)



#prior probability of adding the subtree at pruned_ind above ins_ind
def jump_priors(prune_ind, C,P,v,M, alpha, beta, predictive = False):

    if predictive:
        tnm1 = len(P)-1
    else:
        tnm1 = len(P)
    #end

    priorConsts = np.zeros( (tnm1,5),dtype=np.double)

    jump_priors_h(prune_ind, tnm1-1, C,P,v,M, alpha, beta,  priorConsts, 0, tnm1)

    return priorConsts

cdef void jump_priors_h(  int prune_ind, int ins_ind, long[:,:] C, long[:] P, double[:] v, long[:] M, double l_alpha, double l_beta, double[:,:] priorConsts, double logMdiff, int tnm1):

    cdef int N = (tnm1+1)/2
    cdef int ll,rr,pp

    cdef double lMdiff

    if ins_ind == tnm1-1:

        ll = ins_ind #C[ins_ind,0]
        rr = prune_ind #C[ins_ind,1]
        pp = -1 #P[ins_ind]

        #priorConsts[ins_ind,:] = [ll,rr,pp, logMdiff-beta_oprobs]
        priorConsts[ins_ind,0] = ll
        priorConsts[ins_ind,1] = rr
        priorConsts[ins_ind,2] = pp
        priorConsts[ins_ind,3] = -DBL_MAX #we should never actually try to add above the root 
        priorConsts[ins_ind,4] = 1




        ll = C[ins_ind,0]
        rr = C[ins_ind,1]
        lMdiff = logMdiff - log(M[ins_ind]+M[prune_ind])+log(M[ins_ind]) 
        jump_priors_h( prune_ind, ll, C,P,v,M, l_alpha, l_beta, priorConsts, lMdiff, tnm1)
        jump_priors_h( prune_ind, rr, C,P,v,M, l_alpha, l_beta, priorConsts, lMdiff, tnm1)

        return
    #end

    pp = P[ins_ind]

    cdef double Bold = 1-v[ins_ind]/v[pp]

    cdef double beta_oprobs = beta_logpdf(Bold,l_alpha,l_beta)

    #a single leaf attached to the root always has branch length = 1
    if Bold == 1:
        beta_oprobs = 0
    #end

    

    ll = ins_ind #C[ins_ind,0]
    rr = prune_ind #C[ins_ind,1]
    pp = P[ins_ind]


    #priorConsts[ins_ind,:] = [ll,rr,pp, logMdiff-beta_oprobs]
    priorConsts[ins_ind,0] = ll
    priorConsts[ins_ind,1] = rr
    priorConsts[ins_ind,2] = pp
    priorConsts[ins_ind,3] = logMdiff-beta_oprobs
    priorConsts[ins_ind,4] = 1

    if ins_ind >=N:
        ll = C[ins_ind,0]
        rr = C[ins_ind,1]

        lMdiff = logMdiff - log(M[ins_ind]+M[prune_ind])+log(M[ins_ind]) 

        jump_priors_h( prune_ind, ll, C,P,v,M, l_alpha, l_beta, priorConsts, lMdiff, tnm1)
        jump_priors_h( prune_ind, rr, C,P,v,M, l_alpha, l_beta, priorConsts, lMdiff, tnm1)
    #end






cpdef np.ndarray[DTYPE_t,ndim=2] jump_likelihoods(int pruned_ind, long[:,:] C, long[:] P, double[:] v, double k, np.ndarray[DTYPE_t,ndim=2] Mu_u, double[:] nu_u, double[:,:] Mu_d, double[:] nu_d, double[:,:] Mu_u_old, double[:] nu_u_old, double[:] logZ_up, double[:] logZ_dn, double[:] logZ_old, bint predictive):

    (tnm1,p) = np.shape(Mu_u)
    if predictive:
        tnm1 -= 1
    #end 

    cdef double[:,:] Mu_u_view = Mu_u

    cdef np.ndarray[DTYPE_t,ndim=2] llConsts = np.zeros( (tnm1, 12) ,dtype=np.double)
    cdef double[:,:] llConsts_v = llConsts #np.zeros( (tnm1-1, 4,3) ,dtype=np.double)
    cdef int ii

    cdef double[:,:] Mus_tmp = np.zeros( (3,p) )
    cdef double[:] nus_tmp = np.zeros( 3)

    cdef double[:] tmp_vec = np.zeros(3)
    cdef double[:] tmp_pvec = np.zeros(p)

    for ii in range(tnm1-1):
        jump_likelihood_fast(pruned_ind, ii, C,P,v,k, Mu_u_view, nu_u, Mu_d, nu_d, Mu_u_old, nu_u_old, logZ_up, logZ_dn, llConsts_v[ii], p, tnm1, Mus_tmp, nus_tmp,tmp_vec,tmp_pvec)

            #jump_likelihood(pruned_ind, ii, C,P,v,k, Mu_u, nu_u, Mu_d, nu_d, Mu_u_old, nu_u_old, logZ_up, logZ_dn, logZ_old, consts = llConsts[ii])   

    return llConsts


cdef jump_likelihood_fast(int pruned_ind, int ins_ind, long[:,:] C, long[:] P, double[:] v, double k, double[:,:] Mu_u, double[:] nu_u, double[:,:] Mu_d, double[:] nu_d, double[:,:] Mu_u_old, double[:] nu_u_old, double[:] logZ_up, double[:] logZ_dn, double[:] consts, int p, int tNm1, double[:,:] Mus, double[:] nus, double[:] tmpvec, double[:] tmp_pvec):


#    cdef np.ndarray[DTYPE_t, ndim=2] consts = np.zeros( (4,3))
#    cdef np.ndarray[DTYPE_t, ndim=1] dsts 

    cdef int ins_pp = P[ins_ind]

    #we shouldn't be attaching above the root
    if ins_pp < 0:
        return 
        
    cdef int ll = C[ins_pp,0]
    cdef int rr = C[ins_pp,1]
    cdef int pp = P[ins_pp]

    cdef double lZtmp1,lZtmp2

    cdef double lZ_o,lZpp,tll,trr,tpp,nu
    if pp == -1:
        lZ_o = logZ_up[ins_pp]

        ll = ins_ind
        rr = pruned_ind
        pp = ins_pp
       
        #vnew = (v[pp] + np.max(v[[ll,rr]]))/2 


#        Mus = np.c_[Mu_u[ins_ind,:] , Mu_u_old[pruned_ind], Mu_d[ins_ind,:]].T
#        nus = np.r_[nu_u[ins_ind], nu_u_old[pruned_ind], nu_d[ins_ind]].T


#        Mus = np.zeros( (3,p) )
#        nus = np.zeros( 3 )

        Mus[0,...] = Mu_u[ins_ind]
        Mus[1,...] = Mu_u_old[pruned_ind]
        Mus[2,...] = Mu_d[ins_ind] 

        nus[0] = nu_u[ins_ind]
        nus[1] = nu_u_old[pruned_ind]
        nus[2] = nu_d[ins_ind]

        compute_distances(Mus,k,p,tmpvec)


#        consts[0,0] = ins_ind
#        consts[0,1] = pruned_ind
#        consts[0,2] = ins_pp
#        consts[1] = tmpvec
#        consts[2] = nus
#        consts[3,0] = -lZ_o

        consts[0] = ins_ind
        consts[1] = pruned_ind
        consts[2] = ins_pp
        consts[3:6] = tmpvec
        consts[6:9] = nus
        consts[9] = -lZ_o


#            consts.append( [ins_ind, pruned_ind, ins_pp] )
#            consts.append(constants)
#            #consts.append( Mus )
#            consts.append( nus )
        #end
        return consts

    else:
        tll = v[ins_pp]-v[ll]
        trr = v[ins_pp]-v[rr]
        tpp = v[pp] - v[ins_pp]

        
        
#        Mus = np.c_[Mu_u[[ll,rr],:].T, Mu_d[pp,:]].T
#        nus = np.r_[nu_u[[ll,rr]], nu_d[pp]]

#        Mus = np.zeros( (3,p) )
#        nus = np.zeros( 3 )
       
        Mus[0,...] = Mu_u[ll]
        Mus[1,...] = Mu_u[rr]
        Mus[2,...] = Mu_d[ins_pp] 

        nus[0] = nu_u[ll]
        nus[1] = nu_u[rr]
        nus[2] = nu_d[ins_pp]

        tmpvec[0] = tll
        tmpvec[1] = trr
        tmpvec[2] = tpp
        #ss = np.array([tll,trr,tpp])

#        compute_message(Mus[0:2:], nus[0:2:],k,tmpvec[0:2:],&lZtmp1, tmp_pvec, &nu, 2, p)
#        compute_message(Mus[1:], nus[1:],k,tmpvec[1:],&lZtmp2, tmp_pvec, &nu, 2, p)


        compute_message( Mus, nus, k, tmpvec, &lZ_o, tmp_pvec, &nu, 3,p ) 


        lZpp = logZ_dn[ins_ind]


#        print (lZtmp1,lZtmp2,lZpp,lZ_o, )
#        print (ll,rr,ins_ind)

        ll = ins_ind
        rr = pruned_ind
        pp = ins_pp

        #vnew = (v[pp] + np.max(v[[ll,rr]]))/2 
        #Mus = np.c_[Mu_u[ins_ind,:] , Mu_u_old[pruned_ind], Mu_d[ins_ind,:]].T
        #nus = np.r_[nu_u[ins_ind], nu_u_old[pruned_ind], nu_d[ins_ind]].T

        Mus[0,...] = Mu_u[ins_ind]
        Mus[1,...] = Mu_u_old[pruned_ind]
        Mus[2,...] = Mu_d[ins_ind] 

        nus[0] = nu_u[ins_ind]
        nus[1] = nu_u_old[pruned_ind]
        nus[2] = nu_d[ins_ind]
        
        compute_distances(Mus,k,p,tmpvec)

#        consts[0,0] = ins_ind
#        consts[0,1] = pruned_ind
#        consts[0,2] = ins_pp
#        consts[1,...] = tmpvec
#        consts[2,...] = nus
#        consts[3,0] = lZpp-lZ_o

        consts[0] = ins_ind
        consts[1] = pruned_ind
        consts[2] = ins_pp
        consts[3:6] = tmpvec
        consts[6:9] = nus
        consts[9] = lZpp-lZ_o



#            consts.append( [ins_ind, pruned_ind, ins_pp] )
#            consts.append(constants)
#            #consts.append( Mus )
#            consts.append( nus )
        #end


    #end
def compute_distances_p(np.ndarray[DTYPE_t,ndim=2] Mu_in, double k):
    (N,p) = np.shape(Mu_in)
    cdef np.ndarray[DTYPE_t] dists = np.zeros(3)
    compute_distances(Mu_in,k,p,dists)
    return dists

#computes squared distances
cdef void compute_distances(double[:,:] Mu_in, double k, int p, double[:] dists ):

#    cdef np.ndarray[DTYPE_t, ndim=1] df12 = Mu_in[1]-Mu_in[0]
#    cdef np.ndarray[DTYPE_t, ndim=1] df23 = Mu_in[2]-Mu_in[1]
#    cdef np.ndarray[DTYPE_t, ndim=1] df31 = Mu_in[0]-Mu_in[2]

    cdef int i
    cdef double mdist12=0
    cdef double mdist23=0
    cdef double mdist31=0

    for i in range(p):
        mdist12 += (Mu_in[1,i] - Mu_in[0,i])*(Mu_in[1,i] - Mu_in[0,i])
        mdist23 += (Mu_in[2,i] - Mu_in[1,i])*(Mu_in[2,i] - Mu_in[1,i]) 
        mdist31 += (Mu_in[0,i] - Mu_in[2,i])*(Mu_in[0,i] - Mu_in[2,i])

    mdist12 /= k
    mdist23 /= k
    mdist31 /= k

#    cdef double mdist12 = dot(df12,df12,p)/k
#    cdef double mdist23 = dot(df23,df23,p)/k
#    cdef double mdist31 = dot(df31,df31,p)/k
#
#    cdef np.ndarray[DTYPE_t, ndim=1] dists = np.zeros(3)
    dists[0] = mdist12
    dists[1] = mdist23
    dists[2] = mdist31

cdef void compute_message1d(double[:] Mu_in, double nu_in, double k, double s_in, double* lZ, double[:] Mu, double* nu_out, int p):

    nu_out[0] = nu_in+s_in
    
    Mu[...] = Mu_in
    lZ[0] = 0    

def compute_message_p(np.ndarray[DTYPE_t,ndim=2] Mu_in, np.ndarray[DTYPE_t] nus, double k, np.ndarray[DTYPE_t] ss):
    (nn,p) = np.shape(Mu_in)    
    cdef double lZ,nu_out
    cdef np.ndarray[DTYPE_t] Muout = np.zeros(p)

    compute_message(Mu_in,nus,k,ss,&lZ,Muout,&nu_out,nn,p)

    return (lZ,Muout,nu_out)

cdef void compute_message(double[:,:] Mu_in, double[:] nu_in, double k, double[:] s_in, double* lZ, double[:] Mu, double* nu_out, int nn, int p):

#    cdef int nn,p
#    (nn,p) = np.shape(Mu_in)


    cdef double logdetLam = p*log(k)

    cdef double nu,mdist,mdist12,mdist23,mdist31
    cdef double nu_s
    #cdef double[:] Mu = np.zeros(p)
    cdef double nu_t1, nu_t2, nu_t3


#    nu = np.sum( (nu_in+s_in)**-1)**-1
#    Mu = np.sum( Mu_in/(nu_in+s_in).reshape(nn,1),0)*nu


#    assert not np.isnan(nu)
#    assert not np.isinf(nu)
    cdef int nind,i
   
    nu = 0 
    for nind in range(nn):
        nu += 1.0/(nu_in[nind]+s_in[nind])

    nu = 1.0/nu

    for i in range(p):
        Mu[i] = 0
        for nind in range(nn):
            Mu[i] += Mu_in[nind,i]/(nu_in[nind]+s_in[nind])
        Mu[i] *= nu

    nu_out[0] = nu
    

    if nn == 1:
#        nu = nu_in[0]+s_in[0]
#        Mu = Mu_in[0]
#        return (0,Mu,nu) 
        lZ[0] = 0    

    elif nn == 2:

#        df = Mu_in[1]-Mu_in[0]
#
#        mdist = dot(df,df,p)/k

        mdist = norm_ind(Mu_in,1,0,p)/k



#        nu = 1/npsum( 1/(nu_in+s_in),2)
#        Mu = nu*(Mu_in[0]/(nu_in[0]+s_in[0]) + Mu_in[1]/(nu_in[1]+s_in[1])) #npsum2( Mu_in/(nu_in+s_in).reshape(nn,1),2,p)*nu
        nu_s = nu_in[0]+nu_in[1]+s_in[0]+s_in[1]

        lZ[0]= -.5*( logdetLam + p*log( 2*M_PI*nu_s ) + mdist/nu_s) 
        #return (lZ,Mu,nu) 

    elif nn == 3:

#        df12 = Mu_in[1]-Mu_in[0]
#        df23 = Mu_in[2]-Mu_in[1]
#        df31 = Mu_in[0]-Mu_in[2]
#
#        mdist12 = dot(df12,df12,p)/k
#        mdist23 = dot(df23,df23,p)/k
#        mdist31 = dot(df31,df31,p)/k

        mdist12 = norm_ind(Mu_in,1,0,p)/k
        mdist23 = norm_ind(Mu_in,2,1,p)/k
        mdist31 = norm_ind(Mu_in,0,2,p)/k



#        nu = 1/npsum( 1/(nu_in+s_in),3)
#        Mu = nu*(Mu_in[0]/(nu_in[0]+s_in[0]) + Mu_in[1]/(nu_in[1]+s_in[1])+Mu_in[2]/(nu_in[2]+s_in[2])) 
        #Mu = npsum2( Mu_in/(nu_in+s_in).reshape(nn,1),3,p)*nu

        nu_t1 = nu_in[0]+s_in[0]
        nu_t2 = nu_in[1]+s_in[1]
        nu_t3 = nu_in[2]+s_in[2]

        #nu_t = nu_in+s_in
        #nu_s = nu_t[0]*nu_t[1]+nu_t[1]*nu_t[2]+nu_t[2]*nu_t[0]
        nu_s = nu_t1*nu_t2 + nu_t2*nu_t3 + nu_t3*nu_t1
    
        if nu_s != 0:
            lZ[0] = -( p*log(2*M_PI)+logdetLam + .5*p*log(nu_s) + .5*(mdist12*nu_t3/nu_s+mdist23*nu_t1/nu_s+mdist31*nu_t2/nu_s))
        else:
            lZ[0] = -DBL_MAX
        #return (lZ,Mu,nu) 


    #end

cdef void compute_extrema_c(int p, double[:] v, double tmin, double tmax, double alpha, double beta, int seg, double[:,:] pconstants, double[:,:] constants, int sgn, double ttol , double tinit , int direction, double[:] maxima, double[:] minima, int* mxi, int* mni, double[:] fpout):

#    cdef np.ndarray[DTYPE_t] maxima = -np.ones(7)
#    cdef np.ndarray[DTYPE_t] minima = -np.ones(7)
#
#    cdef int mxi=0
#    cdef int mni=0

#    cdef np.ndarray[DTYPE_t,ndim=1] fpout = np.zeros(2)

#    cdef double[:] mxdat = maxima
#    cdef double[:] mndat = minima

    cdef double[:] fpdat = fpout

    compute_extrema(&eval_pdf,&eval_grad,p, v, tmin, tmax, alpha, beta, seg, pconstants, constants, sgn, ttol , tinit , direction, tmin,tmax,maxima, minima, mxi, mni, fpdat)


#    return (maxima[:mxi],minima[:mni])


cpdef compute_extrema_p(p, np.ndarray[DTYPE_t] v, tmin, tmax, alpha, beta, seg, np.ndarray[DTYPE_t,ndim=2] pconstants, np.ndarray[DTYPE_t,ndim=2] constants, sgn, ttol , tinit , direction):

    maxima = -np.ones(7)
    minima = -np.ones(7)

    cdef int mxi=0
    cdef int mni=0

    cdef np.ndarray[DTYPE_t,ndim=1] fpout = np.zeros(2)

    cdef double[:] vdat = v
    cdef double[:,:] pcdat = pconstants
    cdef double[:,:] cdat = constants

    cdef double[:] mxdat = maxima
    cdef double[:] mndat = minima

    cdef double[:] fpdat = fpout

    compute_extrema(&eval_pdf,&eval_grad,p, vdat, tmin, tmax, alpha, beta, seg, pcdat, cdat, sgn, ttol , tinit , direction, tmin,tmax, mxdat, mndat, &mxi, &mni, fpdat)

    #print (maxima,minima)

    mxind = np.nonzero(maxima >= 0)[0]
    maxima = maxima[mxind]

    mnind = np.nonzero(minima >= 0)[0]
    minima = minima[mnind]


    return (maxima,minima)

#@cython.profile(True)
cdef void compute_extrema( double (*f)(double,int,double[:,:], double[:,:],double,double,double[:], double, int ), void (*fprime)(double,int,double[:,:],double[:,:],double,double,double[:], int, double[:] out), int p, double[:] v, double tmin, double tmax, double alpha, double beta, int seg, double[:,:] pconstants,  double[:,:] constants, int sgn , double ttol , double  tinit , int direction, double atmin, double atmax, double[:] maxima, double[:] minima, int* mxind, int* mnind, double[:] fpout):

    #print sgn
    
#    cdef int tmn1 = len(v)
#    cdef int N = (tmn1+1)/2

 #   assert tmax > tmin
    if ttol == 0:
        ttol = (tmax-tmin)*10.0**-4

#    cdef int ll = <int>constants[seg,0,0]
#    cdef int rr = <int>constants[seg,0,1]
#    cdef int pp = <int>constants[seg,0,2]

#    cdef double tll = v[ll]
#    cdef double trr = v[rr]
#    cdef double tpp = v[pp]


    if tinit == 0:
        tinit = (tmax+tmin)/2

#    if f == &eval_predictive_pdf:
#        print 1
   
    cdef double topt 
    if tmax-tmin < 10.0**-15:
        topt = (tmax+tmin)/2
    else:
        topt = newton_raphson( f, fprime,  tmin,tmax,tinit,seg,pconstants,constants,alpha,beta,v,p,ttol,10.0**-6,sgn,fpout) #tol=10.0**-8,step=.5,sgn=sgn) 
        #topt = newton_raphson_logx( f, fprime,  tmin,tmax,tinit,seg,pconstants,constants,alpha,beta,v,p,ttol,10.0**-4,sgn,fpout,atmin,atmax) #tol=10.0**-8,step=.5,sgn=sgn) 
    #end


    if sgn > 0:
        maxima[mxind[0]] = topt
        mxind[0] += 1
    else:
        minima[mnind[0]] = topt
        mnind[0] += 1
    #end

#    print (tmin,tmax,tinit,topt,sgn)
#    pdfopt = util.eval_pdf_p(topt,seg,pconstants,constants,alpha,beta,v,0,p)
#    pdfinit = util.eval_pdf_p(tinit,seg,pconstants,constants,alpha,beta,v,0,p)
#    pdfmin = util.eval_pdf_p(tmin,seg,pconstants,constants,alpha,beta,v,0,p)
#    pdfmax = util.eval_pdf_p(tmax,seg,pconstants,constants,alpha,beta,v,0,p)
#
#    print(pdfmin,pdfmax,pdfinit,pdfopt)

    cdef double pdf0,tinit_l,tinit_r
    cdef double ltol

    if topt == tmin:
        return
    elif topt == tmax:
        return  
    else:

        pdf0 = f(topt,seg,pconstants,constants,alpha,beta,v,0,p)

        if direction <= 0:

#            opt11 = [tmin]
#            opt12 = []
            if (topt-tmin)/(tmax-tmin) > 10.0**-4:
                tinit_l = topt-(topt-tmin)*10.0**-2
                ltol = (tinit_l-tmin)*.5
                

                if tinit_l > tmin: #-sgn*(f(tinit_l,seg,pconstants,constants,alpha,beta,v,0,p) -pdf0) < 0 and tinit_l > tmin:
                    pyramid( f , topt, tmin, tmax, sgn, &tinit_l, &tinit_r, seg, pconstants, constants, alpha, beta, v,  p)

                    #tinit_l = linesearch(f, topt, -ltol, tmin, -sgn, seg,pconstants,constants,alpha,beta,v,p)
                #end

                #print "tinit_l: %0.16f" % tinit_l
#                while -sgn*(util.eval_pdf_p(tinit_l,seg,pconstants,constants,alpha,beta,v,0,p) -pdf0) > 0 and tinit_l > tmin:
#                    tinit_l -= ttol
#                #end
                if tinit_l > tmin: 
                    compute_extrema(f,fprime,p, v, tmin, topt, alpha, beta, seg, pconstants, constants, -sgn, ttol, tinit_l, -1, atmin, atmax, maxima,minima,mxind,mnind,fpout)
                #end

#                if tinit_l > tmax or tinit_l < tmin:
#                    print "tinit_l out"
#                    print (topt,tinit_l,tmin,tmax)
            #end
            elif -sgn > 0:
                maxima[mxind[0]] = tmin
                mxind[0] += 1
            elif -sgn < 0:
                minima[mnind[0]] = tmin
                mnind[0] += 1
                
#        else:
#            opt11 = []
#            opt12 = []


        if direction >= 0:
#            opt21 = [tmax]
#            opt22 = []
            if (tmax-topt)/(tmax-tmin) > 10.0**-4:
                tinit_r = topt+(tmax-topt)*10.0**-2
                ltol = (tmax-tinit_r)*.5

                if tinit_r < tmax: #-sgn*(f(tinit_r,seg,pconstants,constants,alpha,beta,v,0,p) - pdf0) < 0 and tinit_r < tmax:
                    pyramid( f , topt, tmin, tmax, sgn, &tinit_l, &tinit_r, seg, pconstants, constants, alpha, beta, v,  p)
                    #tinit_r = linesearch(f, topt, ltol, tmax, -sgn, seg,pconstants,constants,alpha,beta,v,p)
                #end
                #print "tinit_r: %0.16f" % tinit_r
#                pdfinit = eval_pdf(tinit_r,seg,pconstants,constants,alpha,beta,v,0,p)
#
#                if sgn > 0:
#                    assert( pdfinit < pdf0)

#                while -sgn*(util.eval_pdf_p(tinit_r,seg,pconstants,constants,alpha,beta,v,0,p) - pdf0) > 0 and tinit_r < tmax:
#                    tinit_r += ttol
#                #end

                if tinit_r < tmax:
                    compute_extrema(f, fprime, p, v, topt, tmax, alpha, beta, seg, pconstants, constants, -sgn, ttol, tinit_r, 1, atmin, atmax, maxima, minima, mxind, mnind, fpout)
                #end

#                if tinit_r < tmin or tinit_r > tmax:
#                    print "tinit_r out"
#                    print (topt,tinit_r,tmin,tmax)

            elif -sgn > 0:
                maxima[mxind[0]] = tmax
                mxind[0] += 1
            elif -sgn < 0:
                minima[mnind[0]] = tmax
                mnind[0] += 1
            #end
#        else:
#            opt21 = []
#            opt22 = []

#        opt12.extend(opt22)
#        opt12.append(topt)
#        opt11.extend(opt21)

#        return (opt12,opt11)




cpdef double newton_raphson_gaussian( double xmin, double xmax, double x0, int seg, np.ndarray[DTYPE_t, ndim=2] priorConsts, np.ndarray[DTYPE_t, ndim=2] llConsts, double alpha, double beta, np.ndarray[DTYPE_t] v, int p , double step=0, double tol=10.0**-6 , int sgn=1): 

    cdef np.ndarray[DTYPE_t,ndim=1] fpout = np.zeros(2)

    return newton_raphson(&eval_pdf, &eval_grad, xmin,xmax,x0,seg,priorConsts,llConsts,alpha,beta,v,p,step, tol, sgn, fpout)


@cython.profile(False)
cdef double newton_raphson( double (*f)(double,int,double[:,:], double[:,:],double,double,double[:], double, int ), void (*fprime)(double,int,double[:,:],double[:,:],double,double,double[:], int, double[:] out ), double xmin, double xmax, double x0, int seg, double[:,:] priorConsts, double[:,:] llConsts, double alpha, double beta, double[:] v, int p ,  double step, double tol , int sgn, double[:] fpout):



    if step == 0:
        step = xmax-xmin
  
    cdef double x = x0
    cdef double xnew,xt
    cdef double xbound
   
    fprime(x,seg,priorConsts,llConsts,alpha,beta,v,p, fpout)
    cdef double fp = fpout[0]
    cdef double signed_h = fpout[1]
    cdef double h = fabs(fpout[1])
 
#    fp = fprime(x)
    cdef double fp0 = fp

#    h = np.abs(hess(x))
    cdef double h0 = h
    cdef double ft1,ft2,ft3
    cdef double fxs1,fxs2,fxs3

    fprime((xmax-xmin)*.9+xmin,seg,priorConsts,llConsts,alpha,beta,v,p,fpout)
    fxs1 = fpout[0]
    fprime((xmax-xmin)*.1+xmin,seg,priorConsts,llConsts,alpha,beta,v,p,fpout)
    fxs2 = fpout[0]
    fprime((xmax-xmin)*.5+xmin,seg,priorConsts,llConsts,alpha,beta,v,p,fpout)
    fxs3 = fpout[0]

    cdef double fpscale = (fabs(fxs1) + fabs(fxs2) + fabs(fxs3) + fp0)/4#relative scale of the function values

    cdef double fx = f(x,seg,priorConsts,llConsts,alpha,beta,v,0,p)
    cdef double fxlast = fx

    
    cdef int itr = 0
    cdef double d1,d2,direction,dx
    #print (xmin,x0,xmax)


    while (fabs(fp/h) > tol and sgn == -1) or (fabs(fp/fpscale) > tol*10.0**2 and sgn == 1) or signed_h*sgn > 0: #and sgn == 1) or (fabs(fp) > tol and sgn == -1):
    #while (fabs(fp*step) > fabs(fscale*tol*.1) ) or signed_h*sgn > 0: #and sgn == 1) or (fabs(fp) > tol and sgn == -1):
    #while (fabs(fp/h) > tol) or signed_h*sgn > 0: #and sgn == 1) or (fabs(fp) > tol and sgn == -1):
        itr+= 1

        if itr == 1000:
            print "Warning: Newton-Raphson taking over 1000 iterations"

            print (x,xmin,xmax,fxs1,fxs2,fabs(fp*step),  fp,step,tol,signed_h*sgn, sgn)
            print (fabs(fp/h), tol, signed_h*sgn, sgn)

            print (fp,fp0,(xmax-xmin),tol)

            print (xt,x)

            llConsts[seg,11] = 1
            fx = f(x,seg,priorConsts,llConsts,alpha,beta,v,0,p)

            fprime(x+(xmax-xmin)*10.0**-4,seg,priorConsts,llConsts,alpha,beta,v,p, fpout)
            fxlast = f(x+(xmax-xmin)*10.0**-4,seg,priorConsts,llConsts,alpha,beta,v,0,p)
            
            print "hess, approximated hess: (%f, %f)" % (fpout[1], (fpout[0]-fp)/((xmax-xmin)*10.0**-4) )
            print "grad, approximated grad: (%f, %f)" % (fp, (fxlast - fx)/((xmax-xmin)*10.0**-4))
            plot_func(xmin,xmax,(xmax-xmin)*10.0**-4,f, seg,priorConsts,llConsts,alpha,beta,v,p)
            time.sleep(100)


        if sgn*fp/h > 0:
            xbound = xmax
        else:
            xbound = xmin

        
#        print (x,xmin,xmax,fx,fp,signed_h,fp/h,tol,fabs(fp/h) > tol,sgn)
#        print (sgn*fp/h,xbound)

        dx = sgn*fp/h
        xnew = x + dx 

        if xnew < xmin:
            xnew = xmin
            dx = .5*(xnew-x)
        elif xnew > xmax:
            xnew = xmax
            dx = .5*(xnew-x)



        #if sgn*(fx - fxlast) < 0 or xnew == xmax or xnew == xmin:

#            if itr > 10000:
#                print "grad, h, x"
#                print (fp,h,x)

        if itr == 1:
            direction = (xbound-x)*10.0**-2
        else:
            direction = dx #sgn*fp/h

        xt = x
        x = linesearch(f,x,direction,xbound,sgn, seg,priorConsts,llConsts,alpha,beta,v,p)
        fx = f(xnew,seg,priorConsts,llConsts,alpha,beta,v,0,p) 

#            if xt == x and signed_h*sgn > 0:
#                direction = (xbound-x)*10.0**-2
#                x = linesearch(f,x,direction,xbound,sgn, seg,priorConsts,llConsts,alpha,beta,v,p)
#
#                ft1 = f(x,seg,priorConsts,llConsts,alpha,beta,v,0,p)
#                ft2 = f(x-.1*direction,seg,priorConsts,llConsts,alpha,beta,v,0,p)
#                ft3 = f(x+.1*direction,seg,priorConsts,llConsts,alpha,beta,v,0,p)
#
#                print (x0,x,xmin,xmax,ft1,ft2,ft3,sgn*fp/h,direction,signed_h,sgn) 
#                plot_func(xmin,xmax,(xmax-xmin)*10.0**-4,f, seg,priorConsts,llConsts,alpha,beta,v,p)
#
#                fprime(x,seg,priorConsts,llConsts,alpha,beta,v,p,fpout)
#                fp = fpout[0]
#                signed_h = fpout[1]
#                h = fabs(fpout[1])
#
#                print (fp,signed_h,h)
#
#                fprime(x-.1*direction,seg,priorConsts,llConsts,alpha,beta,v,p,fpout)
#                fp = fpout[0]
#                signed_h = fpout[1]
#                h = fabs(fpout[1])
#                print (fp,signed_h,h)
#
#                fprime(x+.1*direction,seg,priorConsts,llConsts,alpha,beta,v,p,fpout)
#                fp = fpout[0]
#                signed_h = fpout[1]
#                h = fabs(fpout[1])
#                print (fp,signed_h,h)
#
#                time.sleep(100)
#            if x > xmax or x < xmin:
#                print (xmin,x,xmax)

#            fx = f(x,seg,priorConsts,llConsts,alpha,beta,v,0,p)
#        else:
#            x = xnew

        fxlast = fx
        #x = x + sgn*step*fp/h

#        print x
#        assert (x > xmin and x < xmax)

        #print (x, fx, fp, hess(x))

        #print (x,fx,fp,h,fp/h, tol,sgn)
       
        

 
#        if fmod(itr,1000) == 999:
#            print itr
#            print (x,xmin,xmax,fx,fp,signed_h,fp/h,tol,fabs(fp/h) > tol,sgn)


#        if x < xmin:
#            x = xlast
#            if sgn == -1:
#                return xmin
#            else:
#                step *= .9
#                fx = f(x,seg,priorConsts,llConsts,alpha,beta,v,0,p)
#        elif x > xmax:
#            x = xlast
#            if sgn == -1:
#                return xmax
#            else:
#                step *= .9
#                fx = f(x,seg,priorConsts,llConsts,alpha,beta,v,0,p)


        d1 = x-xmin
        d2 = xmax-x

        if d1 < (xmax-xmin)*10.0**-4 and sgn == -1 and itr > 5 and -fp < 0:
            return xmin
        elif d2 < (xmax-xmin)*10.0**-4 and sgn == -1 and itr > 5 and -fp > 0:
            return xmax




#        if fx == np.inf or fx == -np.inf: # or np.isnan(fx):
#            d1 = x-xmin
#            d2 = xmax-x
#            if d1 < d2 and sgn == -1:
#                return xmin
#            elif sgn == -1:
#                return xmax
#            else:

#                step *= .9

#                ff = lambda x : -f(x)
#                ffprime = lambda x : -fprime(x)

#                a0 = opt.line_search(f,fprime,xlast,sgn*fp,gfk=-fp)
#                x = x + a0*fp

#                return xlast

        if (x == xmin or x == xmax) and sgn == -1 and itr > 5:
            return x


        fprime(x,seg,priorConsts,llConsts,alpha,beta,v,p,fpout)
        fp = fpout[0]
        signed_h = fpout[1]
        h = fabs(fpout[1])
        #fp = fprime(x)
        #h = np.abs(hess(x))

#        if fabs(fp) > 10.0**5 and h > h0*10.0 and sgn == -1: #if we are diverging, set a constant step size to allow moving out of the boundaries
#            h = h0#np.abs()
    #end


    return x 


@cython.profile(False)
cdef double newton_raphson_logx( double (*f)(double,int,double[:,:], double[:,:],double,double,double[:], double, int ), void (*fprime)(double,int,double[:,:],double[:,:],double,double,double[:], int, double[:] out ), double xmin, double xmax, double x0, int seg, double[:,:] priorConsts, double[:,:] llConsts, double alpha, double beta, double[:] v, int p ,  double step, double tol , int sgn, double[:] fpout, double vmin, double vmax):



    if step == 0:
        step = xmax-xmin
  
    cdef double x = x0
    cdef double xnew,xt
    cdef double xbound
   
    fprime(x,seg,priorConsts,llConsts,alpha,beta,v,p, fpout)
    cdef double fp = fpout[0]
    cdef double signed_h = fpout[1]
    cdef double h = fabs(fpout[1])
 
#    fp = fprime(x)
    cdef double fp0 = fp

#    h = np.abs(hess(x))
    cdef double h0 = h
    cdef double ft1,ft2,ft3
    cdef double fxs1,fxs2,fxs3


    cdef double fx = f(x,seg,priorConsts,llConsts,alpha,beta,v,0,p)
    cdef double fxlast = fx

    cdef int itr = 0
    cdef double d1,d2,direction
    #print (xmin,x0,xmax)

    cdef double e_s = (x-vmin)/(vmax-x)
    cdef double gs_hs = (1+e_s)/(e_s-1) 
    cdef double exps,snew,dx,dx2,gs

    while (fabs(fp/h) > tol*fabs(fx)) or signed_h*sgn > 0: #and sgn == 1) or (fabs(fp) > tol and sgn == -1):
    #while (fabs(fp*step) > fabs(fscale*tol*.1) ) or signed_h*sgn > 0: #and sgn == 1) or (fabs(fp) > tol and sgn == -1):
    #while (fabs(fp/h) > tol) or signed_h*sgn > 0: #and sgn == 1) or (fabs(fp) > tol and sgn == -1):
        itr+= 1


        if sgn*fp/h > 0:
            xbound = xmax
        else:
            xbound = xmin

        
#        print (x,xmin,xmax,fx,fp,signed_h,fp/h,tol,fabs(fp/h) > tol,sgn)
#        print (sgn*fp/h,xbound)


        e_s = (x-vmin)/(vmax-x)
       
        if fabs(e_s-1.0) < 10.0**-4:
            gs_hs = (1+e_s)
        else: 
            gs_hs = (1+e_s)/(e_s-1)

 
#        gs = (vmax-vmin)*e_s/((1+e_s)*(1+e_s))
#        hs = (vmax-vmin)*e_s*(e_s-1)/( (1+e_s)*(1+e_s)*(1+e_s))

        snew = log(e_s) + sgn* gs_hs*fp/h
        exps = exp(snew)

        xnew = (vmax*exps+vmin)/(1+exps)

        dx2 = sgn*fp/h
        dx = xnew-x

        if itr == 1000:
            print "Warning: Newton-Raphson taking over 1000 iterations"

            print (x,xmin,xmax,dx, fp,step,tol,gs_hs,signed_h*sgn, sgn)
            print (vmin,vmax,e_s,snew,exps,xnew)
            print (fabs(fp/h), tol, signed_h*sgn, itr)

            print (x-xmin, xmax-x, (vmax-vmin)*10.0**-4)

            llConsts[seg,11] = 1
            fprime(x+(xmax-xmin)*10.0**-4,seg,priorConsts,llConsts,alpha,beta,v,p, fpout)
            fxlast = f(x+(xmax-xmin)*10.0**-4,seg,priorConsts,llConsts,alpha,beta,v,0,p)
            
            print "hess, approximated hess: (%f, %f)" % (fpout[1], (fpout[0]-fp)/((xmax-xmin)*10.0**-4) )
            print "grad, approximated grad: (%f, %f)" % (fp, (fxlast - fx)/((xmax-xmin)*10.0**-4))
            plot_func(xmin,xmax,(xmax-xmin)*10.0**-4,f, seg,priorConsts,llConsts,alpha,beta,v,p)
            time.sleep(100)
#
        #xnew = x + sgn*fp/h

        if xnew < xmin:
            #print "hit bound (%f,%f,%f)" % (vmin,xmin,xnew)
            xnew = xmin
            dx = xnew-x
        elif xnew > xmax:
            #print "hit bound (%f,%f,%f)" % (vmax,xmax,xnew)
            xnew = xmax
            dx = xnew-x

        fx = f(xnew,seg,priorConsts,llConsts,alpha,beta,v,0,p) 

        if itr == 1 or (sgn < 0 and fabs(dx) < (vmax-vmin)*10.0**-4):
            direction = (xbound-x)*.1
        else:
            if x+dx2 > xmax or x+dx2 < xmin:
                direction = dx #sgn*fp/h
            else:
                direciton = dx2


        xt = x
        x = linesearch(f,x,direction,xbound,sgn, seg,priorConsts,llConsts,alpha,beta,v,p)


        fx = f(x,seg,priorConsts,llConsts,alpha,beta,v,0,p)

        fxlast = fx


        d1 = x-xmin
        d2 = xmax-x

        if d1 < (vmax-vmin)*10.0**-4 and sgn == -1 and itr > 5 and -fp < 0:
            return xmin
        if d2 < (vmax-vmin)*10.0**-4 and sgn == -1 and itr > 5 and -fp > 0:
            return xmax




#        if fx == np.inf or fx == -np.inf: # or np.isnan(fx):
#            d1 = x-xmin
#            d2 = xmax-x
#            if d1 < d2 and sgn == -1:
#                return xmin
#            elif sgn == -1:
#                return xmax
#            else:

#                step *= .9

#                ff = lambda x : -f(x)
#                ffprime = lambda x : -fprime(x)

#                a0 = opt.line_search(f,fprime,xlast,sgn*fp,gfk=-fp)
#                x = x + a0*fp

#                return xlast

        if (x == xmin or x == xmax) and sgn == -1 and itr > 5:
            return x


        fprime(x,seg,priorConsts,llConsts,alpha,beta,v,p,fpout)
        fp = fpout[0]
        signed_h = fpout[1]
        h = fabs(fpout[1])
        #fp = fprime(x)
        #h = np.abs(hess(x))

#        if fabs(fp) > 10.0**5 and h > h0*10.0 and sgn == -1: #if we are diverging, set a constant step size to allow moving out of the boundaries
#            h = h0#np.abs()
    #end


    return x 



cdef plot_func(xmin,xmax,step,double (*f) (double,int,double[:,:], double[:,:],double,double,double[:], double, int ) ,seg,priorConsts,llConsts,alpha,beta,v,p):

    eps = step*10.0**-2
    X = np.arange(xmin+eps,xmax-eps,step)   
    Y = np.zeros(len(X)) 
    cdef int i
    for i in range(len(X)):
        Y[i] = f(X[i],seg,priorConsts,llConsts,alpha,beta,v,0,p)
    #end

    plt.figure()

    print X
    print Y

    plt.plot(X,Y,'r-')
   
    #plt.ylim( (np.median(Y[500]),np.max(Y)))
 
    plt.draw()


#find a triplet of points near x0 such that xl < xc < xr and xc > xl, xc > xr if sgn == 1 and xc < xl and xc < xr if sgn == -1 
cdef void pyramid( double (*f)(double,int,double[:,:], double[:,:],double,double,double[:], double, int )  , double x0, double xmin, double xmax, int sgn, double* xlout, double *xrout, int seg, double[:,:] priorConsts, double[:,:] llConsts, double alpha, double beta, double[:] v, int p  ):
   
    cdef double xl,xc,xr
    cdef double l_w,r_w 
    cdef double eps = 10.0**-2

    cdef double fl,fc,fr
    xc = x0
    cdef int itr=0

    while 1: 

        itr += 1

        if itr == 10000:
            print "Warning: pyramid taking over 10000 iterations"
   
        l_w = xc-xmin
        r_w = xmax-xc
 
        xl = xc-l_w*eps
        xr = xc+r_w*eps
     
        fl = f(xl,seg,priorConsts,llConsts,alpha,beta,v,0,p)
        fc = f(xc,seg,priorConsts,llConsts,alpha,beta,v,0,p)
        fr = f(xr,seg,priorConsts,llConsts,alpha,beta,v,0,p)
        

        if sgn*(fc-fl) > 0 and sgn*(fc-fr) > 0:
            xlout[0] = xl
            xrout[0] = xr
            return
        elif sgn*(fc-fl) < 0:
            xc = xl
        elif sgn*(fc-fr) < 0:
            xc = xr
        else:
            xlout[0] = xl
            xrout[0] = xr
            return
        #end

    #end 

 


def linesearch_gaussian(xinit, direction, xbound, sgn, seg ,priorConsts, llConsts, alpha, beta, v, p):
    return linesearch(&eval_pdf, xinit, direction, xbound, sgn, seg ,priorConsts, llConsts, alpha, beta, v, p)


@cython.profile(False)
cdef double linesearch(double (*f)(double,int,double[:,:],double[:,:],double,double,double[:], double, int ), double xinit, double direction, double xbound, int sgn, int seg, double[:,:] priorConsts, double[:,:] llConsts, double alpha, double beta, double[:] v, int p ):

#    print "begin linesearch"
    cdef double xnew = xinit+direction
    cdef double xin,xout

    if direction > 0:
        xnew = d_min(xnew,xbound)
    else:
        xnew = d_max(xnew,xbound)


    xin = xnew
    xout = xnew

    cdef double f0 = f(xinit,seg,priorConsts,llConsts,alpha,beta,v,0,p)
    cdef double fnew = f(xnew,seg,priorConsts,llConsts,alpha,beta,v,0,p)

    cdef double xopt = xinit
    cdef double fopt = f0

    cdef int itr = 0

    cdef double scale_out = .2
    cdef double scale_in = .2

    cdef double fnew_in,fnew_out
    cdef double ftmp

    while sgn * (fopt - f0) <= 0:# or itr < 1:
        itr += 1
        if fmod(itr,10000) == 9999:
            print "Warning: Linesearch taking over %d iterations" % itr
            print (xinit,xbound,direction)

            #return xbound
            return xinit

            print (xinit,xnew,xin,xout, (xbound-xout)*direction   ,xbound,direction,sgn)

            ftmp = f(xinit-direction,seg,priorConsts,llConsts,alpha,beta,v,0,p)
            print (f0,fnew,fnew_in,fnew_out,ftmp)


#            print "Reversing direction of search, check that your input direction is correct"
            #direction = -direction
            xout = xnew 
            xin = xnew
            scale_out *= .8
            scale_in *= .8

        xout = xinit+(xout-xinit)*(1+scale_out)
        xin = xinit+(xin-xinit)*(1-scale_in)

        fnew_out = f(xout,seg,priorConsts,llConsts,alpha,beta,v,0,p)
        if sgn*(fnew_out - fopt) > 0 and (xbound-xout)*direction > 0:
            fopt = fnew_out
            xopt = xout
#            print "end linesearch"
#            return xout
        elif (xbound-xout)*direction <= 0: #if sgn*(fnew_out - fopt) > 0:
            xout = xnew
            scale_out *= .9
       
        fnew_in = f(xin,seg,priorConsts,llConsts,alpha,beta,v,0,p)
        if sgn*(fnew_in - fopt) > 0:
            fopt = fnew_in
            xopt = xin
#            print "end linesearch"
#            return xin


#        if fmod(itr,10000) == 9999:
#            ftmp = f(xinit-direction*.1,seg,priorConsts,llConsts,alpha,beta,v,0,p)            
#            print "Linesearch:"
#            print ftmp
#            print (xinit,f0)
#            print (xnew,fnew)
#            print (xout,fnew_out)
#            print (xin,fnew_in)
#        if sgn*fnew_out > sgn*fnew_in:
#            xin = xout 
#        else:
#            xout = xin
#        #end
        

        #if we haven't found a good spot yet, reduce the scale and try again
#        if itr > 1/(scale*scale):
#            scale *= .5
#            xout = xnew
#            xin = xnew
#            itr = 0
            

#        xnew = xinit+(xnew-xinit)*.8
#        fnew = f(xnew,seg,priorConsts,llConsts,alpha,beta,v,0,p)
#
#        itr+= 1
#
#        if sgn*(fnew - fopt) > 0:
#            fopt = fnew
#            xopt = xnew
#    #        return xnew


    #end

    #linesearch out
#    if direction > 0:
#        xnew = d_min(xinit+direction,xbound)
#    else:
#        xnew = d_max(xinit+direction,xbound)
#
#    while itr < 20 and (xbound-xnew)*direction > 0:
#        fnew = f(xnew,seg,priorConsts,llConsts,alpha,beta,v,0,p)
#
#        if sgn * (fnew - fopt) > 0:
#            fopt = fnew
#            xopt = xnew
#     #       return xnew         
#
#
#        xnew = xinit+(xnew-xinit)*1.2
#
#        itr+= 1


    #end
   
#    if  xopt == xinit:
#        print "WTFFFF sgn: %d " % sgn
    #end

#    print "end linesearch"

    return xopt
    



def eval_pdf_p(vnew,seg,priorConsts,llConsts,alpha,beta,v,logdetLam,p):
    return eval_pdf(vnew,seg,priorConsts,llConsts,alpha,beta,v,logdetLam,p)

#def eval_grad_p(vnew,seg,priorConsts,llConsts,alpha,beta,v,p):
#    return eval_grad(vnew,seg,priorConsts,llConsts,alpha,beta,v,p)


#WARNING: dereferencing pointers passed through a va_list must be done with 2 separate 
#lines as below!  Cython doesn't recognize that the macro va_arg changes the state of 
#vlist and copies the entire argument to (.)[0] on 2 lines of C code, thus breaking
#causing too many calls to va_arg and breaking everything
ctypedef double[:]* dsp
ctypedef double[:,:]* dsdsp
@cython.profile(False)
@cython.boundscheck(False)
@cython.wraparound(False)
cdef double va_eval_pdf(double vnew, int seg, vl_wrapper* vlist_w):

    cdef va_list vlist
    va_copy(vlist,vlist_w.v)



    cdef dsdsp pC_p = <dsdsp>va_arg(vlist,pointer_type)
    cdef double[:,:] pC = pC_p[0]

    #cdef double[:,:] pC = <double[:N,:5]>va_arg(vlist,pointer_type)
    cdef dsdsp lC_p = <dsdsp>va_arg(vlist,pointer_type)
    cdef double[:,:] lC = lC_p[0]
    #cdef double[:,:] lC = <double[:N,:12]>va_arg(vlist,pointer_type)
    cdef double alpha = <double>va_arg(vlist,double_type)
    cdef double beta = <double>va_arg(vlist,double_type)

    cdef dsp v_p = <dsp>va_arg(vlist,pointer_type)
    cdef double[:] v = v_p[0]
    #cdef double[:] v = <double[:N]>va_arg(vlist,pointer_type)
    cdef double logdetLam = <double>va_arg(vlist,double_type)
    cdef int p = <int>va_arg(vlist,int_type)
    va_end(vlist)

    return eval_pdf(vnew,seg,pC,lC,alpha,beta,v,logdetLam,p)


@cython.profile(False)
@cython.boundscheck(False)
@cython.wraparound(False)
cdef double eval_pdf( double vnew, int seg, double[:,:] priorConsts, double[:,:] llConsts, double alpha, double beta, double[:] v, double logdetLam, int p):

    cdef int ll = <int>priorConsts[seg,0]
    cdef int rr = <int>priorConsts[seg,1]   
    cdef int pp = <int>priorConsts[seg,2]
    cdef double pC = priorConsts[seg,3]

    cdef double vpp = v[pp]
    cdef double vll = v[ll]
    cdef double vrr = v[rr] 


#    if vpp == 0 or vnew ==0:
#        print "Warning: division by zero in eval_pdf"
#        print (vnew,vpp)
#        print (ll,rr,pp)
 
    cdef double B1 = 1-vnew/vpp
    #Blist.append(B1)

    cdef double tmp = beta_logpdf(B1,alpha,beta)

    if tmp == -DBL_MAX:
        return -DBL_MAX

    cdef double beta_probs = beta_logpdf(B1,alpha,beta)

    #leaves have no associated beta rv
    if vll > 0:
        B1 = 1-vll/vnew
        tmp = beta_logpdf(B1,alpha,beta)
        if tmp == -DBL_MAX:
            return -DBL_MAX
        beta_probs += beta_logpdf(B1,alpha,beta)
    #end
    if vrr > 0:
        B1 = 1-vrr/vnew
        tmp = beta_logpdf(B1,alpha,beta)
        if tmp == -DBL_MAX:
            return -DBL_MAX
        beta_probs += beta_logpdf(B1,alpha,beta)
    #end

    cdef double prior = beta_probs + pC


    cdef double d12 = llConsts[seg,3]     
    cdef double d23 = llConsts[seg,4]     
    cdef double d31 = llConsts[seg,5]     
   
    cdef double lC = llConsts[seg,9]
#    cdef double tll = vnew-v[ll]
#    cdef double trr = vnew-v[rr]
#    cdef double tpp = v[pp]-vnew
 
    cdef double nu1 = llConsts[seg,6]+(vnew-vll)
    cdef double nu2 = llConsts[seg,7]+(vnew-vrr)
    cdef double nu3 = llConsts[seg,8]+(vpp-vnew)

    cdef double nu_s = nu1*nu2+nu2*nu3+nu3*nu1 

    cdef double lZ = 0

    if nu_s != 0:
        lZ = -( p*log(2*M_PI)+logdetLam + .5*p*log(nu_s) + .5*(d12*nu3+d23*nu1+d31*nu2)/nu_s)
    else:
        return -DBL_MAX
        #lZ = -DBL_MAX

    cdef double LL = lZ+lC

    return prior+LL 


cdef double eval_pdf2( double vnew, int seg, double[:,:] priorConsts, double[:,:] llConsts, double alpha, double beta, double[:] v, double logdetLam, int p):

    cdef int ll = <int>priorConsts[seg,0]
    cdef int rr = <int>priorConsts[seg,1]   
    cdef int pp = <int>priorConsts[seg,2]
    cdef double pC = priorConsts[seg,3]

    cdef double vpp = v[pp]
    cdef double vll = v[ll]
    cdef double vrr = v[rr] 
 
    cdef double B1 = 1-vnew/vpp
    #Blist.append(B1)

    cdef double tmp = beta_logpdf(B1,alpha,beta)

    if tmp == -DBL_MAX:
        return -DBL_MAX

    cdef double beta_probs = beta_logpdf(B1,alpha,beta)

    #leaves have no associated beta rv
    if vll > 0:
        B1 = 1-vll/vnew
        tmp = beta_logpdf(B1,alpha,beta)
        if tmp == -DBL_MAX:
            return -DBL_MAX
        beta_probs += beta_logpdf(B1,alpha,beta)
    #end
    if vrr > 0:
        B1 = 1-vrr/vnew
        tmp = beta_logpdf(B1,alpha,beta)
        if tmp == -DBL_MAX:
            return -DBL_MAX
        beta_probs += beta_logpdf(B1,alpha,beta)
    #end

    cdef double prior = beta_probs + pC


    cdef double d12 = llConsts[seg,3]     
    cdef double d23 = llConsts[seg,4]     
    cdef double d31 = llConsts[seg,5]     
   
    cdef double lC = llConsts[seg,9]
#    cdef double tll = vnew-v[ll]
#    cdef double trr = vnew-v[rr]
#    cdef double tpp = v[pp]-vnew
 
    cdef double nu1 = llConsts[seg,6]+(vnew-vll)
    cdef double nu2 = llConsts[seg,7]+(vnew-vrr)
    cdef double nu3 = llConsts[seg,8]+(vpp-vnew)

    cdef double nu_s = nu1*nu2+nu2*nu3+nu3*nu1 

    cdef double lZ = 0

    if nu_s != 0:
        lZ = -( p*log(2*M_PI)+logdetLam + .5*p*log(nu_s) + .5*(d12*nu3+d23*nu1+d31*nu2)/nu_s)
    else:
        return -DBL_MAX
        #lZ = -DBL_MAX

    cdef double LL = lZ+lC

    print "pdf: (%f,%f,%f)" % (prior,lZ,lC)

    return prior+LL 



@cython.profile(False)
@cython.boundscheck(False)
@cython.wraparound(False)
cdef void eval_grad( double t, int seg, double[:,:] priorConsts, double[:,:] llConsts, double alpha, double beta, double[:] v,  int p, double[:] out):


    cdef int ll = <int>priorConsts[seg,0]
    cdef int rr = <int>priorConsts[seg,1]   
    cdef int pp = <int>priorConsts[seg,2]
 

    cdef double d_lr = llConsts[seg,3]     
    cdef double d_rp = llConsts[seg,4]     
    cdef double d_pl = llConsts[seg,5]     
   
    cdef double tll = v[ll]
    cdef double trr = v[rr]
    cdef double tpp = v[pp]
 
    cdef double nu_ll = llConsts[seg,6]
    cdef double nu_rr = llConsts[seg,7]
    cdef double nu_pp = llConsts[seg,8]

    cdef double dt = d_rp+d_pl-d_lr
    cdef double nu_pt = nu_pp+tpp
    cdef double nu_lt = nu_ll-tll
    cdef double nu_rt = nu_rr-trr

    cdef double nut = nu_lt*nu_rt + nu_lt*nu_pt + nu_pt*nu_rt
    cdef double nu_d = nu_pt*d_lr + nu_lt*d_rp + nu_rt*d_pl
    cdef double nu_s = nut+2*nu_pt*t-t*t 
    #cdef double nu_s2 = nu_s*nu_s
    #cdef double nu_s3 = nu_s2*nu_s

    if nu_s == 0 or tpp-t==0 or t==0 or t==tll or t==trr:
        print "Warning: division by zero in eval_grad"

    cdef double nu_s_i1 = 1/nu_s
    cdef double nu_s_i2 = nu_s_i1*nu_s_i1
    cdef double nu_s_i3 = nu_s_i2*nu_s_i1

    cdef double t2 = t*t
    cdef double tpmt = 1/(tpp-t)
    cdef double tpmt2 = tpmt*tpmt

    cdef double nupt_m_t = nu_pt-t
    cdef double nupt_m_t2 = nupt_m_t*nupt_m_t  

    cdef double tnz
    cdef double tmp1
    cdef double tmp2


    cdef double grad =  -p*nupt_m_t*nu_s_i1-.5*(dt*nu_s-2*nupt_m_t*(nu_d+dt*t))*nu_s_i2
    cdef double hess = (p*(nu_s+2*nupt_m_t2) + 2*dt*nupt_m_t)*nu_s_i2 - (4*nupt_m_t2+nu_s)*(nu_d + dt*t)*nu_s_i3 

#    cdef double eps = (tpp-tll)*10.0**-6
#    cdef double s = t+ eps
#    cdef double nu_s2 = nut+2*nu_pt*s-s*s
#    cdef double aph1 = -p*(nu_pt-s)/nu_s2- (-p*nupt_m_t*nu_s_i1)
#    cdef double aph2 = -.5*(dt*nu_s2-2*(nu_pt-s)*(nu_d+dt*s))/(nu_s2*nu_s2) -  (-.5*(dt*nu_s-2*nupt_m_t*(nu_d+dt*t))*nu_s_i2)
#    if llConsts[seg,11] == 1:
#        print (aph1/eps, p*(nu_s+2*nupt_m_t2)*nu_s_i2)
#        print (aph2/eps, 2*dt*nupt_m_t*nu_s_i2 -(4*nupt_m_t2+nu_s)*(nu_d + dt*t)*nu_s_i3)
#        print (hess, (aph1+aph2)/eps)
#        print (tll,trr,tpp)

        
        
        


    if tll > 0 and trr > 0:
        tmp1 = 1/(t*(t-tll))
        tmp2 = 1/(t*(t-trr))
        grad += (alpha-1)*( -tpmt + tll*tmp1 + trr*tmp2 ) - (beta-1)/t 
        hess += (alpha-1)*(-tpmt2 - tll*(2*t-tll)*tmp1*tmp1 - trr*(2*t-trr)*tmp2*tmp2  )+(beta-1)/t2

#        if llConsts[seg,11] == 1:
#            aph1 =   (alpha-1)*( -1/(tpp-s) + tll/(s*(s-tll)) + trr/(s*(s-tll)) ) - (beta-1)/s   -    (alpha-1)*( -tpmt + tll*tmp1 + trr*tmp2 ) - (beta-1)/t
#            aph1 /= eps
#
#            print ( (alpha-1)*(-tpmt2 - tll*(2*t-tll)*tmp1*tmp1 - trr*(2*t-trr)*tmp2*tmp2  )+(beta-1)/t2, aph1)

#        grad += (alpha-1)*( -1/tpmt + tll/(t*(t-tll)) + trr/(t*(t-trr)) ) - (beta-1)/t 
#        hess += (alpha-1)*(-1/tpmt2 - tll*(2*t-tll)/(t2*(t-tll)*(t-tll)) - trr*(2*t-trr)/(t2*(t-trr)*(t-trr)))+(beta-1)/t2

    elif tll == 0 and trr == 0:
        grad += (alpha-1)*( -tpmt  ) + (beta-1)/t
        hess += (alpha-1)*(-tpmt2 )-(beta-1)/t2
    else:
        tnz = d_max(tll,trr)
        tmp1 = 1/(t*(t-tnz))
        grad += (alpha-1)*( -tpmt + tnz*tmp1 )
        hess += (alpha-1)*(-tpmt2 - tnz*(2*t-tnz)*tmp1*tmp1 )

#        if llConsts[seg,11] == 1:
#            aph1 =  (alpha-1)*( -1/(tpp-s) + tnz/(s*(s-tnz)) )   -   (alpha-1)*( -tpmt + tnz*tmp1 )
#            aph1 /= eps
#            print ((alpha-1)*(-tpmt2 - tnz*(2*t-tnz)*tmp1*tmp1 ), aph1)
    #end

#    cdef double* out = <double*>malloc(sizeof(double)*2)
#    if llConsts[seg,11] == 1:
#        print hess

    out[0] = grad
    out[1] = hess

#    return out


@cython.profile(False)
@cython.boundscheck(True)
@cython.wraparound(False)
cdef double eval_predictive_pdf( double vnew, int seg, double[:,:] priorConsts, double[:,:] llConsts, double alpha, double beta, double[:] v, double logdetLam, int p):

    cdef int ll = <int>priorConsts[seg,0]
#    cdef int rr = <int>priorConsts[seg,1]   
    cdef int pp = <int>priorConsts[seg,2]
    cdef double pC = priorConsts[seg,3]

    cdef double vpp = v[pp]
    cdef double vll = v[ll]
#    cdef double vrr = v[rr] 
 
    cdef double B1 = 1-vnew/vpp
    #Blist.append(B1)

    cdef double tmp = beta_logpdf(B1,alpha,beta)

    if tmp == -DBL_MAX:
        return -DBL_MAX

    cdef double beta_probs = beta_logpdf(B1,alpha,beta)

    #leaves have no associated beta rv
    if vll > 0:
        B1 = 1-vll/vnew
        tmp = beta_logpdf(B1,alpha,beta)
        if tmp == -DBL_MAX:
            return -DBL_MAX
        beta_probs += beta_logpdf(B1,alpha,beta)
    #end

#    rr is the new leaf, vrr is 0
#    if vrr > 0:
#        B1 = 1-vrr/vnew
#        tmp = beta_logpdf(B1,alpha,beta)
#        if tmp == -DBL_MAX:
#            return -DBL_MAX
#        beta_probs += beta_logpdf(B1,alpha,beta)
#    #end


    cdef double prior = beta_probs + pC

    cdef double lC = llConsts[seg,9]

#    cdef double d12 = llConsts[seg,1,0]     
#    cdef double d23 = llConsts[seg,1,1]     
    cdef double d31 = llConsts[seg,5]     
   
#    cdef double tll = vnew-v[ll]
#    cdef double trr = vnew-v[rr]
#    cdef double tpp = v[pp]-vnew
 
    cdef double nu1 = llConsts[seg,6]+(vnew-vll)
    cdef double nu2 = vnew
    cdef double nu3 = llConsts[seg,8]+(vpp-vnew)

    cdef double nu_s = nu1*nu2+nu2*nu3+nu3*nu1 

    cdef double nu_p = nu2 + nu1*nu3/(nu1+nu3) #1/(1/nu1+1/nu3)

    cdef double lZ = 0

    if nu_s != 0:
        lZ = -.5*( p*log(2*M_PI)+logdetLam + p*log(nu1+nu3) + d31*nu_p/nu_s)
    else:
        return -DBL_MAX
        #lZ = -DBL_MAX

    cdef double LL = lZ+lC

#    print "Prior: %f" % prior
#    print "Likelihood: %f" % LL
#    print lZ
#    print nu_s
#    print nu_p
#    print (vll,vnew,vpp)
#    print (ll,pp)

    return prior+LL 


@cython.profile(False)
@cython.boundscheck(True)
@cython.wraparound(False)
cdef void eval_predictive_grad( double t, int seg, double[:,:] priorConsts, double[:,:] llConsts, double alpha, double beta, double[:] v,  int p, double[:] out):


    cdef int ll = <int>priorConsts[seg,0]
    cdef int rr = <int>priorConsts[seg,1]   
    cdef int pp = <int>priorConsts[seg,2]
 

    #cdef double d_lr = llConsts[seg,1,0]     
    #cdef double d_rp = llConsts[seg,1,1]     
    cdef double d_pl = llConsts[seg,5]     
   
    cdef double tll = v[ll]
    cdef double trr = 0
    cdef double tpp = v[pp]
 
    cdef double nu_ll = llConsts[seg,6]
    cdef double nu_rr = 0 #llConsts[seg,2,1]
    cdef double nu_pp = llConsts[seg,8]

    cdef double nu_pt = nu_pp+tpp
    cdef double nu_lt = nu_ll-tll
    cdef double nu_rt = nu_rr-trr

    cdef double nut = nu_lt*nu_rt + nu_lt*nu_pt + nu_pt*nu_rt
    cdef double nu_s = nut+2*nu_pt*t-t*t 
    #cdef double nu_s2 = nu_s*nu_s
    #cdef double nu_s3 = nu_s2*nu_s

    cdef double nu_s_i1 = 1/nu_s
    cdef double nu_s_i2 = nu_s_i1*nu_s_i1
    cdef double nu_s_i3 = nu_s_i2*nu_s_i1

    cdef double t2 = t*t
    cdef double tpmt = 1/(tpp-t)
    cdef double tpmt2 = tpmt*tpmt

    cdef double nupt_m_t = nu_pt-t
    cdef double nupt_m_t2 = nupt_m_t*nupt_m_t  

    cdef double tnz
    cdef double tmp1
    cdef double tmp2

    cdef double nu_d = nu_ll+nu_pp+tpp-tll
    cdef double nu_u = nu_pp-nu_ll+tpp+tll
    cdef double nupl = (nu_pt-t)*(nu_lt+t)


    cdef double grad = -d_pl/2*(  (1+(nu_u-2*t)/nu_d)*nu_s_i1  - 2*nupt_m_t*(nu_rt+t+nupl/nu_d)*nu_s_i2  )
    cdef double hess = -d_pl/2*( -2/(nu_d*nu_s) + 2*(nu_rt+t + nupl/nu_d)*nu_s_i2   )  - 4*nupt_m_t*grad*nu_s_i1

#    print "Gradient: %f" % grad
#    print "Hessian: %f" % hess
#    print "times: (%f, %f)" % (t,v[pp])

    if tll == 0:
        grad += (alpha-1)*( -tpmt  ) + (beta-1)/t
        hess += (alpha-1)*(-tpmt2 )-(beta-1)/t2
    else:
        tnz = d_max(tll,trr)
        tmp1 = 1/(t*(t-tnz))
        grad += (alpha-1)*( -tpmt + tnz*tmp1 )
        hess += (alpha-1)*(-tpmt2 - tnz*(2*t-tnz)*tmp1*tmp1 )
    #end

#    cdef double* out = <double*>malloc(sizeof(double)*2)
    out[0] = grad
    out[1] = hess

#    return out

ctypedef long[:]* isp

cdef double eval_abpdf(double t, int seg, vl_wrapper *vlist_w):

    cdef va_list vlist
    va_copy(vlist,vlist_w.v)



    cdef isp P_p = <isp>va_arg(vlist,pointer_type)
    cdef long[:] P = P_p[0]
    cdef dsp v_p = <dsp>va_arg(vlist,pointer_type)
    cdef double[:] v = v_p[0]

    cdef double alpha = <double>va_arg(vlist,double_type)
    cdef double beta = <double>va_arg(vlist,double_type)
    cdef double xi = <double>va_arg(vlist,double_type)
    cdef double lam = <double>va_arg(vlist,double_type)

    cdef int N = <int>va_arg(vlist,int_type)
    va_end(vlist)

    if seg == 0:
        alpha = t
    else:
        beta = t



    cdef int i
    cdef double lna=0,lnb=0

    for i in range(N-2):
        lna += log( 1-v[N+i]/v[P[N+i]]) 
        lnb += log( v[N+i]/v[P[N+i]] ) 
    #end 

    cdef double result = (N-1)*(sp.gammaln(alpha+beta)-sp.gammaln(alpha)-sp.gammaln(beta)) + (alpha-1)*(lna-lam) + xi*log(alpha-1) + (beta-1)*(lnb-lam) + xi*log(beta-1)
#    print (seg,alpha,beta,lna,lnb,result)
    return result 

#eval_noisepdf( t, var_ind, df, sig0_2, a0, b0, lambda, dsum, N, p)
cdef double eval_noisepdf(double t, int var_ind, vl_wrapper *vlist_w):
    cdef va_list vlist
    va_copy(vlist,vlist_w.v)

    cdef double df = <double>va_arg(vlist,double_type)
    cdef double sig0_2 = <double>va_arg(vlist,double_type)
    cdef double a0 = <double>va_arg(vlist,double_type)
    cdef double b0 = <double>va_arg(vlist,double_type)
    cdef double lam = <double>va_arg(vlist,double_type)
 
    cdef dsp sigs_p = <dsp>va_arg(vlist,pointer_type)
    cdef double[:] sigs = sigs_p[0]
 
    cdef int N = <int>va_arg(vlist,int_type)
    cdef int p = <int>va_arg(vlist,int_type)
    va_end(vlist)


 #   print (df,sig0_2,a0,b0,lam,dsum,N,p)

    if var_ind == 0:
        df = t
    if var_ind == 1:
        sig0_2 = t


    cdef int i
#    cdef double dist_term = 0
#    for i in range(N):
#        dist_term += log( (df*sig0_2+dists[i])/2)
#    #end
#
#    dist_term *= (p+df)/2


    cdef double sig_term = 0
    for i in range(N):
        sig_term += -(df/2+1)*log(sigs[i]) -  df*sig0_2/(2*sigs[i])
    #end

    cdef double result = N*df/2*log(df*sig0_2/2)-N*sp.gammaln(df/2)+sig_term  - df*lam -(a0+1)*log(sig0_2) - b0/sig0_2

    #cdef double result = N*df/2*log(df*sig0_2/2)-N*sp.gammaln(df/2)+N*sp.gammaln( (p+df)/2)-dist_term  - df*lam -(a0+1)*log(sig0_2) - b0/sig0_2

#    if df < .00001:
#       print (df, N*df/2*log(df*sig0_2/2), -N*sp.gammaln(df/2), N*sp.gammaln( (p+df)/2), -dist_term, -(a0+1)*log(sig0_2), - b0/sig0_2 )
        

    return result

@cython.profile(False)
cdef inline double norm_ind(double[:,:] vecs, int ind1, int ind2, int p):
    cdef int i
    cdef double sm = 0
    for i in range(p):
        sm += (vecs[ind1,i]-vecs[ind2,i])*(vecs[ind1,i]-vecs[ind2,i])

    return sm

@cython.profile(False)
cdef inline double beta_logpdf(double x, double alpha, double beta):

    if x == 0 or x == 1:
        return -DBL_MAX #np.inf

    return (alpha-1)*log(x)+(beta-1)*log(1-x)

cdef double dot(double[:] a, double[:] b, int p):
    cdef double sm = 0
    cdef int i
    for i in range(p):
        sm += a[i]*b[i]

    return sm

cdef double npsum(double[:] a, int p):
    cdef double sm = 0
    cdef int i
    for i in range(p):
        sm += a[i]

    return sm

cdef np.ndarray[DTYPE_t,ndim=1] npsum2(np.ndarray[DTYPE_t,ndim=2] a, int p1, int p2):
    cdef np.ndarray[DTYPE_t,ndim=1] sm

    sm = np.zeros(p2,dtype=np.double)


    cdef int i
    for i in range(p1):
        sm += a[i,:]

    return sm

def fbmdat2npy(filename):
    return np.loadtxt(filename)

def npy2fbmdat(X,fname):
    np.savetxt(fname,X,fmt='%+f')


cpdef benchmark_numpy():
    cdef np.ndarray[DTYPE_t,ndim=1] A = np.zeros(1000)
    cdef np.ndarray[DTYPE_t,ndim=2] B = np.ones((1000,2))
    cdef int i,ind
    cdef double sm = 0
    cdef double[:] Aview = A
    cdef double[:,:] Bview = B
    for i in range(3000000):
        ind = <int>fmod(i,1000)
        sm += numpy_test(ind,Aview,Bview)

cpdef benchmark_c():
    cdef np.ndarray[DTYPE_t] A = np.zeros(1000)
    cdef np.ndarray[DTYPE_t] B = np.ones(1000)
    cdef int i,ind
    cdef double sm = 0
    cdef double* Adat = <double*>A.data 
    cdef double* Bdat = <double*>B.data 
    for i in range(3000000):
        ind = <int>fmod(i,1000)
        sm += c_test(ind,Adat,Bdat)

cdef double c_test(int ind, double* A, double* B):
    return A[ind]+B[ind]

@cython.boundscheck(False)
@cython.wraparound(False)
cdef double numpy_test(int ind, double[:] A, double[:,:] B):
    return A[ind]+B[ind,0]

